package dig.app.com;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class NewProfile extends Activity{
	
	ArrayList<HashMap<String, String>> myProfilelist = new ArrayList<HashMap<String, String>>();
	
	private static final int REQUEST_EDIT_FILE=1;
	private static final int REQUEST_EDIT_IMAGE=2;
	int async=0;
	public Bitmap logo,pic;
	Object details;
	ImageView profileEditPic,companyEditLogo,companyEditVideo;
	boolean videoDisplay=false;
	String fileEditPath,fileEditImagePath = null;
	boolean connection=false;
	InputStream is,to,deleteStream=null;
	String userName,passWord;
	String result,data,deleteResult=null;
	String videoId,c_name,c_designation,c_company,c_company_address,c_phone,c_email,c_pic,c_logo,c_desription,c_comments,c_webUrl,c_office,c_fax;
	private ArrayList<String> response=new ArrayList<String>();
	String name,designation,company,company_adderss,phone,email,myProfile_pic,myProfile_logo, myProfile_video, myProfile_description, myProfile_url, myProfile_comments, myProfile_office, myProfile_fax ;
	int row_id;
	TextView ProfileName;
	EditText ProfileNumber,Designation,CompanyName,CompanyAddress,ProfileEmail,newVideo,profileDescription,webUrl,comments,office,fax;
	String c_video,userId=null;
	Button changeVideo;
	String fileImagePath,filePath;
	//private Drawable mDrawable,imageDrawable,logoDrawable;
	static Context context;
	private Bitmap mBitmap,imageBitmap,logoBitmap;
	private int useDefaultPic=0,useDefaultLogo=0;
	private static final int COMPLETE=0;
	private static final int FAILED=1;
	private static final int IMAGECOMPLETE=2;
	private static final int LOGOCOMPLETE=3;
	private static final int IMAGEFAILED=4;
	private static final int LOGOFAILED=5;
	Bitmap pLogo,pPic,pVideo,logoThumb,profileThumb;
	ToggleButton toggle;
	int autoLoginValue=0;
	int profilePicChange=0,profileLogoChange=0;
	int profilePicIntent=0,profileLogoIntent=0;
	String previous_pic,previous_logo;
	private ProgressDialog loadingDialog;
	private String freshImageBy,freshLogoBy;
	private LinearLayout profileLayout;
	Button deleteAccount;
	RelativeLayout.LayoutParams layoutParams;
	RelativeLayout.LayoutParams defaultlayoutParams;
	private int deleteCheck=0;
	@SuppressWarnings("unchecked")
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.bussiness_list);
		
		
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		SharedPreferences.Editor prefEditor = loginSettings.edit();  
		prefEditor.putInt("number", 4); 
		prefEditor.commit(); 
		
		context = this;
		profileLayout=(LinearLayout) findViewById(R.id.profile_layout);
		
		ProfileName=(TextView) findViewById(R.id.bussiness_name);
		ProfileNumber=(EditText) findViewById(R.id.bussiness_number);
		Designation=(EditText) findViewById(R.id.bussiness_designation);
		CompanyName=(EditText) findViewById(R.id.bussiness_company);
		CompanyAddress=(EditText) findViewById(R.id.bussiness_address);
		ProfileEmail=(EditText) findViewById(R.id.bussiness_email);
		companyEditVideo=(ImageView) findViewById(R.id.bussiness_company_video);
		changeVideo=(Button) findViewById(R.id.change_video);
		newVideo=(EditText) findViewById(R.id.new_video);
		profileDescription=(EditText) findViewById(R.id.bussiness_description);
		webUrl=(EditText) findViewById(R.id.bussiness_weburl);
		comments=(EditText) findViewById(R.id.bussiness_comments);
		toggle = (ToggleButton) findViewById(R.id.toggle_login);
		office=(EditText) findViewById(R.id.bussiness_office);
		fax=(EditText) findViewById(R.id.bussiness_fax);
		//update=(Button) findViewById(R.id.update);
		deleteAccount =(Button) findViewById(R.id.delete_account);
		
		ProfileName.setSelected(true);
		
		profileEditPic=(ImageView) findViewById(R.id.bussiness_pic);
		companyEditLogo=(ImageView) findViewById(R.id.bussiness_company_logo);
		
		layoutParams = new RelativeLayout.LayoutParams(110, 100);
		defaultlayoutParams = new RelativeLayout.LayoutParams(34, 34);
		
		SharedPreferences imageSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		String logoName=imageSettings.getString("profileLogoName", null);
		String picName=imageSettings.getString("profilePicName", null);
		Typeface typeFace=Typeface.createFromAsset(getAssets(),"fonts/helvetica.ttf");
		//ProfileName.setTypeface(typeFace);
		//ProfileEmail.setTypeface(typeFace);
		/*if(picName==null){
			
			filePath="imagename.jpg";
			
		}
		
		if(logoName==null){
			
			fileImagePath="imagename.jpg";
			
		}*/
		 //prefEditor.putString("profilePicName",filePath);
		
		
		
		
		pPic=BitmapFactory.decodeResource(this.getResources(), R.drawable.profile_pic);
		pLogo=BitmapFactory.decodeResource(this.getResources(), R.drawable.logo);
		pVideo=BitmapFactory.decodeResource(this.getResources(), R.drawable.video_thumbnail_big2);
		logoThumb=BitmapFactory.decodeResource(this.getResources(), R.drawable.logo_thumbnail);
		profileThumb=BitmapFactory.decodeResource(this.getResources(), R.drawable.profile_pic);
		
		deleteAccount.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				deleteOption();
				
			}
		});
		
		companyEditVideo.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(myProfile_video.length() >0){
					
					playVideo(myProfile_video);
					
				}else{
					
					Toast.makeText(NewProfile.this, "You have not specified any Video Link", Toast.LENGTH_LONG).show();
				}
				
				
				
			}
		});
		
		toggle.setOnClickListener(new View.OnClickListener() {
			
		
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				
				 if((toggle.isChecked()))
			        {
			               // System.out.println("checked");
			                autoLoginValue=1;
			                
			        }
			        else
			        {
			                //system.out.println("Unchecked");
			                autoLoginValue=0;
			        }			
				
				 
			}
		});
		
		details = getLastNonConfigurationInstance();
		
		if(details ==null){
			
			//bumpDisable();
			serverConnection();
			
			
			SharedPreferences settings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor editor = settings.edit();  
			editor.putInt("profile", 0);
			editor.putInt("login", 0);
			editor.putString("register", null);
			editor.commit();
			
			
			
		}else{
			
			int size=((ArrayList<HashMap<String, String>>)details).size();
			
			if(size >0){
				
				dataFill((ArrayList<HashMap<String, String>>)details);
			}else{
				
				//Toast.makeText(NewProfile.this, "No Data Found", Toast.LENGTH_LONG).show();
			}
			
			
		}
		
		
	}
	
	
	
	
	/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.update_menu, menu);
		return true;
	}*/
	
	public boolean onMenuItemSelected(int featureId, MenuItem item){
		
		switch(item.getItemId()){
			
			case R.id.menu_update:
				
								
				validation();
				
				break;
				
			case R.id.menu_update_cancel:
				
				
				previousState();
				
				break;	
				
			/*case R.id.menu_delete_profile:
				deleteOption();
				
				break;*/
		}
		return super.onMenuItemSelected(featureId, item);
		
		
	}
	
	
	
	public void bumpEnable(){
		
		int value=0;
		MainActivity parent;
        parent =(MainActivity) NewProfile.this.getParent();
        parent.manipulateBump(value);
        
	}
	
	
	public void bumpDisable(){
		
		int value=1;
		MainActivity parent;
        parent =(MainActivity) NewProfile.this.getParent();
        parent.manipulateBump(value);
        
	}
	
	
	public void onStart(){
		super.onStart();
		
		//System.out.println("myprofile onStart");
		
		profileLayout.requestFocus();
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		SharedPreferences.Editor prefEditor = loginSettings.edit();  
		prefEditor.putInt("number", 4); 
		prefEditor.commit();
		
	}
	
	

	public void deleteOption(){
		
		AlertDialog alert=null;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(NewProfile.this);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			  
			  deleteCheck =1;
			
			  dialog.dismiss();
			  deleteFromDb();
			  
              
			  
			  
		  }});
		
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			
			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  
				
				  dialog.dismiss();
				  
				  
			  }});
		
		
		builder.setMessage("Delete Account");
		alert=builder.create();
		alert.show();
		
		
	}
	
	
	
	public void deleteFromDb(){
		
		connection=haveNetworkConnection();
		
		if(connection){
			MyProfileDeleteTask task = new MyProfileDeleteTask(this);
			//task.execute(new String[] {"http://192.168.1.187/android/myprofiledelete.php"});
			//task.execute(new String[] {"http://dev-fsit.com/iphone/digcard/myprofiledelete.php"});
			task.execute(new String[] {"http://digcardapp.com/app/myprofiledelete.php"});
			
			//bumpDisable();
			
		}else{
			alertFunction("No Internet Connection",1);
			//Toast.makeText(this, "No network connection", Toast.LENGTH_SHORT).show();
		}
		
		
		
	}
	public void playVideo(String video){
    	
    	
			String video_path = video;
			
			try{
				
				
				//Uri uri = Uri.parse(video_path);

		    	// With this line the Youtube application, if installed, will launch immediately.
		    	// Without it you will be prompted with a list of the application to choose.
		    	
		    	//uri = Uri.parse("vnd.youtube:"  + uri.getQueryParameter("v"));
				
				Uri uri = Uri.parse("vnd.youtube:"  + video_path);
		    	
		    	
		    	Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		    	startActivity(intent);
				
			}catch(Exception e){
				
				
			}
			

    	
    	
    }
	

	private void serverConnection() {
		// TODO Auto-generated method stub
		//System.out.println("My profile server connection");
		
		
		
		connection=haveNetworkConnection();
		
		if(connection){
			MyProfileTask task = new MyProfileTask(this);
			//task.execute(new String[] {"http://dev-fsit.com/iphone/digcard/myprofile.php"});
			//task.execute(new String[] {"http://dev-fsit.com/iphone/myprofile.php"});
			//http://192.168.1.187/android/AndroidMyprofile.php
			//task.execute(new String[] {"http://192.168.1.187/android/myprofile.php"});
			task.execute(new String[] {"http://digcardapp.com/app/myprofile.php"});
			bumpDisable();
			
		}else{
			alertFunction("No Internet Connection", 1);
			//Toast.makeText(this, "No network connection", Toast.LENGTH_SHORT).show();
		}
		
		
		
	}
	
	
	private class MyProfileTask extends AsyncTask<String, Integer, ArrayList<HashMap<String, String>>>{
		
		
		private Context mContext;
		
		MyProfileTask(Context context)
        {
			
			
            mContext = context;
            loadingDialog = new ProgressDialog(mContext);
           // loadingDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            loadingDialog.setMessage("Loading..");
            loadingDialog.setCancelable(false);
           // loadingDialog.setMax(100);
            loadingDialog.show();
        }
		

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(String... urls) {
			// TODO Auto-generated method stub
			
			myProfilelist = new ArrayList<HashMap<String, String>>();
			
			for (String url : urls) {
				
				try {
					
					
					SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
		             userName = loginSettings.getString("user_name", null);
		             passWord = loginSettings.getString("password", null);
		             userId   = loginSettings.getString("user_id", null);
		             String auto_user_id= loginSettings.getString("auto_user_id", null);
		             
		             if(userId==null){
		            	 
		            	 userId=auto_user_id;
		            	 
		            	SharedPreferences checkSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		            	SharedPreferences.Editor prefEditor = checkSettings.edit();  
		    			prefEditor.putString("user_id", userId); 
		    			prefEditor.commit(); 
		             }
		             
		            // int id=Integer.parseInt(userId);
		            
					
		             
					DefaultHttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost(url);
					
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		            
		            nameValuePairs.add(new BasicNameValuePair("user_id", userId));
		            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		            
		            HttpResponse execute = httpclient.execute(httppost);
					
					HttpEntity entity     = execute.getEntity();
					is = entity.getContent();
					
				}catch(Exception e){
					
					//Toast.makeText(getBaseContext(), "No server connection", Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}
					
			try{
				
				BufferedReader reader = new BufferedReader(new InputStreamReader( is, "iso-8859-1"),8); 
				StringBuilder sb = new StringBuilder();
	        	sb.append(reader.readLine() + "\n");
	        	String line="0";
	        	
	        	while((line =reader.readLine()) !=null){
	        		
	        		sb.append(line + "\n");
	        		
	        	}
				
	        	is.close();
	        	
	        	result=sb.toString();
	        	
				
				
			}catch(Exception e){
				
				//Toast.makeText(NewProfile.this, "Error in string convertion", Toast.LENGTH_LONG).show();
				Log.e("log_tag", "Error converting result "+e.toString());
			}
			
			try{
				
				JSONArray jArray = new JSONArray(result);
				JSONObject json_data;
				
				for(int i=0; i<jArray.length(); i++){
				
				HashMap<String, String> map = new HashMap<String, String>();
				
				json_data= jArray.getJSONObject(0);
				
				row_id=json_data.getInt("user_id");
				
				String card_id=Integer.toString(row_id);
				
	        	name=json_data.getString("name");
	        	
	        	designation=json_data.getString("designation");
	        	
	        	company=json_data.getString("company_name");
	        	
	        	company_adderss=json_data.getString("company_address");
	        	
	        	phone=json_data.getString("phone");
	        	
	        	email=json_data.getString("email");
	        	
	        	myProfile_pic=json_data.getString("profile_picture");
	        	
	        	myProfile_logo=json_data.getString("company_logo");
	        	
	        	myProfile_video=json_data.getString("video");
	        	
	        	myProfile_description=json_data.getString("description");
	        	myProfile_comments= json_data.getString("comment");
	        	myProfile_url=json_data.getString("web_url");
	        	myProfile_office=json_data.getString("office");
	        	myProfile_fax=json_data.getString("fax");
	        	
	        	
	        	
	        	map.put("id", card_id);
	        	map.put("name", name);
	        	map.put("designation", designation);
	        	map.put("company", company);
	        	map.put("company_address", company_adderss);
	        	map.put("phone", phone);
	        	map.put("email", email);
	        	map.put("pic", myProfile_pic);
	        	map.put("logo", myProfile_logo);
	        	map.put("video", myProfile_video);
	        	map.put("description", myProfile_description);
	        	map.put("comments", myProfile_comments);
	        	map.put("webUrl", myProfile_url);
	        	map.put("office", myProfile_office);
	        	map.put("fax", myProfile_fax);
	        	
	        	myProfilelist.add(map);
				}
			}catch(JSONException ep){
				
				Log.e("log_tag", "Error JAson result "+ep.toString());
				
	        	//Toast.makeText(NewProfile.this, "No Data Found", Toast.LENGTH_LONG).show();
	        	
	        }catch (ParseException e) {
	        	// TODO Auto-generated catch block
	        	
				e.printStackTrace();
				
			}
		}		
		
		return myProfilelist;
		
	}
		
		protected void onProgressUpdate(Integer... values)
        {
			 loadingDialog.show();
			
        }
		
		
		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> list) {
			
			async=1;
			bumpEnable();
			
		
			
			try {
				
				loadingDialog.dismiss();
				loadingDialog = null;
				
		    } catch (Exception e) {
		        
		    }
		    
		   // s
		    SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
		    int number=loginSettings.getInt("number", 3);
		    int update=loginSettings.getInt("profile", 0);
		    
		    SharedPreferences updateSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
	    	SharedPreferences.Editor prefEditor = updateSettings.edit();  
		    
		    if(update==1){
		    	prefEditor.putInt("profile", 0);
		    	//Toast.makeText(getApplication(), "Card Updated Successfully", Toast.LENGTH_SHORT).show();
		    	
		    	if(number==4){
		    		
		    		alertFunction("Card Updated Successfully",0);
		    		
		    	}
	     		
	     		
		    }
		    prefEditor.putInt("checkFirstTime", 0);
		    prefEditor.putInt("login", 0);
		    prefEditor.putString("register", null);
		    prefEditor.putInt("save", 0); 
		    prefEditor.commit();
		    
		    if(number==4){
		    	
		    	if(list.size() >0){
			    	
			    	dataFill(list);
			    }else{
			    	
			    	Toast.makeText(NewProfile.this, "No Data Found", Toast.LENGTH_LONG).show();
			    }
		    	
		    }
		    
		    
				
		}

   }
	
	
	public void dataFill(ArrayList<HashMap<String, String>> fill){
		
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
		int doNothig = loginSettings.getInt("doNothing", 0);
		int auto=loginSettings.getInt("auto", 0);
		String userId=loginSettings.getString("user_id", null);
		String autoUserId=loginSettings.getString("auto_user_id", null);
		
		
		
		
		@SuppressWarnings("rawtypes")
		Iterator itr = fill.iterator();
		while (itr.hasNext()) {
			  
			@SuppressWarnings("unchecked")
			HashMap<String, String> map = (HashMap<String, String>) itr.next();
			//String c_id = (String) map.get("id");
			   c_name = (String) map.get("name").trim();
			   c_designation = (String) map.get("designation").trim();
			   c_company= (String) map.get("company").trim();
			   c_company_address = (String) map.get("company_address").trim();
			   c_phone = (String) map.get("phone").trim();
			   c_email = (String) map.get("email").trim();
			   
			   
			   SharedPreferences imageSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			   previous_pic=imageSettings.getString("save pic", null);
			   previous_logo=imageSettings.getString("save logo", null);
			   
			   
			   //previous_pic=c_pic;
			   //previous_logo=c_logo;
			   
			  /* if(c_pic!=(String) map.get("pic")){
				   
				   profilePicChange=1;
				   
			   }else{
				   
				   profilePicChange=0;
			   }
			   
			   System.out.println("previous c_logo "+c_logo);
			   
			   if(c_logo!=(String) map.get("logo")){
				   
				   profileLogoChange=1;
				   
			   }else{
				   
				   profileLogoChange=0;
			   }*/
			   
			   c_pic = (String) map.get("pic").trim();
			   c_logo = (String) map.get("logo").trim();
			   c_video = (String) map.get("video").trim();
			  
			   
			   SharedPreferences itemSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
				SharedPreferences.Editor prefEditor = itemSettings.edit();  
				prefEditor.putString("save pic", c_pic); 
				prefEditor.putString("save logo", c_logo); 
				prefEditor.commit(); 
			   
			   //system.out.println("previous c_pic "+previous_pic);
			   
			   
			   //system.out.println("previous c_logo "+previous_logo);
			   
			   
			   c_desription= (String) map.get("description").trim();
			   c_comments= (String) map.get("comments").trim();
			   c_webUrl= (String) map.get("webUrl").trim();
			   c_office= (String) map.get("office").trim();
			   c_fax= (String) map.get("fax").trim();
			  // if(doNothig!=1){
				   
			   
			  
			   
				   showThumbnail(c_video);
				   loadBitmap(c_pic);
				   logoBitmap(c_logo);
				   //loadDrawable(c_pic);
				   //logoDrawable(c_logo);
			   //}
			 
			  
			/*if(auto==1){
				
				toggle.setChecked(true);
				autoLoginValue=1;
			}else{
				
				toggle.setChecked(false);
				autoLoginValue=0;
			}*/
				   
				   
				   if(autoUserId!=null&&autoUserId.equals(userId)){
					   
					   
					   
					   toggle.setChecked(true);
						autoLoginValue=1;
					   
						
						
					   
				   }else{
					   
					   toggle.setChecked(false);
						autoLoginValue=0;
					   
				   }
			 
				   
				   SharedPreferences settings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					SharedPreferences.Editor editor = settings.edit();  
					editor.putInt("auto", autoLoginValue); 
					editor.commit(); 
			  
			  ProfileName.setText(c_name);
			  ProfileNumber.setText(c_phone);
			  Designation.setText(c_designation);
			  CompanyName.setText(c_company);
			  CompanyAddress.setText(c_company_address);
			  ProfileEmail.setText(c_email);
			  profileDescription.setText(c_desription);
			  webUrl.setText(c_webUrl);
			  comments.setText(c_comments);
			  if(office!=null){
				  
				  office.setText(c_office);
			  }else{
				  
				  office.setText("");
			  }
			 
			  if(fax!=null){
				  
				  fax.setText(c_fax);
			  }else{
				  
				  fax.setText("");
			  }
			  
			  
			 
			  
			 /* try {
					
					byte[] imageAsBytes,logoAsBytes;
					imageAsBytes = Base64.decode(c_pic.getBytes());
					ImageView image = (ImageView)NewProfile.this.findViewById(R.id.bussiness_pic);
				    image.setImageBitmap(
				            BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
				    
				    logoAsBytes = Base64.decode(c_logo.getBytes());
				    ImageView Companylogo = (ImageView)NewProfile.this.findViewById(R.id.bussiness_company_logo);
				    Companylogo.setImageBitmap(
				            BitmapFactory.decodeByteArray(logoAsBytes, 0, logoAsBytes.length));
				    
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
			  
			  
		}	
		
		
		
	}
	
	
	/*public void loadDrawable(final String imgeUrl){
		
		imageDrawable=null;
		
		new Thread(){
    		public void run(){
    			try{
    				
    				//System.out.println("new loadDrawable Thread");
    				imageDrawable=getDrawableFromUrl(imgeUrl);
    				imageLoaderHandler.sendEmptyMessage(IMAGECOMPLETE);
    				
    			}catch(MalformedURLException e){
    				
    				//System.out.println("MalformedURLException ");
    				
    				imageLoaderHandler.sendEmptyMessage(IMAGEFAILED);
    				
    			}catch(IOException e){
    				
    				//System.out.println("IOException ");
    				
    				imageLoaderHandler.sendEmptyMessage(IMAGEFAILED);
    				
    			}
    			
    		};

			
    		
    	}.start();
		
		
		
	}*/
	
	
	
	public void loadBitmap(final String imgeUrl ){
		
		
		imageBitmap=null;
		
		new Thread(){
    		public void run(){
    			
    			try{
    				
    				imageBitmap=getBitmapFromUrl(imgeUrl);
    				imageLoaderHandler.sendEmptyMessage(IMAGECOMPLETE);
    				
    			}catch(MalformedURLException e){
    				
    			}catch(IOException e){
    				
    				//System.out.println("IOException ");
    				
    				imageLoaderHandler.sendEmptyMessage(IMAGEFAILED);
    				
    			}
    			
    			
    		};
    		
    	}.start();
		
	}
	
	public void logoBitmap(final String imgeUrl){
		
		logoBitmap=null;
		
		new Thread(){
			
			public void run(){
				
				try{
    				
    				//System.out.println("new logoDrawable Thread");
					logoBitmap=getBitmapFromUrl(imgeUrl);
    				imageLoaderHandler.sendEmptyMessage(LOGOCOMPLETE);
    			}catch(MalformedURLException e){
    				
    				//System.out.println("MalformedURLException ");
    				
    				imageLoaderHandler.sendEmptyMessage(LOGOFAILED);
    				
    			}catch(IOException e){
    				
    				//System.out.println("IOException ");
    				
    				imageLoaderHandler.sendEmptyMessage(LOGOFAILED);
    				
    			}
				
				
				
			};
			
			
		}.start();
		
		
	}
	
	
/*public void logoDrawable(final String imgeUrl){
		
		logoDrawable=null;
		
		new Thread(){
    		public void run(){
    			try{
    				
    				//System.out.println("new logoDrawable Thread");
    				logoDrawable=getDrawableFromUrl(imgeUrl);
    				imageLoaderHandler.sendEmptyMessage(LOGOCOMPLETE);
    			}catch(MalformedURLException e){
    				
    				//System.out.println("MalformedURLException ");
    				
    				imageLoaderHandler.sendEmptyMessage(LOGOFAILED);
    				
    			}catch(IOException e){
    				
    				//System.out.println("IOException ");
    				
    				imageLoaderHandler.sendEmptyMessage(LOGOFAILED);
    				
    			}
    			
    		};

			
    		
    	}.start();
		
		
		
	}*/
	
	
	
	public void showThumbnail(String video){
    	
		  
    	
		String thumbNail=video;
		String videoId=null;
		String imageUrl=null;
		
		if(thumbNail !=null &&thumbNail.length()>0 ){
			
			try{
				//Uri uri = Uri.parse(thumbNail);
				//videoId=uri.getQueryParameter("v").toString();
				
				
		    	
		    	imageUrl="http://img.youtube.com/vi/"+thumbNail+"/2.jpg";
		    	
		    	//system.out.println("imageUrl "+imageUrl);
				
			}catch(Exception e){
				
				Log.i("profile", "No video id");
				
			}
			
			
		}
		
    	
    	if(thumbNail !=null&&thumbNail.length()>0){
    		
    		
    		//setImageDrawable(imageUrl);
    		setImageBitmap(imageUrl);
    		
    	}else{
    		
    		companyEditVideo.setImageBitmap(pVideo);
    		
    	}
    }
	
	
	public void setImageBitmap(final String url){
		
		mBitmap=null;
		
		new Thread(){
			
			public void run(){
				
				try{
    				
    				//System.out.println("new setImageDrawable Thread");
					mBitmap=getBitmapFromUrl(url);
    				imageLoaderHandler.sendEmptyMessage(COMPLETE);
    			}catch(MalformedURLException e){
    				
    				//System.out.println("MalformedURLException ");
    				
    				imageLoaderHandler.sendEmptyMessage(FAILED);
    				
    			}catch(IOException e){
    				
    				//System.out.println("IOException ");
    				
    				imageLoaderHandler.sendEmptyMessage(FAILED);
    				
    			}
				
				
				
			};
			
			
		}.start();
		
	}
	
	
	/*public void setImageDrawable(final String url){
    	
    	//System.out.println("in set Drawable");
    	
    	mDrawable=null;
    	
    	new Thread(){
    		public void run(){
    			try{
    				
    				//System.out.println("new setImageDrawable Thread");
    				mDrawable=getDrawableFromUrl(url);
    				imageLoaderHandler.sendEmptyMessage(COMPLETE);
    			}catch(MalformedURLException e){
    				
    				//System.out.println("MalformedURLException ");
    				
    				imageLoaderHandler.sendEmptyMessage(FAILED);
    				
    			}catch(IOException e){
    				
    				//System.out.println("IOException ");
    				
    				imageLoaderHandler.sendEmptyMessage(FAILED);
    				
    			}
    			
    		};

			
    		
    	}.start();
    	
    }*/
    
    private final Handler imageLoaderHandler = new Handler(new Callback(){
    	
    	
	
		public boolean handleMessage(Message msg) {
			// TODO Auto-generated method stub
			
			//system.out.println("message handler");
			switch(msg.what){
	    	
	    	
	    	case COMPLETE:
	    		
	    		//system.out.println("message complete");
	    		
	    		companyEditVideo.setImageBitmap(mBitmap);
	    		
	    		break;
	    	case FAILED:
	    		//system.out.println("message failed");
	    		companyEditVideo.setImageBitmap(pVideo);
	    		//Toast.makeText(NewProfile.this, "Failed to load Video thumb nail.Check your connection", Toast.LENGTH_LONG).show();
	    		
	    		break;
	    		
	    	case IMAGECOMPLETE:
	    		
	    		
	    		if(imageBitmap!=null){
	    			/*String haystack = c_pic;
	    			String needle = "default.jpg";
	    			
	    			int index = haystack.indexOf(needle);
	    			System.out.println("index "+index);
	    			if (index != -1){System.out.println("default");
	    				profileEditPic.getLayoutParams().height = 34;
	    				profileEditPic.getLayoutParams().width = 34;
	    			}else{System.out.println("not default");
	    				profileEditPic.getLayoutParams().height = 100;
	    				profileEditPic.getLayoutParams().width = 100;
	    			}*/
	    			//system.out.println("imageDrawable not null");
	    			//profileEditPic.setLayoutParams(layoutParams);
	    			if(imageBitmap.getWidth()>100 ||imageBitmap.getHeight()>100){
	    				profileEditPic.getLayoutParams().height = 100;
	    				profileEditPic.getLayoutParams().width = 100;
	    			}
	    			profileEditPic.setImageBitmap(imageBitmap);
	    			
	    			
	    			ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
					//Bitmap bitmap = ((BitmapDrawable)imageDrawable).getBitmap();
	    			imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArray);
					byte [] FreshBa=byteArray.toByteArray();
					freshImageBy=Base64.encodeBytes(FreshBa);
					
	    			
	    			
	    		}else{
	    			
	    			//System.out.println("imageDrawable null");
	    			profileEditPic.getLayoutParams().height = 34;
	    			profileEditPic.getLayoutParams().width = 34;
	    			//profileEditPic.setLayoutParams(layoutParams);
	    			profileEditPic.setImageBitmap(pPic);
	    			//Toast.makeText(NewProfile.this, "Failed to load image.Check your connection", Toast.LENGTH_LONG).show();
	    		}
	    		
	    		
	    		
	    		break;
	    		
	    	case IMAGEFAILED:
	    		
	    		//System.out.println("message profile image failed");
	    		profileEditPic.getLayoutParams().height = 34;
    			profileEditPic.getLayoutParams().width = 34;
	    		//profileEditPic.setLayoutParams(layoutParams);
	    		profileEditPic.setImageBitmap(pPic);
	    		//Toast.makeText(NewProfile.this, "Failed to load image.Check your connection", Toast.LENGTH_LONG).show();
	    		
	    		break;	
	    	case LOGOCOMPLETE:
	    		
	    		
	    		
	    		
	    		if(logoBitmap!=null){
	    			/*String haystack = c_logo;
	    			String needle = "default.jpg";
	    			
	    			int index = haystack.indexOf(needle);
	    			
	    			if (index != -1){
	    				companyEditLogo.getLayoutParams().height = 34;
		    			companyEditLogo.getLayoutParams().width = 34;
		    			companyEditLogo.setImageBitmap(logoThumb);
	    			}else{
	    				companyEditLogo.getLayoutParams().height = 100;
		    			companyEditLogo.getLayoutParams().width = 100;
		    			companyEditLogo.setImageBitmap(logoBitmap);
	    			}*/
	    	            
	    			if(logoBitmap.getWidth()>100 ||logoBitmap.getHeight()>100){
	    				companyEditLogo.getLayoutParams().height = 100;
	    				companyEditLogo.getLayoutParams().width = 100;
	    			}
	    			companyEditLogo.setImageBitmap(logoBitmap);
	    			//companyEditLogo.setLayoutParams(layoutParams);
	    			
	    			
	    			
	    			ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
					//Bitmap bitmap = ((BitmapDrawable)logoDrawable).getBitmap();
	    			logoBitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArray);
					byte [] FreshLogoBy=byteArray.toByteArray();
					freshLogoBy=Base64.encodeBytes(FreshLogoBy);
	    			
	    		}else{
	    			
	    			//System.out.println("logoDrawable null");
	    			companyEditLogo.getLayoutParams().height = 34;
	    			companyEditLogo.getLayoutParams().width = 34;
	    			//companyEditLogo.setLayoutParams(layoutParams);
	    			companyEditLogo.setImageBitmap(logoThumb);
	    			//Toast.makeText(NewProfile.this, "Failed to load image.Check your connection", Toast.LENGTH_LONG).show();
	    			
	    		}
	    		
	    		
	    		 
	    		
	    		break;
	    		
	    	case LOGOFAILED:
	    		
	    		
	    		companyEditLogo.getLayoutParams().height = 34;
    			companyEditLogo.getLayoutParams().width = 34;
	    		//companyEditLogo.setLayoutParams(layoutParams);
	    		//System.out.println("message logo failed");
	    		companyEditLogo.setImageBitmap(logoThumb);
	    		//Toast.makeText(NewProfile.this, "Failed to load image.Check your connection", Toast.LENGTH_LONG).show();
	    		
	    		/* try {
						
						loadingDialog.dismiss();
						loadingDialog = null;
						
				    } catch (Exception e) {
				        
				    }*/
	    		
	    		break;
	    		
	    		default:
	    			//system.out.println("message default");
	    			break;
	    		
	    		
	    	}
			return true;
	    
		}
    	
    });
    		
   


	/*  private static Drawable getDrawableFromUrl(final String url) throws IOException,MalformedURLException{
		// TODO Auto-generated method stub
		  
		  //system.out.println("image from url");
		return Drawable.createFromStream(((java.io.InputStream)new java.net.URL(url).getContent()), "name");
	}*/
    
    
    private Bitmap getBitmapFromUrl(String url) throws IOException,MalformedURLException{
    	
    	Bitmap logoBitmap= BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, null);
    	
		return logoBitmap;
    	
    	
    }
	
	
	  public void videoEdit(View view){
		  
		  
		  //system.out.println("Video edit");
					
		  newVideo.setVisibility(View.VISIBLE);
					
				  
		  
	  }
	  
	  
	  public class MyProfileDeleteTask extends AsyncTask<String,Void,String>{
		  
		  ProgressDialog dialog;
		  
		  MyProfileDeleteTask(Context context)
	        {
				
				
	           
	             dialog = new ProgressDialog(context);
	             dialog.setMessage("Deleting..");
	             dialog.setCancelable(false);
	             dialog.show();
	        }

		@Override
		protected String doInBackground(String... urls) {
			// TODO Auto-generated method stub
			
			for(String url : urls){
				
				SharedPreferences itemSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
				userId   = itemSettings.getString("user_id", null);
				
				if(userId!=null){
					
					try{
						DefaultHttpClient httpclient = new DefaultHttpClient();
						HttpPost httppost = new HttpPost(url);
						
						List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			            
			            nameValuePairs.add(new BasicNameValuePair("user_id", userId));
			            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			            
			            HttpResponse execute = httpclient.execute(httppost);
						
						HttpEntity entity     = execute.getEntity();
						deleteStream=entity.getContent();
						
					}catch(Exception e){
					
					//Toast.makeText(getBaseContext(), "No server connection", Toast.LENGTH_LONG).show();
					e.printStackTrace();
					}
					
					
					try{
						
						BufferedReader reader = new BufferedReader(new InputStreamReader( deleteStream, "iso-8859-1"),8); 
						StringBuilder sb = new StringBuilder();
			        	sb.append(reader.readLine() + "\n");
			        	String line="0";
			        	
			        	while((line =reader.readLine()) !=null){
			        		
			        		sb.append(line + "\n");
			        		
			        	}
						
			        	deleteStream.close();
			        	
			        	deleteResult=sb.toString();
			        	
						
						
					}catch(Exception e){
						
						//Toast.makeText(NewProfile.this, "Error in string convertion", Toast.LENGTH_LONG).show();
						Log.e("log_tag", "Error converting result "+e.toString());
					}
					
				}
				
				
				
			}
			return deleteResult;
		}
		
		
		public void onPostExecute(String deleteData){
			
			if(dialog!=null){
				
				dialog.dismiss();
				dialog=null;
				
				
			}
			
			String data=deleteData.trim();
				System.out.println("deleteData "+deleteData);
							
			if(data.equalsIgnoreCase("true")){
				
				 
				  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
			        String user_id=loginSettings.getString("user_id", null);
			        String auto_user_id= loginSettings.getString("auto_user_id", null);
			        int registerCount = loginSettings.getInt("register_count", 0);
			        String main_user_id=loginSettings.getString("main_user_id", null);
			        String main_user_name=loginSettings.getString("oneTimeName", null);
			        String profileName = ProfileName.getText().toString().trim();
			        SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
			        SharedPreferences.Editor prefEditor = deleteSettings.edit(); 
			        
			        prefEditor.putString("user_name", null);  
	                prefEditor.putString("password", null); 
	                prefEditor.putString("user_id", null);
	                prefEditor.putInt("auto", 0);
	                prefEditor.putInt("update", 1);
	                if(profileName.equals(main_user_name)){
	                	prefEditor.putInt("register_count", registerCount-1);
	                }
	                
	                
	                	if(registerCount<=1 && user_id.equals(main_user_id)){
	                		prefEditor.putInt("oneTimeCheck",0);
	                		prefEditor.putString("oneTimeName",null);
	                		prefEditor.putString("main_user_id", null);
	                		prefEditor.putInt("register_count", 0);
	                	}
	                	
	                	
			        if(user_id.equals(auto_user_id)){
			        	
			        	
			        	
			        	prefEditor.putString("auto_user_name", null);  
		                prefEditor.putString("auto_password", null); 
		                prefEditor.putString("auto_user_id", null);
			        	
			        }
			        
			        prefEditor.commit();
			        
			        
			        Uri listUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL + "/" + user_id);
			        
			        Cursor cursor = getContentResolver().query(listUri, null, null, null, null);
			        if(cursor!=null && cursor.getCount()>0){
			        	
			        	getContentResolver().delete(listUri, null, null);
			        	
			        }else{
			        	
			        	
			        }
			        cursor.close();
			        
			        MainActivity parent;
        	    	parent =(MainActivity) NewProfile.this.getParent();
        	    	parent.changeTab();
			        
							
			}else{
				alertFunction("Unable to delete",0);
				//Toast.makeText(NewProfile.this, "Unable to delete", Toast.LENGTH_SHORT).show();
			}
			
			
		}
		  
	  }
	  
	  public void onResume(){
			super.onResume();
			
			//com.facebook.Settings.publishInstallAsync(this, "659621604053010");
		    
			
			MainActivity parent;
        	parent =(MainActivity) this.getParent();
        	parent.manipulateTitle("My profile");
			
        	deleteCheck =0;
			
			
			if(profilePicIntent==1){
				
				profilePicIntent=0;
				
			}
			
			if(profileLogoIntent==1){
				
				profileLogoIntent=0;
				
			}
			
			SharedPreferences itemSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor prefEditor = itemSettings.edit();  
			prefEditor.putInt("number", 4); 
			prefEditor.commit(); 
			int save=0;;
			int login=0;
			String user;
			
			SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			  
			 save =loginSettings.getInt("profile", 0);
			 login=loginSettings.getInt("login", 0);
			 String register=loginSettings.getString("register", null);
			 user=loginSettings.getString("user_id", null);
			 int check=loginSettings.getInt("checkFirstTime", 0);
			 int position=loginSettings.getInt("number", 0);
			 String auto_user_id=loginSettings.getString("auto_user_id", null);
			 
			 //system.out.println("number "+position);
			 //system.out.println();
			 //system.out.println();
			 
			 
			 //to clear previous data for another user login
			 if(async==1&&check==1){
				 
				 
				 
				 SharedPreferences profileSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					SharedPreferences.Editor editor = profileSettings.edit();  
					editor.putInt("checkFirstTime", 0);
					editor.commit(); 
					
					clearData();
			 }else if(register!=null){
				 
				//to clear previous data for another user register
				 
				 SharedPreferences profileSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					SharedPreferences.Editor editor = profileSettings.edit();  
					editor.putString("register", null);
					editor.commit();
					
					clearData();
					
			 }else{
				 

				 	SharedPreferences profileSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					SharedPreferences.Editor editor = profileSettings.edit();  
					editor.putInt("save", 0);
					editor.commit();
				 
			 }
			 
			
		 connection=haveNetworkConnection(); 
		 
		 if(connection ==false){
			 alertFunction("No Internet Connection", 1);
			 //Toast.makeText(NewProfile.this, "No network connection available", Toast.LENGTH_LONG).show();
		 }else{
			 
			 //for auto login if no 1 is loged in
			if(user==null &&auto_user_id!=null){
				
				SharedPreferences profileSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
				SharedPreferences.Editor editor = profileSettings.edit();  
				editor.putString("user_id", auto_user_id);
				editor.commit();
				
				clearData();
				serverConnection();
				 
			//to update profile	
			}else if(save==1){
				 
				 serverConnection();
				 
				 SharedPreferences profileSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					SharedPreferences.Editor editor = profileSettings.edit();  
					editor.putInt("profile", 0);
					//editor.putInt("doNothing", 0);
					editor.commit(); 
			 //}else if(async!=0&&login==1 &&user!=userId && userId!=null){
					//to update after login
			 	}else if(async!=0&&login==1){
			 		
			 		//system.out.println("login or register");
				 
				 serverConnection();
				 
				 SharedPreferences checkSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					String userId=checkSettings.getString("user_id", null);
					String auto_user=checkSettings.getString("auto_user_id", null);
				 
				 
				 SharedPreferences profileSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					SharedPreferences.Editor editor = profileSettings.edit();  
					editor.putInt("login", 0);
					//editor.putString("register", null);
					//editor.putInt("doNothing", 0);
					
					if(userId==null){
						
						  
						if(auto_user!=null){
							
							editor.putString("user_id", auto_user); 
							
						}
						
					editor.commit(); 
						
					}
			 }else if(register!=null){
				 
				 serverConnection();
				 
				 	SharedPreferences profileSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					SharedPreferences.Editor editor = profileSettings.edit();  
					editor.putString("register", null);
					editor.commit(); 
				 
			 }
		 }
		 
		 
		 /*if(save!=1){
			 
			 	SharedPreferences profileSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
				SharedPreferences.Editor editor = profileSettings.edit();  
				editor.putInt("doNothing", 1); 
				editor.commit();
			 
			 dataFill(myProfilelist);
		 }*/
			
		 //bumpServiceStart();
	  }
	  
	 /* private void bumpServiceStart(){
			
			SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	        int bumpActive= deleteSettings.getInt("bump", 0);
	        int recieverActive=deleteSettings.getInt("reciever", 0);
	        
	        if(bumpActive==0){
	        	
	        	System.out.println("going to start bump service");
	        	
	        	MainActivity parent;
	        	parent =(MainActivity) this.getParent();
	        	parent.bumpServiceStart();
	        	
	        	
	        }	
		}
	  */
	  
	  @SuppressWarnings("unchecked")
	@Override
		public Object onRetainNonConfigurationInstance() {
		    //final ArrayList<HashMap<String, String>> prev_list = mylist;
		    //keepPhotos(list);
			
			if(details!=null && async!=1){
				myProfilelist=(ArrayList<HashMap<String, String>>) details;
			}
		    return myProfilelist;
		}
	  
	  
	  private boolean haveNetworkConnection() {
		    boolean haveConnectedWifi = false;
		    boolean haveConnectedMobile = false;

		    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		    NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		    for (NetworkInfo ni : netInfo) {
		        if (ni.getTypeName().equalsIgnoreCase("WIFI"))
		            if (ni.isConnected())
		                haveConnectedWifi = true;
		        if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
		            if (ni.isConnected())
		                haveConnectedMobile = true;
		    }
		    return haveConnectedWifi || haveConnectedMobile;
		}
	  
	  
	public void browse(View view){

		
		
		
		switch(view.getId()){
		
			case R.id.bussiness_company_logo:
				
				profileLogoIntent=1;
					
					Intent i = new Intent(Intent.ACTION_PICK,
		               android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					i.putExtra("crop", "true");
					i.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri(1));
					i.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
					startActivityForResult(i, REQUEST_EDIT_FILE); 
	       
		       
					break;
					
			case R.id.bussiness_pic:
				
				
				profilePicIntent=1;
				
				
				Intent intnt = new Intent(Intent.ACTION_PICK,
			               android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				intnt.putExtra("crop", "true");
				intnt.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri(0));
				intnt.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
						startActivityForResult(intnt, REQUEST_EDIT_IMAGE); 
					
				break;
				
			case R.id.change_video:	
				
				
				videoDisplay=!videoDisplay;
				if(videoDisplay){
					
					Toast.makeText(this, "Enter the new youtube link in the text field given below ", Toast.LENGTH_LONG).show();
					newVideo.setVisibility(View.VISIBLE);
					
				}else{
					newVideo.setVisibility(View.GONE);
					
				}
				
				
				
				break;
		}   
	}
	
	
	private Uri getTempUri(int check) {
	    return Uri.fromFile(getTempFile(check));
	}
	
	File cropFile;
	private File getTempFile(int numb) {
	    if (isSDCARDMounted()) {

	    	
	    File bsRoot = new File(Environment.getExternalStorageDirectory(), "DigCrop");
	    if (!bsRoot.exists()) {
	    	bsRoot.mkdirs();
	    }
	    if(numb == 0){
	    	cropFile = new File(bsRoot, "profile"+System.currentTimeMillis()+"crop.jpg");
	    }else{
	    	cropFile = new File(bsRoot, "logo"+System.currentTimeMillis()+"crop.jpg");
	    }
	    
	    
	    try {
	    	cropFile.createNewFile();
	    } catch (IOException e) {

	    }
	    return cropFile;
	    } else {
	    return null;
	    }
	    }

	    private boolean isSDCARDMounted(){
	    String status = Environment.getExternalStorageState();
	    if (status.equals(Environment.MEDIA_MOUNTED))
	    return true;
	    return false;
	    }
	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
	    super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 

	    switch(requestCode) { 
			  case REQUEST_EDIT_FILE:
				  
				  profileLogoIntent=0;
				  
			        if(resultCode == RESULT_OK){  
			        	Uri selectedLogo = Uri.fromFile(cropFile);
				           System.out.println(selectedLogo);
				           
			               useDefaultLogo=0;
			            try {
			            	

					           String selectedLogoPath = cropFile.getAbsolutePath();
			            	
			            	
			            	
			            	if (selectedLogoPath != null) {
			                	
		                	      fileImagePath = selectedLogoPath;
		                	      
		                	      String[] tokens = fileImagePath.split("/");
		                	      
		                	      int size=tokens.length;
		                	      
		                	      fileImagePath=tokens[size-1];
		                	      
		                	      
		                	      
		                	      
		                	      boolean logoName=checkPicName(fileImagePath);
		                	      
		                	      if(!logoName){
		                	    	  
		                	    	  fileImagePath="imagename.jpg";
		                	      }
		                	
		                } else {
		                	
		                	//Toast.makeText(getApplicationContext(), "Unknown Logo path", Toast.LENGTH_LONG).show();
		               }
			            	
			            	
			            	logo =decodeUri(selectedLogo);
			            	//companyEditLogo.getLayoutParams().height = 100;
			            	//companyEditLogo.getLayoutParams().width = 100;
			            	//companyEditLogo.setLayoutParams(layoutParams);
			            	if(logo.getWidth()>100 ||logo.getHeight()>100){
			    				companyEditLogo.getLayoutParams().height = 100;
			    				companyEditLogo.getLayoutParams().width = 100;
			    			}
			            	companyEditLogo.setImageBitmap(logo);
			            	
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			        }
			        
			        break;
			    
			  case REQUEST_EDIT_IMAGE:   
				  
				  profilePicIntent=0;
				  
				  
				  if(resultCode == RESULT_OK){  
					  Uri selectedImage = Uri.fromFile(cropFile);
			            useDefaultPic=0;
			           
			            try {
			            
			             
			            	String selectedImagePath = cropFile.getAbsolutePath();
			            
			            
		                
				                if (selectedImagePath != null) {
				                	
				                	      filePath = selectedImagePath;
				                	      
				                	      String[] tokens = filePath.split("/");
				                	      
				                	      int size=tokens.length;
				                	      
				                	      filePath=tokens[size-1];
				                	      
				                	      
				                	      
				                	      boolean picName=checkPicName(filePath);
				                	      if(!picName){
				                	    	  
				                	    	  filePath="imagename.jpg";
				                	      }
				                	      
				                	
				                } else {
				                	
				                	//Toast.makeText(getApplicationContext(), "Unknown Image path", Toast.LENGTH_LONG).show();
				               }
			            
			                
							pic=decodeUri(selectedImage);
							//system.out.println(pic);
							//profileEditPic.getLayoutParams().height = 100;
							//profileEditPic.getLayoutParams().width = 100;
							//profileEditPic.setLayoutParams(layoutParams);
							if(pic.getWidth()>100 ||pic.getHeight()>100){
								profileEditPic.getLayoutParams().height = 100;
								profileEditPic.getLayoutParams().width = 100;
			    			}
							profileEditPic.setImageBitmap(pic);
							
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			        }
			        break;
	    }
	}
	
	private boolean checkPicName(String name){
		
		
		Pattern pattern = Pattern.compile("^[a-zA-Z0-9\\.]+$");
		Matcher matcher = pattern.matcher(name);
		return matcher.matches();
		
	}
	
	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException{
		
		// TODO Auto-generated method stub
		//system.out.println("On profile pic result scaling function");
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);
		
		 final int REQUIRED_SIZE = 80;
		 int width_tmp = o.outWidth;
		 int height_tmp = o.outHeight;
		 
		 int scale = 1;
		 
		 while (true) {
	            if (width_tmp / 2 < REQUIRED_SIZE
	               || height_tmp / 2 < REQUIRED_SIZE) {
	                break;
	            }
	            width_tmp /= 2;
	            height_tmp /= 2;
	            scale *= 2;
	        }
		 
		 BitmapFactory.Options o2 = new BitmapFactory.Options();
		 o2.inSampleSize = scale;
		 return BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);
		 
		
	}
	
	
	
	
	
public void validation(){
		
		boolean emailCheck,numberCheck,videoCheck,urlCheck,nameCheck;
		String card_email,card_name,card_company_name,card_designation,card_address,card_number,card_video, card_discription,card_comment,card_url;
		int success=0,valid=0,ready=0;
		
		card_email=ProfileEmail.getText().toString();
		card_name=ProfileName.getText().toString();
		card_discription=profileDescription.getText().toString();
		card_company_name=CompanyName.getText().toString();
		card_designation=Designation.getText().toString();
		card_address=CompanyAddress.getText().toString();
		card_number=ProfileNumber.getText().toString();
		card_video=newVideo.getText().toString();
		card_comment=comments.getText().toString();
		card_url=webUrl.getText().toString();
		card_discription=profileDescription.getText().toString();
		
		nameCheck=checkName(card_name);
		emailCheck=checkEmail(card_email);
		numberCheck=checkNumber(card_number);
		videoCheck=checkVideo(card_video);
		urlCheck=checkUrl(card_url);
		
		
		
		
		/* if(c_pic!=(String) map.get("pic")){
		   
		   profilePicChange=1;
		   
	   }else{
		   
		   profilePicChange=0;
	   }
	   
	   System.out.println("previous c_logo "+c_logo);
	   
	   if(c_logo!=(String) map.get("logo")){
		   
		   profileLogoChange=1;
		   
	   }else{
		   
		   profileLogoChange=0;
	   }*/
		
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
		String previouslyEncodedLogoImage=loginSettings.getString("profileLogo", null);
		String previouslyEncodedPicImage=loginSettings.getString("profilePic", null);
		
		
		profileLogoChange=1;
		profilePicChange=1;
		
		
		
		if(previouslyEncodedLogoImage!=null &&freshLogoBy!=null){
			
			if(previouslyEncodedLogoImage.equals(freshLogoBy)){
				
				
				
				profileLogoChange=0;
				
			}else{
				
				
				
				profileLogoChange=1;
			}
		}else{
			
			
			
			profileLogoChange=1;
		}
		
		
		//System.out.println("c_logo "+c_logo);
		//System.out.println("previous_logo "+previous_logo);
		
		
		if(previouslyEncodedPicImage!=null && freshImageBy!=null){
			
			if(previouslyEncodedPicImage.equals(freshImageBy)){
				
				
				 profilePicChange=0;
				 
			}else{
				
				
				
				profilePicChange=1;
				 
			}
			
		}else{
			
			
			profilePicChange=1;
		}
		
		
		
		//System.out.println("c_pic "+c_pic);
		//System.out.println("previous_pic "+previous_pic);
		
		
		if(card_name.equalsIgnoreCase("")){
			makeToast("Name");
			success=0;
		}else if(nameCheck==false){
			alertFunction("Enter a valid name",1);
			//Toast.makeText(NewProfile.this, "Please enter a valid name ", Toast.LENGTH_SHORT).show();
			success=0;
		}else if(card_email.equalsIgnoreCase("")){
			makeToast("Email Address");
			success=0;
		}else if(emailCheck==false){
			alertFunction("Enter a valid email address",1);
			//Toast.makeText(NewProfile.this, "Please enter a valid email address", Toast.LENGTH_SHORT).show();
			success=0;
		}else if(card_number.equalsIgnoreCase("")){
			makeToast("Number");
			success=0;
		}else if(numberCheck==false){
			alertFunction("Enter a valid number",1);
			//Toast.makeText(NewProfile.this, "Please enter only numbers in Phone field", Toast.LENGTH_SHORT).show();
			success=0;
		}else{
			success=1;
			
		}/*else if(card_company_name.equalsIgnoreCase("")){
			makeToast("Company Name");
			success=0;
		}else if(card_designation.equalsIgnoreCase("")){
			makeToast("Title");
			success=0;
		}else if(card_address.equalsIgnoreCase("")){
			makeToast("Address");
			success=0;
		}else if(card_discription.equalsIgnoreCase("")){
			makeToast("Description");
			success=0;
		}else if(card_comment.equalsIgnoreCase("")){
			//makeToast("Comments");
			success=1;
		}*/
		
		 if(card_url.equalsIgnoreCase("")){
			
			 valid=1;
		}else if(urlCheck ==false){
			alertFunction("Enter a valid Web Address",1);
			//Toast.makeText(NewProfile.this, "Enter a valid Web Address.", Toast.LENGTH_SHORT).show();
			valid=0;
		}else{
			
			valid=1;
			
		}
		
		if(card_video.equalsIgnoreCase("")){
			
			ready=1;
		}else if(videoCheck ==false){
			alertFunction("Video ID is missing.Enter a valid youtube video URL with 'v' parameter",1);
			//Toast.makeText(NewProfile.this, "Video ID is missing.Enter a valid youtube video URL.", Toast.LENGTH_SHORT).show();
			ready=0;
		}else{
			
			ready=1;
			
			Uri uri = Uri.parse(card_video);
			
			try{
				
				 videoId =  uri.getQueryParameter("v");
				 
				
			}catch(Exception e){
				
				
				Log.i("Register", "no  id");
			}
			
		}
		
		if(success==1 && ready==1 && valid==1){
		
			
			
			UpdateTask upTask = new UpdateTask(this);
			//upTask.execute(new String[] {"http://dev-fsit.com/iphone/updateprofile.php"});
			//upTask.execute(new String[] {"http://dev-fsit.com/iphone/digcard/updateprofile.php"});
			//upTask.execute(new String[] {"http://192.168.1.187/android/updateprofile.php"});
			upTask.execute(new String[] {"http://digcardapp.com/app/updateprofile.php"});
			bumpDisable();
			
		}
		
	}
	

	private boolean checkName(String name){
		
		Pattern pattern = Pattern.compile("^[a-zA-Z \\.]+$");
		Matcher matcher = pattern.matcher(name);
		return matcher.matches();
		
		
		//return connection;
		
	}
	private boolean checkUrl(String url){
		
		
		
		
		if (!url.startsWith("https://") && !url.startsWith("http://")){
		    url = "http://" + url;
		}
	
	
	return URLUtil.isValidUrl(url);
	
		/*String regex = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
	
		//Pattern pattern = Pattern.compile(regex);
		Pattern patt = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = patt.matcher(url);
		//return matcher.matches();
		return true;*/
	
	
	}

	private boolean checkVideo(String video){
	
		String video_path = video;
		Uri uri = Uri.parse(video_path);
		int count=0;
		try{
			
			String	videoId =  uri.getQueryParameter("v");
			 count=videoId.length();
			
		}catch(Exception e){
			
			Log.i("Register", "no video id");
		}
		
		if(count > 0){
			
			return true;
			
		}else{
			
			return false;
		}
	
	
}

	private boolean checkNumber(String number){
		if(number.length()<=15){
			Pattern pattern = Pattern.compile("^[0-9]+$");
			Matcher matcher = pattern.matcher(number);
			return matcher.matches();
		}else{
			return false;
		}
		
		
		
	}

	private boolean checkEmail(String email){
		
		
		//final String regularExp = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		final String MAIL_REGEX = "([_A-Za-z0-9-]+)(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})";
		
		Pattern pattern = Pattern.compile(MAIL_REGEX);
	    Matcher matcher = pattern.matcher(email);
	    return matcher.matches();
	
		
	}
	
	private void makeToast(String str){
		
		//String notice="Please fill the "+str+" field";
		
		//Toast.makeText(NewProfile.this, notice, Toast.LENGTH_SHORT).show();
		
		alertFunction("Please enter the required fields",1);
	}
	
	
	
	
	
	private class UpdateTask extends AsyncTask<String, Integer, ArrayList<String>>{
		String ba1,bytePic=null;
		private ProgressDialog updateDialog;
		private Context upContext;
		byte [] ba,imageBy;
		public UpdateTask(Context context) {
			// TODO Auto-generated constructor stub
			
			upContext = context;
			updateDialog = new ProgressDialog(upContext);
			//updateDialog.setTitle("Update");
			updateDialog.setMessage("Updating..");
			updateDialog.setCancelable(false);
           // loadingDialog.setMax(100);
			updateDialog.show();
			
		}


		@Override
		protected ArrayList<String> doInBackground(String... urls) {
			// TODO Auto-generated method stub
			String card_email,card_name,card_company_name,card_designation,card_address,card_number,card_video, card_discription, card_webUrl, card_comments,card_office,card_fax;
			
			card_email=ProfileEmail.getText().toString().trim();
			card_name=ProfileName.getText().toString().trim();
			card_discription=profileDescription.getText().toString().trim();
			card_company_name=CompanyName.getText().toString().trim();
			card_designation=Designation.getText().toString().trim();
			card_address=CompanyAddress.getText().toString().trim();
			card_number=ProfileNumber.getText().toString().trim();
			card_video=newVideo.getText().toString().trim();
			card_webUrl=webUrl.getText().toString().trim();
			card_comments= comments.getText().toString().trim();
			card_office=office.getText().toString().trim();
			card_fax=fax.getText().toString().trim();
			
			
			
			
			
			if(TextUtils.isEmpty(card_video)){
				
				videoId=c_video;
			}
		
			if(videoId==null){
				
				videoId="";
			}
			
			//MyAppData appState = ((MyAppData)getApplication());
			
			SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			
			String previouslyEncodedLogoImage=loginSettings.getString("profileLogo", "null");
			String previouslyEncodedPicImage=loginSettings.getString("profilePic", "null");
     		//SharedPreferences.Editor prefEditor = loginSettings.edit();  
     		//prefEditor.putInt("profile", 1); 
			
			
					
			
			
			if(useDefaultLogo==0){
				if(logo !=null){
					
					
					
					ByteArrayOutputStream bao = new ByteArrayOutputStream();
					logo.compress(Bitmap.CompressFormat.JPEG, 90, bao);
					ba = bao.toByteArray();
					ba1=Base64.encodeBytes(ba);
					
					
					
					
				}else if(logoBitmap !=null){
					
					

					
					
					
					if(previouslyEncodedLogoImage!=null&&profileLogoChange==0){
						
						//byte[] b = Base64.decode(previouslyEncodedLogoImage);
						
						try {
							ba=Base64.decode(previouslyEncodedLogoImage.getBytes());
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}else{
						
						
						
						ByteArrayOutputStream bao = new ByteArrayOutputStream();
						//Bitmap bitmap = ((BitmapDrawable)logoDrawable).getBitmap();
						logoBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);
						ba = bao.toByteArray();
						ba1=Base64.encodeBytes(ba);
						

						
						
					}
					
					
					
					
					
				}else{
					
					ByteArrayOutputStream bao = new ByteArrayOutputStream();
					logoThumb.compress(Bitmap.CompressFormat.JPEG, 90, bao);
					ba = bao.toByteArray();
					 ba1=Base64.encodeBytes(ba);
					
				}
			}else{
				ByteArrayOutputStream bao = new ByteArrayOutputStream();
				logoThumb.compress(Bitmap.CompressFormat.JPEG, 90, bao);
				ba = bao.toByteArray();
				ba1=Base64.encodeBytes(ba);
			}
			
			
			if(useDefaultPic ==0){
				if(pic !=null){
					
					
					
					ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
					pic.compress(Bitmap.CompressFormat.JPEG, 90, byteArray);
					imageBy = byteArray.toByteArray();
					bytePic=Base64.encodeBytes(imageBy);
					
					
					
					
					//appState.setProfileImageByteArray(imageBy);
					//System.out.println("new :"+bytePic);
					}else if(imageBitmap !=null){
						
						//bytePic=myProfile_pic;
						
						
						//byte[] array=appState.getProfileImageByteArray();
						
						if(previouslyEncodedPicImage!=null&&profilePicChange==0){
							
							
							
							try {
								imageBy=Base64.decode(previouslyEncodedPicImage.getBytes());
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
						}else{
							
							
						
							ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
							//Bitmap bitmap = ((BitmapDrawable)imageDrawable).getBitmap();
							imageBitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArray);
							imageBy = byteArray.toByteArray();
							
							
							bytePic=Base64.encodeBytes(imageBy);
							
							
							 
						
						}
						
					}else{
						ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
						pPic.compress(Bitmap.CompressFormat.JPEG, 90, byteArray);
						imageBy = byteArray.toByteArray();
						bytePic=Base64.encodeBytes(imageBy);
						
														
						
					}
			}else{
				
				ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
				pPic.compress(Bitmap.CompressFormat.JPEG, 90, byteArray);
				imageBy = byteArray.toByteArray();
				bytePic=Base64.encodeBytes(imageBy);
			}
			
			
			
			
			
			
			for (String url : urls) {
				
				
				
				try {
					
					
					//system.out.println(" update http post");
					DefaultHttpClient httpClient = new DefaultHttpClient();
					HttpContext localContext = new BasicHttpContext();

					HttpPost httpPost = new HttpPost(url);
					//system.out.println("url :"+url);
					
					SharedPreferences checkSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
					userId=checkSettings.getString("user_id", null);
						
					System.out.println("user Id to update "+userId);
					
					if(imageBy==null){
						
						
						
					}else{
						
						
						
					}
					
					if(ba==null){
						
						
						
					}else{
						
						
						
					}
					
					
					if(filePath==null){
						String haystack = c_pic;
		    			String needle = "default.jpg";
		    			
		    			int index = haystack.indexOf(needle);
		    			
		    			if (index != -1){
		    				filePath="default.jpg";
		    			}else{
		    				filePath="imagename.jpg";
		    			}
						
					}
					
					if(fileImagePath==null){
						String haystack = c_logo;
		    			String needle = "default.jpg";
		    			
		    			int index = haystack.indexOf(needle);
		    			
		    			if (index != -1){
		    				fileImagePath="default.jpg";
		    			}else{
		    				fileImagePath="imagename.jpg";
		    			}
						
					}
					
					
					MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
					
					FormBodyPart bodyPart=new FormBodyPart("user_id", new StringBody(userId));
								 			entity.addPart(bodyPart);			
								 bodyPart=new FormBodyPart("name", new StringBody(card_name));
								 			entity.addPart(bodyPart);
								 bodyPart=new FormBodyPart("email", new StringBody(card_email));
								 			entity.addPart(bodyPart);
								 bodyPart=new FormBodyPart("companyname", new StringBody(card_company_name));
								 			entity.addPart(bodyPart);
								 bodyPart=new FormBodyPart("designation", new StringBody(card_designation));
								 			entity.addPart(bodyPart);
								 bodyPart=new FormBodyPart("companyAddr", new StringBody(card_address));
								 			entity.addPart(bodyPart);
								 bodyPart=new FormBodyPart("phno", new StringBody(card_number));
								 			entity.addPart(bodyPart);
					 			 bodyPart=new FormBodyPart("description", new StringBody(card_discription));
								 			entity.addPart(bodyPart);
								 bodyPart=new FormBodyPart("video", new StringBody(videoId));
								 			entity.addPart(bodyPart);
								 bodyPart=new FormBodyPart("comment", new StringBody(card_comments));
								 			entity.addPart(bodyPart);
								 bodyPart=new FormBodyPart("web_url", new StringBody(card_webUrl));
								 			entity.addPart(bodyPart);
								 bodyPart=new FormBodyPart("office", new StringBody(card_office));
								 			entity.addPart(bodyPart);	
								 bodyPart=new FormBodyPart("fax", new StringBody(card_fax));
								 			entity.addPart(bodyPart);			
					entity.addPart("profile", new ByteArrayBody(imageBy,filePath));
					entity.addPart("company", new ByteArrayBody(ba,fileImagePath));
					
					httpPost.setEntity(entity);
					
					
					
			
					
					HttpResponse execute = httpClient.execute(httpPost,localContext);
					//system.out.println("update http post response");
					HttpEntity rentity     = execute.getEntity();
					to = rentity.getContent();
					

				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
				try{
					
					String line = "";
				     StringBuilder total = new StringBuilder();
				     // Wrap a BufferedReader around the InputStream
				     BufferedReader rd = new BufferedReader(new InputStreamReader(to, "iso-8859-1"),8);
				     // Read response until the end
				     try {
				      while ((line = rd.readLine()) != null) { 
				    	  //system.out.println("total");
				        total.append(line); 
				      }
				     } catch (IOException e) {
				      e.printStackTrace();
				     }
				     
					//system.out.println("http response string");
					
					
					
		        	//sb.append(reader.readLine());
		        	
		        	
		        		
		        	
					
		        	to.close();
		        	 data=total.toString();
		        	
					
					
				}catch(Exception e){
					Log.e("log_tag", "Error converting result "+e.toString());
				}
				
				try{
					
					//system.out.println("json function");
					
					JSONArray jArray = new JSONArray(data);
					//result=jArray[0];
					JSONObject json_data;
					
					response=new ArrayList<String>();
					
					for(int i=0; i<jArray.length(); i++){
						
						json_data=jArray.getJSONObject(i);
						String state=json_data.getString("flag");
						
						if(state.equals("true")){
							
							String id=json_data.getString("user_id");
							String date=json_data.getString("date");
							response.add(state);
							response.add(id);
							response.add(date);
							
						}else{
							
							response.add(state);
						}
						
						
						//system.out.println("response[0] == "+response.get(0));
						//system.out.println("response[1] == "+response.get(1));
						//system.out.println("response[2] == "+response.get(2));
						
					}
					
					
				}catch(JSONException ep){
					
					Log.e("log_tag", "Error JAson result "+ep.toString());
					
		        	//Toast.makeText(getBaseContext(), "No Data Found", Toast.LENGTH_LONG).show();
		        	
		        }catch (ParseException e) {
		        	// TODO Auto-generated catch block
		        	
					e.printStackTrace();
					
				}
				
	}
	return response;
}
		
		
		protected void onProgressUpdate(Integer... values){
			 updateDialog.show();
			
        }
		
		@Override
		protected void onPostExecute(ArrayList list) {
			
			bumpEnable();
			
			try {
				
				updateDialog.dismiss();
				updateDialog = null;
				
		    } catch (Exception e) {
		        
		    }
			
			String data=(String)list.get(0);
			if(data.equals("true"))
            {
             Log.w("SENCIDE", "TRUE");
             
             
             SharedPreferences shre = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			 SharedPreferences.Editor prefEditor = shre.edit();  
			 prefEditor.putString("profileLogo",ba1);
			 prefEditor.putString("profileLogoName",fileImagePath);
			 prefEditor.putString("profilePic",bytePic);
			 prefEditor.putString("profilePicName",filePath);
			 prefEditor.commit();
             
			 useDefaultPic =0;
			 useDefaultLogo =0;
			 newVideo.setText("");
			 newVideo.setVisibility(View.GONE);
			 videoDisplay=false;
			 
             SharedPreferences checkSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
      		int number=checkSettings.getInt("number", 3);
      		String userName=checkSettings.getString("user_name", null);
      		String password=checkSettings.getString("password", null);
      		
      		String card_email=ProfileEmail.getText().toString().trim();
      		String card_name=ProfileName.getText().toString().trim();
      		String card_discription=profileDescription.getText().toString().trim();
      		String card_company_name=CompanyName.getText().toString().trim();
      		String card_designation=Designation.getText().toString().trim();
      		String card_address=CompanyAddress.getText().toString().trim();
      		String card_number=ProfileNumber.getText().toString().trim();
      		String card_video=newVideo.getText().toString().trim();
      		String card_webUrl=webUrl.getText().toString().trim();
      		String card_comments= comments.getText().toString().trim();
      		String card_office=office.getText().toString().trim();
			String card_fax=fax.getText().toString().trim();
      		
      		
      		String user_id=(String)list.get(1);
 			String date=(String)list.get(2);
 			
 			System.out.println("user id "+user_id);
 			
 			//making imahe fron gallery to null
 			if(logo!=null){
 				
 				logo=null;
 				
 			}
 			 if(pic!=null){
 				 
 				 pic=null;
 				 
 			 } 
 			//fileImagePath = "imagename.jpg";
 			//filePath = "imagename.jpg";
 			
 			//String[] projection = new String[] {CardTable.KEY_USERID};
 			//Uri uri=CardManagingProvider.CONTENT_URI_DETAIL;
 			Uri queryUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL + "/" + user_id);
 			Cursor cursor = getContentResolver().query(queryUri, null, null, null, CardTable.KEY_USERID);
      		
 			ContentValues values=new ContentValues();
	 		values.put(CardTable.KEY_USERID, user_id);
	 		values.put(CardTable.KEY_NAME, card_name);
	 		values.put(CardTable.KEY_EMAIL, card_email);
	 		values.put(CardTable.KEY_NUMBER, card_number);
	 		values.put(CardTable.KEY_PROFILE_PIC, imageBy);
	 		values.put(CardTable.KEY_DESIGNATION, card_designation);
	 		values.put(CardTable.KEY_VIDEO, videoId);
	 		values.put(CardTable.KEY_COMPANY, card_company_name);
	 		values.put(CardTable.KEY_ADDRESS, card_address);
	 		values.put(CardTable.KEY_LOGO, ba);
	 		values.put(CardTable.KEY_DESCRIPTION, card_discription);
	 		values.put(CardTable.KEY_UPDATE_DATE, date);
	 		values.put(CardTable.KEY_COMMENTS, card_comments);
	 		values.put(CardTable.KEY_WEB_ADDRESS, card_webUrl);
	 		values.put(CardTable.KEY_OFFICE, card_office);
	 		values.put(CardTable.KEY_FAX, card_fax);
	 		
	 		//Uri queryUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL + "/" + user_id);
	 		
             
      		
	 		if ( cursor != null && cursor.getCount() > 0 ) {
	 		    if (cursor.moveToFirst()) {
	 		    	
	 		    	//system.out.println("cursor not null");
	 		    	getContentResolver().update(queryUri, values, null, null);
	 		    	
	 		    }
	 		   
	 		    
	 	  }else {
	 		 //system.out.println("cursor  null");
	 		 getContentResolver().insert(CardManagingProvider.CONTENT_URI_DETAIL, values);
	 		
	 		
	 	  }
	        
	        cursor.close();
	 		
      		
             
             //switchTabInActivity(1);
            SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
     		SharedPreferences.Editor preEditor = loginSettings.edit();  
     		preEditor.putInt("profile", 1); 
     		preEditor.putInt("update", 1); 
     		
     		if(autoLoginValue==1){
     			
     			//system.out.println("autoLoginValue "+autoLoginValue);
     			preEditor.putInt("auto", 1);
     			preEditor.putString("auto_user_id", userId);
     			preEditor.putString("auto_user_name", userName);
     			preEditor.putString("auto_password", password);
     			
     		}else{
     			//system.out.println("autoLoginValue "+autoLoginValue);
     			preEditor.putInt("auto", 0);
     			preEditor.putString("user_id", userId);
     			preEditor.putString("auto_user_id", null);
     			preEditor.putString("auto_user_name", null);
     			preEditor.putString("auto_password", null);
     		}
     		preEditor.commit(); 
     		
     		if(number==4){
     			
     			serverConnection();
     			
     		}else{
     			
     			//Toast.makeText(getApplication(), "Update Success", Toast.LENGTH_SHORT).show();
     			//alertFunction("Card Updated Successfully");
     		}
     		
     		if(number==1){
     			
     			MainActivity parent;
     	    	parent =(MainActivity) NewProfile.this.getParent();
     	    	parent.fetchCard();
     			
     		}else if(number ==5){
     			
     			MainActivity parent;
     	    	parent =(MainActivity) NewProfile.this.getParent();
     	    	parent.logOut();
     		}
             
     		//card_video=newVideo.getText().toString();
     		
     		/*if(!videoId.equals(c_video)){
     			
     			showThumbnail(videoId);
     		}*/
     		
     		
            /* Intent ntnt= new Intent(NewProfile.this, MainActivity.class);
             ntnt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
             startActivity(ntnt);
             */
             
            }else{
             Log.w("SENCIDE", "FALSE");
            // Toast.makeText(getApplication(), "Update Failed", Toast.LENGTH_SHORT).show();           
             alertFunction("Card Update Failed",0);
            }
	    	
	   
		}
		
	}
	
	private void switchTabInActivity(int i) {
		// TODO Auto-generated method stub
    	
    	MainActivity parent;
    	parent =(MainActivity) this.getParent();
    	parent.switchTab(i);
	}
	
	private int shouldSave(){
		
		int unSaved=0;
		
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
		int auto=loginSettings.getInt("auto", 0);
		
		String video=newVideo.getText().toString();
		String id=null;
		if(!TextUtils.isEmpty(video)){
			
			Uri uri=Uri.parse(video);
			
			try{
				 id=uri.getQueryParameter("v");
				
			}catch(Exception e){
				
			}
			
		}
		
		 
		 
		if(!c_name.equalsIgnoreCase(ProfileName.getText().toString().trim())){
			unSaved=1;
			
		}else if(!c_email.equalsIgnoreCase(ProfileEmail.getText().toString().trim())){
			unSaved=1;
			
		}else if(!c_company.equalsIgnoreCase(CompanyName.getText().toString().trim())){
			unSaved=1;
			
			
			
			
		}else if(!c_phone.equalsIgnoreCase(ProfileNumber.getText().toString().trim())){
			unSaved=1;
			
		}else if(!c_company_address.equalsIgnoreCase(CompanyAddress.getText().toString().trim())){
			unSaved=1;
			
		}else if(!c_designation.equalsIgnoreCase(Designation.getText().toString().trim())){
			unSaved=1;
			
		}else if(!c_desription.equalsIgnoreCase(profileDescription.getText().toString().trim())){
			unSaved=1;
			
		}else if(!c_comments.equalsIgnoreCase(comments.getText().toString().trim())){
			unSaved=1;
			
		}else if(!c_webUrl.equalsIgnoreCase(webUrl.getText().toString().trim())){
			unSaved=1;
			
		}else if(id!=null&&!c_video.equals(id)){
			
			
			
			unSaved=1;
			
		}else if(logo!=null){
			unSaved=1;
			
		}else if(pic!=null){
			
			unSaved=1;
			
		}else if(!c_office.equalsIgnoreCase(office.getText().toString().trim())){
			unSaved=1;
			
		}else if(!c_fax.equalsIgnoreCase(fax.getText().toString().trim())){
			unSaved=1;
			
		}else if(auto!=autoLoginValue){
			unSaved=1;
			
		}
		if(deleteCheck !=1){
			return unSaved;
		}else{
			return 0;
		}
		
		
		
	}
	
	
	public void onPause(){
		super.onPause();
		
		
		SharedPreferences profileSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
		int update=profileSettings.getInt("profile", 0);
		int searachUpdate=profileSettings.getInt("update", 0);
		//int number=profileSettings.getInt("number", 0);
		
		if(update==0){
			
			
			int save=shouldSave();
			
			
			if(save==1&&profilePicIntent==0&&profileLogoIntent==0){
				
				
				SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
				SharedPreferences.Editor prefEditor = loginSettings.edit();  
				prefEditor.putInt("save", 1);
				prefEditor.commit();
				
				AlertDialog alert=null;
				
				AlertDialog.Builder builder = new AlertDialog.Builder(NewProfile.this);
				builder.setCancelable(false);
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

				  public void onClick(DialogInterface dialog, int arg1) {
				  // do something when the OK button is clicked
					  
					 
					
					  dialog.dismiss();
					  
					  
		              validation();
		            SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		  			SharedPreferences.Editor prefEditor = loginSettings.edit();  
		  			prefEditor.putInt("save", 0);
		  			prefEditor.putInt("profile", 1);
		  			prefEditor.commit();
		  			//loginSettings.getInt("number", 3);
					  
					  
				  }});
				
				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					  public void onClick(DialogInterface dialog, int arg1) {
					  // do something when the OK button is clicked
						  
						  
						
						  dialog.dismiss();
						  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
							SharedPreferences.Editor prefEditor = loginSettings.edit();  
							prefEditor.putInt("save", 0);
							prefEditor.putInt("cancel", 1);
							prefEditor.commit();
							previousState();
						  
					  }});
				
				builder.setTitle("Warning");
				builder.setMessage("Do you want to update the card?");
				alert=builder.create();
				alert.show();
				
				alert.setOnDismissListener(new DialogInterface.OnDismissListener(){

					public void onDismiss(DialogInterface dialog) {
						// TODO Auto-generated method stub
						
						SharedPreferences settings = getSharedPreferences("Myprofile", MODE_PRIVATE);
						int cancel=settings.getInt("cancel", 0);
						
						SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
						SharedPreferences.Editor prefEditor = loginSettings.edit();  
						prefEditor.putInt("save", 0);
						prefEditor.putInt("cancel", 0);
						prefEditor.commit();
						
						if(cancel==1){
							
							previousState();
						}
						
						
					}});
				
				
			}
			
			
			
			/*if(save ==1){
				
				System.out.println("save ==1");
				return;
				
				
			}*/
			
		}/*else if(searachUpdate==1&&number==1){
			
			
			MainActivity parent;
 	    	parent =(MainActivity) NewProfile.this.getParent();
 	    	parent.fetchCard();
			
			
			
		}*/
		
		
		
	}
	
	
	public void clearData(){
		 
			  	 
		
		 ProfileName.setText("");
		  ProfileNumber.setText("");
		  Designation.setText("");
		  CompanyName.setText("");
		  CompanyAddress.setText("");
		  ProfileEmail.setText("");
		  profileDescription.setText("");
		  webUrl.setText("");
		  comments.setText("");
		  office.setText("");
		  fax.setText("");
		  
		  logo=null;
		  pic=null;
		  if(pPic.getWidth()>100 || pPic.getHeight()>100){
			  profileEditPic.getLayoutParams().width=100;
			  profileEditPic.getLayoutParams().height=100;
		  }
		  if(pLogo.getWidth()>100 || pLogo.getHeight()>100){
			  companyEditLogo.getLayoutParams().width=100;
			  companyEditLogo.getLayoutParams().height=100;
		  }
		  //companyEditLogo.getLayoutParams().width=100;
		  //companyEditLogo.getLayoutParams().height=100;
		  
		  //profileEditPic.setLayoutParams(layoutParams);
		  //companyEditLogo.setLayoutParams(layoutParams);
 		 profileEditPic.setImageBitmap(pPic);
 		 companyEditVideo.setImageBitmap(pVideo); 
		 companyEditLogo.setImageBitmap(pLogo);
		 fileImagePath = "imagename.jpg";
		 filePath = "imagename.jpg";
		 toggle.setChecked(false);
		 autoLoginValue=0;
 	
		
	}
	
	public void previousState(){
		
		  ProfileName.setText(c_name);
		  ProfileNumber.setText(c_phone);
		  Designation.setText(c_designation);
		  CompanyName.setText(c_company);
		  CompanyAddress.setText(c_company_address);
		  ProfileEmail.setText(c_email);
		  profileDescription.setText(c_desription);
		  webUrl.setText(c_webUrl);
		  comments.setText(c_comments);
		  office.setText(c_office);
		  fax.setText(c_fax);
		  useDefaultPic =0;
		  useDefaultLogo =0;
		  if(imageBitmap!=null){
			  if(imageBitmap.getWidth()>100 || imageBitmap.getHeight()>100){
				  profileEditPic.getLayoutParams().width=100;
				  profileEditPic.getLayoutParams().height=100;
			  }
			  
			  //profileEditPic.setLayoutParams(layoutParams);
  			profileEditPic.setImageBitmap(imageBitmap);
  			
  		}
		  fileImagePath = "imagename.jpg";
		  filePath = "imagename.jpg";
		  
		  if(mBitmap!=null){
			  
			  companyEditVideo.setImageBitmap(mBitmap); 
			  
		  }
		  

  		if(logoBitmap!=null){
  			if(logoBitmap.getWidth()>100 || logoBitmap.getHeight()>100){
  				companyEditLogo.getLayoutParams().width=100;
  			  companyEditLogo.getLayoutParams().height=100;
			  }
  			
			  //companyEditLogo.setLayoutParams(layoutParams);
  			companyEditLogo.setImageBitmap(logoBitmap);
  		}
			  
  		
  		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
  		int autoValue=loginSettings.getInt("auto", 0);
  		
  		if(autoValue==1){
  			
  			toggle.setChecked(true);
  			
  			autoLoginValue=1;
  			
  		}else{
  			
  			toggle.setChecked(false);
  			autoLoginValue=0;
  		}
  		
  		
  	//making image from gallery to null
			if(logo!=null){
				logo=null;
			}
			 if(pic!=null){
				 pic=null;
			 } 
  		
		
  		SharedPreferences profileSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);
		int number=profileSettings.getInt("number", 3);
		
		
		if(number==1){
 			
 			MainActivity parent;
 	    	parent =(MainActivity) NewProfile.this.getParent();
 	    	parent.fetchCardServer();
 			
 		}else if(number ==5){
 			
 			MainActivity parent;
 	    	parent =(MainActivity) NewProfile.this.getParent();
 	    	parent.logOut();
 		}
	}
	
	public boolean isTablet(Context context) {  
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)    
                							>= Configuration.SCREENLAYOUT_SIZE_LARGE; 
	}

	
	public String getPath(Uri uri) {
		
        String[] projection = { MediaStore.Images.Media.DATA };

        Cursor cursor = managedQuery(uri, projection, null, null, null);

        if (cursor != null) {

            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL

            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA

            int column_index = cursor

                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            cursor.moveToFirst();

            return cursor.getString(column_index);

        } else

            return null;

    }
	
	@Override
	public void onBackPressed() {
		
		
		SharedPreferences deleteSettings = getApplicationContext().getSharedPreferences("Myprofile", MODE_PRIVATE);  
	    int bumpActive= deleteSettings.getInt("bump", 0);
	    final int auto= deleteSettings.getInt("auto", 0);
	    int save=deleteSettings.getInt("save", 0);
	    
	   /* if(bumpActive == 1){
	    	
	    	MainActivity parent;
	    	parent =(MainActivity) this.getParent();
	    	parent.bumpServiceCancel(bumpActive);
	    	
	    }
		*/
		
		//super.onBackPressed();
	    
	    
	    if(save!=1){
	    	

			AlertDialog.Builder builder = new AlertDialog.Builder(NewProfile.this);
			builder.setTitle("Exit");
			builder.setCancelable(false);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  dialog.dismiss();
			        
				  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					SharedPreferences.Editor prefEditor = loginSettings.edit();  
					
					//if(auto==0){
						prefEditor.putString("user_name", null);  
					    prefEditor.putString("password", null);
					    prefEditor.putString("user_id", null);
						prefEditor.putInt("number", 3);
						
					/*}else{
						
						prefEditor.putInt("number", 0);
						
					}*/
					
					prefEditor.commit();
				  
				 finish();
				  
			  }});
			
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				  public void onClick(DialogInterface dialog, int arg1) {
				  // do something when the OK button is clicked
					  
					  dialog.dismiss();
					  
					  
						   return;
						
						
						
				        
					  
				  }});
			
			
			
			builder.setMessage("You want to exit application").create().show();
	    	
	    	
	    }
		
		
	}
	
	public static void alertFunction(String message,int check){
		AlertDialog alert=null;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			
			  dialog.dismiss();
			  
			  
		  }});
		if(check ==1){
			builder.setTitle("Warning");
		}
		builder.setMessage(message);
		alert=builder.create();
		alert.show();
		
	}
	
	public void cancel(View view){
		switch(view.getId()){
		
		case R.id.profile_logo_cancel:
			fileImagePath = "default.jpg";
			companyEditLogo.getLayoutParams().width=34;
			  companyEditLogo.getLayoutParams().height=34;
			 //companyEditLogo.setLayoutParams(defaultlayoutParams);
			companyEditLogo.setImageBitmap(logoThumb);
			useDefaultLogo=1;
			break;
		case R.id.profile_pic_cancel:
			//profileEditPic.setLayoutParams(defaultlayoutParams);
			filePath ="default.jpg";
			profileEditPic.getLayoutParams().width=34;
			profileEditPic.getLayoutParams().height=34;
			  profileEditPic.setImageBitmap(pPic);
			useDefaultPic=1;
			break;
		}
	}
	
	
}
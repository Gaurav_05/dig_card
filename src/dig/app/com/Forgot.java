package dig.app.com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;



import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Forgot extends Activity{
	
	EditText Email;
	Button Recover;
	String password=null;
	TextView Title;
	InputStream is=null;
	String result=null;
	boolean connection=false;
	//ForgotTask forgotTask ;
	String recipient ;
	ForgotTask forgotTask ;
	
	private RelativeLayout forgotLayout;
	
	int deviceType=0;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forgot);
		//System.out.println("recover dfsfs");
		
		MyAppData appState = ((MyAppData)getApplication());
		deviceType=appState.getTablet();
		/*if(deviceType==1){
			
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			
		}else{
			
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			
		}*/
		
		
		forgotLayout=(RelativeLayout) findViewById(R.id.forgot_layout);
		
		Email=(EditText) findViewById(R.id.recover_mail);
		Recover=(Button) findViewById(R.id.recover_password);
		Title=(TextView) findViewById(R.id.forgot_title);
		Title.setText("Login");
		
		Recover.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				
				recipient= Email.getText().toString().trim();
				//System.out.println(recipient);
				if(TextUtils.isEmpty(recipient)){
					//Toast.makeText(Forgot.this, "Enter Email Id", Toast.LENGTH_SHORT).show();
					alertFunction("Please Enter Email ID!");
				}else if(checkEmail(recipient)){
					
				// recoverPassword(recipient);
					
					connection=haveNetworkConnection();
				
					if(connection){
						
						//postLoginDetails(User, Pass);
						
						forgotTask= new ForgotTask(Forgot.this);
						//forgotTask.execute(new String[] {"http://dev-fsit.com/iphone/login.php"});
						//forgotTask.execute(new String[] {"http://dev-fsit.com/iphone/digcard/forgotpassword.php"});
							forgotTask.execute(new String[] {"http://digcardapp.com/app/forgotpassword.php"});
						
						
					}else{
						alertFunction("No Internet Connection");
						//Toast.makeText(Forgot.this, "No network connection available", Toast.LENGTH_LONG).show();
					}
					
				}else{
					
					//Toast.makeText(Forgot.this, "Enter a valid email id", Toast.LENGTH_LONG).show();
					alertFunction("Please enter valid Email ID!");
					
				}
				
			}

			
		});
		
	}
	
	
	
	public void onStart(){
		super.onStart();
		
		forgotLayout.requestFocus();
	}
	
	
	public void onResume(){
    	super.onResume();
    	
    	//com.facebook.Settings.publishInstallAsync(this, "659621604053010");
    }
	
	//checking for Internet connection
	private boolean haveNetworkConnection() {
	    boolean haveConnectedWifi = false;
	    boolean haveConnectedMobile = false;

	    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo[] netInfo = cm.getAllNetworkInfo();
	    for (NetworkInfo ni : netInfo) {
	        if (ni.getTypeName().equalsIgnoreCase("WIFI"))
	            if (ni.isConnected())
	                haveConnectedWifi = true;
	        if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
	            if (ni.isConnected())
	                haveConnectedMobile = true;
	    }
	    return haveConnectedWifi || haveConnectedMobile;
	}
	
	
	private class ForgotTask extends AsyncTask<String, Void, String>{
		
		
		private Context mContext;
        private ProgressDialog loadingDialog;

		public ForgotTask(Context context) {
			// TODO Auto-generated constructor stub
			 mContext = context;
	            loadingDialog = new ProgressDialog(mContext);
	           // loadingDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	            loadingDialog.setMessage("Processing..");
	            loadingDialog.setCancelable(false);
	           // loadingDialog.setMax(100);
	            loadingDialog.show();
			
		}

		@Override
		protected String doInBackground(String... urls) {
			// TODO Auto-generated method stub
			
			recipient= Email.getText().toString().trim();
			
			for (String url : urls) {
				
				try {	
					
					HttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost(url);
					
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
					nameValuePairs.add(new BasicNameValuePair("email", recipient));
		            
		            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		            
		            HttpResponse execute = httpclient.execute(httppost);
					//System.out.println("http post response");
					HttpEntity rentity     = execute.getEntity();
					is = rentity.getContent();
					
					
				}catch (Exception e) {
					e.printStackTrace();
				}
				
				
				try{
					
					String line = "";
				     StringBuilder total = new StringBuilder();
				     // Wrap a BufferedReader around the InputStream
				     BufferedReader rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"),8);
				     // Read response until the end
				     try {
				      while ((line = rd.readLine()) != null) { 
				    	 
				        total.append(line); 
				      }
				     } catch (IOException e) {
				      e.printStackTrace();
				     }				    				     									
		        	is.close();
		        	result=total.toString();
		        	
				}catch(Exception e){
					Log.e("log_tag", "Error converting result "+e.toString());
				}
				
			}
			
			return result;
		}
		
		protected void onProgressUpdate(Integer... values) {
			
			 loadingDialog.show();
			
	    }
		
		@Override
		protected void onPostExecute(String list) {
			
			try {
				loadingDialog.dismiss();
				loadingDialog = null;
		    } catch (Exception e) {
		        // nothing
		    }
		    
			
			
		    String data=list.trim();
		    
		    
		    
		    if(data.toString().equalsIgnoreCase("true")){
	           
		    	
		    	AlertDialog alert=null;
                //Toast.makeText(getApplication(), "Email does not exist", Toast.LENGTH_SHORT).show();
            	AlertDialog.Builder builder = new AlertDialog.Builder(Forgot.this);
            	builder.setCancelable(false);
        		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

        		  public void onClick(DialogInterface dialog, int arg1) {
        		  // do something when the OK button is clicked
        			
        			  dialog.dismiss();
        			  Email.setText("");
        			  
        		  }});
        		builder.setTitle("Congrats");
        		builder.setMessage("Your password is sent to the email ID.");
        		alert=builder.create();
        		alert.show();
	             //Toast.makeText(getApplication(), "Please check your email for login details", Toast.LENGTH_SHORT).show();
	            // finish();
	             
            }else{
            	AlertDialog alert=null;
                //Toast.makeText(getApplication(), "Email does not exist", Toast.LENGTH_SHORT).show();
            	AlertDialog.Builder builder = new AlertDialog.Builder(Forgot.this);
            	builder.setCancelable(false);
        		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

        		  public void onClick(DialogInterface dialog, int arg1) {
        		  // do something when the OK button is clicked
        			
        			  dialog.dismiss();
        			  
        			  
        		  }});
        		//builder.setTitle("Warning");
        		builder.setMessage("Email ID Not Found");
        		alert=builder.create();
        		alert.show();
            }
		    
		}    
		
	}
	
	
	
	
	
	private boolean checkEmail(String email){
		
		
		//final String regularExp = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		final String MAIL_REGEX = "([_A-Za-z0-9-]+)(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})";
		Pattern pattern = Pattern.compile(MAIL_REGEX);
	    Matcher matcher = pattern.matcher(email);
	    return matcher.matches();
	
		
	}
	
	
	public void alertFunction(String message){
		AlertDialog alert=null;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(Forgot.this);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			
			  dialog.dismiss();
			  
			  
		  }});
		builder.setTitle("Warning");
		builder.setMessage(message);
		alert=builder.create();
		alert.show();
		
	}
	
	
	/*private void recoverPassword(String recipient) {
		// TODO Auto-generated method stub
		
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost("http://dev-fsit.com/iphone/forgotpassword.php");
		//http://192.168.1.187/android/password.php
		
		try {
            // Add user name and password
        
          
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("email", recipient));
            
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            
            HttpResponse response = httpclient.execute(httppost);
            
            String str = inputStreamToString(response.getEntity().getContent()).toString();
            
            //Log.w("SENCIDE", str);
            
            if(str.toString().equalsIgnoreCase("true"))
            {
           //  Log.w("SENCIDE", "TRUE");
             Toast.makeText(getApplication(), "Please check your email for login details", Toast.LENGTH_SHORT).show();
             
             finish();
            }else
            {
               // Log.w("SENCIDE", "FALSE");
                Toast.makeText(getApplication(), "Invalid email", Toast.LENGTH_SHORT).show();           
               }
            
		}catch(ClientProtocolException e){
			 e.printStackTrace();
		}catch (IOException e) {
	         e.printStackTrace();
        }
		
	}

	private StringBuilder inputStreamToString(InputStream content) {
		// TODO Auto-generated method stub
		String line = "";
	     StringBuilder total = new StringBuilder();
	     // Wrap a BufferedReader around the InputStream
	     BufferedReader br = new BufferedReader(new InputStreamReader(content));
	     // Read response until the end
	     try {
	      while ((line = br.readLine()) != null) { 
	        total.append(line); 
	      }
	     } catch (IOException e) {
	      e.printStackTrace();
	     }
	     // Return full string
	     return total;
	}*/

}

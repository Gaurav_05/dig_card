package dig.app.com;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;



import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


@SuppressWarnings("serial")
public class NewFetch extends ListActivity implements Serializable,Runnable{

	private MyCustomAdapter mAdapter;
	
	private static ProgressDialog loadingDialog;
	private static AlertDialog mLoading = null;
	private Bitmap mLogo;
	private Bitmap icon;
	
	private byte [] byteGlobalArray;
	
	private byte [] byteGlobalLogoArray;
	
	public ArrayList<Boolean> checks=new ArrayList<Boolean>();
	//private NewCardList cardList;
	
	//public static List<String> imageList; 
	//public static List<String> logoList;
	private static final int DOWNLOADALL=0;
	private static final int SYNCHRONIZE=1;
	private static final int SYNCHRONIZE_NONE=2;
	private static final int DOWNLOAD=3;
	private static final int TASK_TIMEOUT=4;
	private static final int NORMAL=5;
	private static final int SEARCH=6;
	static ContentResolver resolver;
	 static Context context;
	private static ProgressDialog pd,progress;
	
	private static ProgressDialog bar;;
	boolean isRunning=false;
	Intent intnt;
	FetchCardList task ;
	
	int refreshProcess=0;
	 //private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
	
	//public static ArrayList<HashMap<String, String>> imageList; 
	//public static ArrayList<HashMap<String, String>> logoList; 
	public static ArrayList<HashMap<String, byte[]>> imageList;
	public static ArrayList<HashMap<String, byte[]>> logoList;
	ArrayList<HashMap<String, String>> myList = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> mySearchList;  
	ArrayList<HashMap<String, ArrayList<HashMap<String, String>>>> allMyList = new ArrayList<HashMap<String, ArrayList<HashMap<String, String>>>>();
	
	EditText fetchSearch;
	
	int tabPosition;
	File myDirectory;
	
	
	InputStream is=null;
	StringBuilder sb=null;
	String result=null;
	int row_id;
	String name,designation,company,company_adderss,phone,email,profile_pic,logo,video,description,updateDate,comments,web_address,office,fax;
	CheckBox check;
	private ListView mainListView = null;
	RelativeLayout bottom;
	Button bottom_download;
	//CoverFlow coverFlow;
	Object data=null;
	TextView Title;
	int async=0;
	ListView fetchListview;
	int orientation;
	private RelativeLayout fetchLayout;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fetch);
		
		
		context=this;
		resolver=context.getContentResolver();
		//cardList= new NewCardList();
		
		SharedPreferences itemSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		SharedPreferences.Editor prefEditor = itemSettings.edit();  
		prefEditor.putInt("number", 1); 
		prefEditor.putInt("fetch", 0);
		prefEditor.commit(); 
		
		
		switch (this.getResources().getConfiguration().orientation)
        {
        
        case Configuration.ORIENTATION_PORTRAIT:
        
        	
        	orientation=1;
        	break;
        	
        case Configuration.ORIENTATION_LANDSCAPE:
        	
        	orientation=2;
        	        	
        	break;
        	
        default:
        	  try {
				throw new Exception("Unexpected orientation enumeration returned");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	   	
        	
        }
        
		
		//image array initialization
		
			mLogo=BitmapFactory.decodeResource(NewFetch.this.getResources(), R.drawable.logo);
 		  icon=BitmapFactory.decodeResource(NewFetch.this.getResources(), R.drawable.profile_pic);
 		 ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
 		 ByteArrayOutputStream byteArrayLogoOutput = new ByteArrayOutputStream();
 		 icon.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutput);
 		 mLogo.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayLogoOutput);
 	      byteGlobalArray = byteArrayOutput.toByteArray();
 	      byteGlobalLogoArray = byteArrayLogoOutput.toByteArray();
		
		
		fetchLayout=(RelativeLayout) findViewById(R.id.fetch_layout);
		
		fetchListview=(ListView) findViewById(android.R.id.list);
		fetchSearch=(EditText) findViewById(R.id.fetch_search);
		
		fetchListview.requestFocus();
		
		 Title=(TextView) findViewById(R.id.title);
		 check=(CheckBox) findViewById(R.id.card_id);
		 
		 bottom=(RelativeLayout) findViewById(R.id.bottom_bar);
		 
		 SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	        String userName = loginSettings.getString("user_name", null);
	        String passWord = loginSettings.getString("password", null);
	        //int visible=loginSettings.getInt("check", 0);
	        tabPosition=loginSettings.getInt("number", 1);
	        
	       
	        
	        fetchSearch.clearFocus();
	        
	       fetchSearch.addTextChangedListener(new TextWatcher(){
	            public void afterTextChanged(Editable s) {
	             String searchData=  fetchSearch.getText().toString();
	             
	             
	            // if(orientation ==1 && tabPosition==1){
	             if(tabPosition==1){
	            	 
	            	 doSearch(searchData);
	            	 
	             }
	             
	             	
	             
	             	             
	            }
	            public void beforeTextChanged(CharSequence s, int start, int count, int after){}
	            public void onTextChanged(CharSequence s, int start, int before, int count){}
	        }); 
	        
	        
	        if(userName !=null && passWord !=null){
	        	
	        	
				   //serverConnection();
				 }else{
					 
					// showAlert();
				 }
		 
	      
		 bottom_download=(Button) findViewById(R.id.button_download);
		 
	}
	
	
	
	public String searchText(){
		
		String searchData=  fetchSearch.getText().toString();
		
		return searchData;
	}
	
	
	public void onStart(){
		super.onStart();
		
		fetchLayout.requestFocus();
		
	}
	
	public void download_alert(){
		
		
		int number=0;
		this.mainListView=getListView();
		int count = mainListView.getCount();
		
		for(int j=0;j<count;j++){
			
			
			boolean data=checks.get(j);
			
			if(data){
				
				number++;
				
	
			}
		}
		
		if(number==0){
			
			AlertDialog.Builder builder = new AlertDialog.Builder(NewFetch.this);
			builder.setCancelable(false);
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  dialog.dismiss();
			        
				  
				 
			  }});
			
			/*builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				
				  public void onClick(DialogInterface dialog, int arg1) {
				  // do something when the OK button is clicked
					  
					  dialog.dismiss();
					  
					  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
						SharedPreferences.Editor prefEditor = loginSettings.edit();  
						prefEditor.putInt("check", 0); 
						prefEditor.commit();  
						
						//bottom.setVisibility(View.GONE);
						
						String data=searchText();
						
						if(data.length()==0){
							
							mAdapter = new MyCustomAdapter(NewFetch.this,myList,NORMAL);
							 
							 setListAdapter(mAdapter);
							
						}else{
							
							doSearch(data);
							
						}*/
						
						//mAdapter = new MyCustomAdapter(NewFetch.this,myList,NORMAL);
						 
						 //setListAdapter(mAdapter);
						
						/*Intent ntnt= new Intent(NewFetch.this, MainActivity.class);
						ntnt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				        startActivity(ntnt);
				       */ 
					  
				 // }});
			
			
			//builder.show();
			builder.setTitle("Warning");
			builder.setMessage("Please Select atleast one card.").create().show();
			
		}else {
			
			itemDownloadAlert();
		}
		
	}
	
	
	public void itemDownloadAlert(){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(NewFetch.this);
		builder.setTitle("Download");
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface arg0, int arg1) {
		  // do something when the OK button is clicked
			  
			  
			  bumpDisable();
	          
			 itemDownload();
		        
			  
		  }});
		
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			
			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  dialog.dismiss();
				  
				  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					SharedPreferences.Editor prefEditor = loginSettings.edit();  
					prefEditor.putInt("check", 0); 
					prefEditor.commit();  
					
					//bottom.setVisibility(View.GONE);
					
					String data=searchText();
					
					if(data.length()==0){
						
						mAdapter = new MyCustomAdapter(NewFetch.this,myList,NORMAL);
						 
						 setListAdapter(mAdapter);
						
					}else{
						
						doSearch(data);
						
					}
					
					//mAdapter = new MyCustomAdapter(NewFetch.this,myList,NORMAL);
					 
					// setListAdapter(mAdapter);
					
					/*Intent ntnt= new Intent(NewFetch.this, MainActivity.class);
					ntnt.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			        startActivity(ntnt);
			        */
				  
			  }});
		
		//builder.show();
		builder.setMessage("Do you want to download selected cards ?").create().show();
		
		
	}
	
	public AlertDialog showAlert(Context context, String str){
		
		
		myList = new ArrayList<HashMap<String, String>>();
       	mAdapter = new MyCustomAdapter(NewFetch.this,myList,NORMAL);
		 
		 setListAdapter(mAdapter);
		 async=0;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(NewFetch.this);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface arg0, int arg1) {
		  // do something when the OK button is clicked
			  
			  switchTabInActivity(3);
		        
			  
		  }});
		
		//builder.show();
		builder.setTitle("Warning");
		return builder.setMessage("You have to login").create();
		
	}
	
	public void showDownloadAlert(){
			
			AlertDialog.Builder builder = new AlertDialog.Builder(NewFetch.this);
			builder.setTitle("Download All");
			builder.setCancelable(false);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	
			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				
				  dialog.dismiss();
				  bumpDisable();
				  
				  downloadAll();
				  
			        
				  
			  }});
			
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				  public void onClick(DialogInterface dialog, int arg1) {
				  // do something when the OK button is clicked
					  
					  
					  
					  dialog.dismiss();
				        
					  
				  }});
			
			//builder.show();
			builder.setMessage("Do you want to download all Cards ?").create().show();
			
		}
	
	
	
	public void onStop(){
    	super.onStop();
    	
    	dismissLoading();
	
	}
	
	
	public void showLoading(Context context, String text) {
        dismissLoading();

        mLoading = showAlert(context, text);
        mLoading.show();
    }
	
	
	private static void dismissLoading() {
        if (mLoading != null) {
            mLoading.dismiss();
            mLoading = null;
        }
    }
	
	
	public void showSyncAlert(){
			
			AlertDialog.Builder builder = new AlertDialog.Builder(NewFetch.this);
			builder.setTitle("Update Cards");
			builder.setCancelable(false);
			builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
	
			  public void onClick(DialogInterface arg0, int arg1) {
			  // do something when the OK button is clicked
				  
				  bumpDisable();
				  synchronize();
			        
				  
			  }});
			
			builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
				
				  public void onClick(DialogInterface dialog, int arg1) {
				  // do something when the OK button is clicked
					  
					  dialog.dismiss();
				        
					  
				  }});
			
			//builder.show();
			builder.setMessage("Do you want to Update all cards?").create().show();
			
		}
		
	public void doSearch(String string){
			
			if(string.length()!=0){
				
				mySearchList=new ArrayList<HashMap<String, String>>();
				
				int count = myList.size();
				
				for(int i=0;i<count;i++){
					
					HashMap<String, String> value =myList.get(i);
					
					String searchName=value.get("name");
					String searchCompany=value.get("company");
					String lowerCaseName=searchName.toLowerCase();
					String lowerCaseCompany=searchCompany.toLowerCase();
					String lowerSearch=string.toLowerCase();
					String searchDesignation=value.get("designation").toLowerCase();
					String searchEmail=value.get("email").toLowerCase();
					String searchPhone=value.get("phone").toLowerCase();
					String searchCompanyAddress=value.get("company_address").toLowerCase();
					String searchDescription=value.get("description").toLowerCase();
					String searchComments=value.get("comments").toLowerCase();
					String searchWebUrl=value.get("webUrl").toLowerCase();
					String searchFax=value.get("fax").toLowerCase();
					String searchOffice=value.get("office").toLowerCase();
					
					/*if(lowerCaseName.matches(".*"+lowerSearch+".*") || lowerCaseCompany.matches(".*"+lowerSearch+".*") 
							|| searchDesignation.matches(".*"+lowerSearch+".*") || searchEmail.matches(".*"+lowerSearch+".*") 
							|| searchPhone.matches(".*"+lowerSearch+".*") || searchCompanyAddress.matches(".*"+lowerSearch+".*") 
							|| searchDescription.matches(".*"+lowerSearch+".*") || searchComments.matches(".*"+lowerSearch+".*") 
							|| searchWebUrl.matches(".*"+lowerSearch+".*") || searchFax.matches(".*"+lowerSearch+".*")
							|| searchOffice.matches(".*"+lowerSearch+".*")){*/
						if(lowerCaseName.contains(lowerSearch) || lowerCaseCompany.contains(lowerSearch) 
							   || searchDesignation.contains(lowerSearch) || searchEmail.contains(lowerSearch) 
							   || searchPhone.contains(lowerSearch) || searchCompanyAddress.contains(lowerSearch) 
							   || searchDescription.contains(lowerSearch) || searchComments.contains(lowerSearch)
							   || searchWebUrl.contains(lowerSearch) ||searchFax.contains(lowerSearch) 
							   || searchOffice.contains(lowerSearch) ){
						
						
						
						
						String d_id = value.get("id");
				         String d_name = value.get("name");
				         String d_number = value.get("phone");
				         String d_address = value.get("company_address");
				         String d_comp_name = value.get("company");
				         String d_designation = value.get("designation");
				         String d_email = value.get("email");
				         String d_profile_description=value.get("description");
				         String d_profile_date=value.get("updateDate");
				         String d_profile_video=value.get("video");
				         String d_profile_pic=value.get("pic");
				         String d_profile_logo=value.get("logo");
				         String d_profile_comments=value.get("comments");
				         String d_profile_url=value.get("webUrl");
				         String d_office=value.get("office");
				         String d_fax=value.get("fax");
				         
				         
				         HashMap<String, String> newValue = new HashMap<String, String>();  
				         
				         newValue.put("id", d_id);
				         newValue.put("name", d_name);
				         newValue.put("phone", d_number);
				         newValue.put("company_address", d_address);
				         newValue.put("company", d_comp_name);
				         newValue.put("designation", d_designation);
				         newValue.put("email", d_email);
				         newValue.put("description", d_profile_description);
				         newValue.put("updateDate", d_profile_date);
				         newValue.put("video", d_profile_video);
				         newValue.put("pic", d_profile_pic);
				         newValue.put("logo", d_profile_logo);
				         newValue.put("number",Integer.toString(i));
				         newValue.put("comments",d_profile_comments);
				         newValue.put("webUrl",d_profile_url);
				         newValue.put("office",d_office);
				         newValue.put("fax",d_fax);
				         
				         mySearchList.add(newValue);
				         
				        // System.out.println("LIKE id ::"+d_id);
						
					}
					
				}
				//int search=1;
				
				mAdapter = new MyCustomAdapter(NewFetch.this,mySearchList,SEARCH);
				 
				 setListAdapter(mAdapter);
				
			}else{
				
				mySearchList=null;
				
				
				mAdapter = new MyCustomAdapter(NewFetch.this,myList,NORMAL);
				 
				 setListAdapter(mAdapter);
				//serverConnection();
				
			}
			
			
			
			
			
		}
		
		
		public void onResume(){
			super.onResume();
			
			//com.facebook.Settings.publishInstallAsync(this, "659621604053010");
		    
			
			boolean tablet= isTablet(this);
			if(tablet){
				
				MainActivity.getInstance().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			}else{
				
				MainActivity.getInstance().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				
			}
			MainActivity parent;
        	parent =(MainActivity) this.getParent();
        	parent.manipulateTitle("Digcard");
			
			SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	        String userName = loginSettings.getString("user_name", null);
	        String passWord = loginSettings.getString("password", null);
	        String user = loginSettings.getString("user_id", null);
	        int changeInProfile=loginSettings.getInt("save", 0);
	        int update=loginSettings.getInt("update", 0);
			int bumpActive=loginSettings.getInt("bump", 0);
			int loginSearch=loginSettings.getInt("loginSearch", 0);
			int registerSearch=loginSettings.getInt("registerSearch", 0);
			
			SharedPreferences itemSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor prefEditor = itemSettings.edit();  
			prefEditor.putInt("number", 1); 
			prefEditor.commit(); 
			
			
			//initializeArray();
			
			/*if(bumpActive==1){
				
				MainActivity parent;
	        	parent =(MainActivity) this.getParent();
	        	parent.manipulateBump();
			}*/
			
			SharedPreferences checkSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			String auto_user_id= checkSettings.getString("auto_user_id", null);
	        
	       // if(user ==null && auto_user_id==null){
			if(user ==null){
	       	 
	       	 	//showAlert();
	        	showLoading(NewFetch.this, "Not Loged In");
	        
	       	 
	        }else if(async!=1&&changeInProfile!=1){
	        	
	        	
	        	
	        	refreshList(0);
	        	
	        }else if(update ==1&&changeInProfile!=1){
	        	
	        	/*
				if(async==1){
					
						loadingDialog = new ProgressDialog(NewFetch.this);
			            loadingDialog.setMessage("Fetching from server. Please wait...");
			            loadingDialog.setCancelable(false);
			            loadingDialog.setIndeterminate(false);
			            loadingDialog.show();
					
				}*/
					
	        	
	        	refreshList(0);
	        	
	        	/*SharedPreferences updateSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
				SharedPreferences.Editor prefUpdateEditor = updateSettings.edit();  
				prefUpdateEditor.putInt("update", 0); 
				prefUpdateEditor.commit(); */
				
	        	
	        	
	        	
	        }else if(registerSearch==1||loginSearch==1){
	        	if(changeInProfile!=1){
	        		refreshList(0);
	        	}
	        	
	        	
	        }else{
	        	
	        	
	        	SharedPreferences updateSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
				SharedPreferences.Editor prefUpdateEditor = updateSettings.edit();  
				prefUpdateEditor.putInt("save", 0); 
				prefUpdateEditor.commit();
	        	
	        }
	        
	        
	        
	        
	        
	        
	        
	     
		}
	
		public boolean isTablet(Context context) {  
	        return (context.getResources().getConfiguration().screenLayout   
	                & Configuration.SCREENLAYOUT_SIZE_MASK)    
	                >= Configuration.SCREENLAYOUT_SIZE_LARGE; 
		}
		
		public void onPause(){
			super.onPause();
			MainActivity.getInstance().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
			if (task != null && task.getStatus() != AsyncTask.Status.FINISHED) {
				
				
				 
				 task.cancel(true);
				 task = null;
			 
	        }
			
			initializeArray();
			//imageList = new ArrayList<HashMap<String, byte[]>>();  
	   		//logoList = new ArrayList<HashMap<String, byte[]>>();
			
			
			
			/*if(refreshProcess==1){
				
				AlertDialog.Builder builder = new AlertDialog.Builder(NewFetch.this);
				
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

				  public void onClick(DialogInterface dialog, int arg1) {
				  // do something when the OK button is clicked
					  
					 
					
					  dialog.dismiss();
				  }

				});
				
							
				
				builder.setMessage("Please wait..Refreshing List").create().show();
				
			}*/
			
		}
		
	public void checkServerConnection(){
		
		if(async!=1){
			
			refreshList(0);
		}
	}	
		
	private void switchTabInActivity(int i) {
		// TODO Auto-generated method stub
    	
    	MainActivity parent;
    	parent =(MainActivity) this.getParent();
    	parent.switchTab(i);
	}
	
	
	
	
	@Override
    public void onDestroy() {
		super.onDestroy();
		
		 if (task != null && task.getStatus() != AsyncTask.Status.FINISHED) {
			 
			 task.cancel(true);
			 task = null;
		 
        }

		 
	
	}
	
	
	
	
	
	
	
	
	public void refreshList(int value){
		
		
		
		boolean connect=haveNetworkConnection();
		if(connect){
     		
			refreshProcess=value;
			
    		task = new FetchCardList(NewFetch.this);
    		//task.execute(new String[] {"http://dev-fsit.com/iphone/digcard/fetchcard.php"});
    		//task.execute(new String[] {"http://192.168.1.187/android/fetchcard.php"});
    		//task.execute(new String[] {"http://dev-fsit.com/iphone/fetchcard.php"});
    		//handler.sendMessageDelayed(msg, delayMillis)
    		
    		task.execute(new String[] {"http://digcardapp.com/app/fetchcard.php"});
    		//task.execute(new String[] {"http://192.168.1.187/android/AndroidFetch.php"});
			
    		//bumpDisable();
    		
    	}else{
    		alertFunction("No Internet Connection",1);
    		//Toast.makeText(this, "No network connection", Toast.LENGTH_SHORT).show();
    	}
		
	}
	
	
	
	public void cancelFunction(){
		
		String data=searchText();
		
		if(data.length()==0){
			
			mAdapter = new MyCustomAdapter(NewFetch.this,myList,NORMAL);
			 
			 setListAdapter(mAdapter);
			
		}else{
			
			doSearch(data);
			
		}
		
		
		
	}
	
	
	public boolean onMenuItemSelected(int featureId, MenuItem item){
		
		switch(item.getItemId()){
		
		case R.id.menu_download:
			
			
			
			
			download();
			break;
			
		case R.id.menu_download_all:
			
			
			
			showDownloadAlert();
			break;
		case R.id.menu_sync:
			
			
			 showSyncAlert();
			break;	
			
		case R.id.refresh:
			
			
			refreshList(1);
			break;
		case R.id.menu_download_button:
			
			
			download_alert();
			break;
			
		case R.id.menu_download_cancel:

			
			SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor prefEditor = loginSettings.edit();  
			prefEditor.putInt("check", 0); 
			prefEditor.commit();  
			
			//bottom.setVisibility(View.GONE);
			
			//mAdapter = new MyCustomAdapter(NewFetch.this,myList,NORMAL);
			 
			// setListAdapter(mAdapter);
			 cancelFunction();
			
			 break;
		
		}
		return super.onMenuItemSelected(featureId, item);
		
	
	}
	
	
	public void download(){
		
		
		
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		SharedPreferences.Editor prefEditor = loginSettings.edit();  
		prefEditor.putInt("check", 1); 
		prefEditor.commit();  
		
		
		
		String data=searchText();
		
		if(data.length()==0){
			
			mAdapter = new MyCustomAdapter(NewFetch.this,myList,NORMAL);
			 
			 setListAdapter(mAdapter);
			
		}else{
			
			doSearch(data);
		}
		
         
        
	}
	
	
	
	
	
	private void itemDownload(){
		
		progress = new ProgressDialog(NewFetch.this);
		//progress.setTitle("Download");
		progress.setMessage("Downloading..");
		progress.setCancelable(false);
		progress.show();
		
		
		
		
		
		
		Thread downloadThread=new Thread(new Runnable(){

			
			public void run() {
				// TODO Auto-generated method stub
				
				
		
				initializeArray();
				
				String d_profile_pic;
				String d_profile_logo;
				
				mainListView=getListView();
				int count = mainListView.getCount();
				
				
				for(int j=0;j<count;j++){
					
					
					boolean data=checks.get(j);
					
					if(data){
						
						
						byte[] decodedPicByte=null;
						byte[] decodedLogoByte=null;
						
						
						@SuppressWarnings("unchecked")
						HashMap<String, String> val = (HashMap<String, String>) mainListView.getItemAtPosition(j);
						
						
						String[] key= new String[] {CardTable.KEY_NAME,CardTable.KEY_USERID};
						
						 String d_id = val.get("id");
				         String d_name = val.get("name");
				         String d_number = val.get("phone");
				         String d_address = val.get("company_address");
				         String d_comp_name = val.get("company");
				         String d_designation = val.get("designation");
				         String d_email = val.get("email");
				         String d_profile_description=val.get("description");
				         String d_profile_date=val.get("updateDate");
				         String d_profile_video=val.get("video");
				         String profileUrl=val.get("pic").toString();
				         String logoUrl=val.get("logo").toString();
				         String d_profile_comments=val.get("comments");
				         String d_profile_url=val.get("webUrl");
				         String d_office=val.get("office");
				         String d_fax=val.get("fax");
				         
				         
				         /*if(mySearchList ==null ){
				        	 
				        	 
				        	 HashMap<String, byte[]> arrayImage=imageList.get(j);
					         HashMap<String, byte[]> arrayLogo=logoList.get(j);
					         
					         decodedPicByte=arrayImage.get("profile");
					         decodedLogoByte=arrayLogo.get("logo");
						      
						
						      
						      
				        	 
				         }else{
				        	 
				        	 HashMap<String, String> searchData= mySearchList.get(j);
				        	 String number=searchData.get("number");
				        	 HashMap<String, byte[]> arrayImage =imageList.get(Integer.parseInt( number));
				        	 HashMap<String, byte[]> arrayLogo=logoList.get(Integer.parseInt( number));
				        	 decodedPicByte=arrayImage.get("profile");
				        	 decodedLogoByte=arrayLogo.get("logo");
				        	  
				        	
							      
				        	  
				         }*/
				         
				         	decodedPicByte= byteGlobalArray;
				         	decodedLogoByte=byteGlobalLogoArray;
				         
				         
				         
				         Uri queryUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL + "/" + d_id);
				         Cursor c = getContentResolver().query(queryUri, key, null, null, null);
				         
				         
				         ContentValues values=new ContentValues();
					 		values.put(CardTable.KEY_USERID, d_id);
					 		values.put(CardTable.KEY_NAME, d_name);
					 		values.put(CardTable.KEY_EMAIL, d_email);
					 		values.put(CardTable.KEY_NUMBER, d_number);
					 		values.put(CardTable.KEY_PROFILE_PIC, decodedPicByte);
					 		values.put(CardTable.KEY_DESIGNATION, d_designation);
					 		values.put(CardTable.KEY_VIDEO, d_profile_video);
					 		values.put(CardTable.KEY_COMPANY, d_comp_name);
					 		values.put(CardTable.KEY_ADDRESS, d_address);
					 		values.put(CardTable.KEY_LOGO, decodedLogoByte);
					 		values.put(CardTable.KEY_DESCRIPTION, d_profile_description);
					 		values.put(CardTable.KEY_UPDATE_DATE, d_profile_date);
					 		values.put(CardTable.KEY_COMMENTS, d_profile_comments);
					 		values.put(CardTable.KEY_WEB_ADDRESS, d_profile_url);
					 		values.put(CardTable.KEY_OFFICE, d_office);
					 		values.put(CardTable.KEY_FAX, d_fax);
					 		
					 		
					 		if ( c != null && c.getCount() > 0 ) {
					 			if (c.moveToFirst()) {
					 				do {
					 					
					 					String c_id=c.getString(c.getColumnIndexOrThrow(CardTable.KEY_USERID));
					 					
					 					if(d_id.equals(c_id)){
					 		        		 
					 		        		 getContentResolver().update(queryUri, values, null, null);
					 		        		
					   				   
					 		        	 }else {
					 		        		
					 		        		 getContentResolver().insert(CardManagingProvider.CONTENT_URI_DETAIL, values);
					 		        		
					 		        	 }
					 					
					 				}while(c.moveToNext());
					 				
					 			}
					 			
					 			
					 			
					 		}else {
					 			
					 			
					 			
					 			getContentResolver().insert(CardManagingProvider.CONTENT_URI_DETAIL, values);
					 			
		   				   
					 		}
					 		
					 		c.close();
					}
				}
				
				
				handler.sendEmptyMessage(DOWNLOAD);
				
				
				
			}
			
		});
		
		downloadThread.start();
		
		int count=0;
				
		//for(int i=0;i<myList.size();i++){
		for(int i=0;i<mainListView.getCount();i++){
			
			boolean data=checks.get(i);
			
			if(data){
				
				count++;
			}
			
		}
		
				int item=0;
				//for(int i=0;i<myList.size();i++){
				for(int i=0;i<mainListView.getCount();i++){	
					boolean data=checks.get(i);
					
					if(data){
						
						//HashMap<String, String> map = myList.get(i);
						@SuppressWarnings("unchecked")
						HashMap<String, String> map = (HashMap<String, String>) mainListView.getItemAtPosition(i);
			            String profileUrl=map.get("pic").toString();
			            String logoUrl=map.get("logo").toString();
			            String userId=map.get("id").toString();
						ProfileBitmapManager.PROFILEINSTANCE.profileQueueJob(profileUrl,item,userId,count);
		 	            LogoBitmapManager.LOGOINSTANCE.logoQueueJob(logoUrl,item,userId,count);
						item++;
					}
				}
	}
	
	private void downloadAll(){
		
		//pd=ProgressDialog.show(this, "Download All", "Please wait..", true, false);
		pd = new ProgressDialog(NewFetch.this);
       	//pd.setTitle("Download All");
		pd.setMessage("Downloading Cards..");
		pd.setCancelable(false);
        pd.show();
    	
			initializeArray();
		
		
			Thread thread=new Thread(this);
	    	thread.start();
	    	
	    	
			
			
			for (int i = 0; i < myList.size(); i++) {
	             
				HashMap<String, String> map = myList.get(i);
	            String profileUrl=map.get("pic").toString();
	            String logoUrl=map.get("logo").toString();
	            String userId=map.get("id").toString();
	            
	            ProfileBitmapManager.PROFILEINSTANCE.profileQueueJob(profileUrl,i,userId,myList.size());
	            LogoBitmapManager.LOGOINSTANCE.logoQueueJob(logoUrl,i,userId,myList.size());
	            
	            	             
	         }
			
			
			
				    	
			
		
	}
	
	
	@SuppressWarnings("null")
	private void initializeArray(){
		
		  
   				
   		imageList = new ArrayList<HashMap<String, byte[]>>();  
   		logoList = new ArrayList<HashMap<String, byte[]>>();
   		
   		String str="sorry";
       		  
   		byte[] arr = str.getBytes(); 
   	     
   		for (int i = 0; i < myList.size(); i++) {
              
                
                HashMap<String, byte[]> map=new HashMap<String, byte[]>();
                HashMap<String, byte[]> mapLogo=new HashMap<String, byte[]>();

                map.put("profile",arr);
                mapLogo.put("logo",arr);

                imageList.add(map);
                logoList.add(mapLogo);
            }
	}
	
	private void synchronize(){
		final int exist;
		//bar=ProgressDialog.show(this, "Synchronizing to card server", "Please wait..", true, false);
		bar = new ProgressDialog(NewFetch.this);
		//bar.setTitle("Synchronizing to card server");
		bar.setMessage("Updating Cards..");
		bar.setCancelable(false);
		bar.show();
		
   			initializeArray();
		
		
		Thread background=new Thread(new Runnable(){

		
			public void run() {
				// TODO Auto-generated method stub
				
				
					/*for(int k=0;k<myList.size();k++){
						
						checks.add(k, false);
						
					}*/
					
					
					int j;
					String[] projection = new String[] {CardTable.KEY_USERID,CardTable.KEY_UPDATE_DATE};
					Uri uri=CardManagingProvider.CONTENT_URI_DETAIL;
					Cursor cursor = getContentResolver().query(uri, projection, null, null, CardTable.KEY_USERID);
					
					String list_profile_logo;
					String list_profile_pic;
					
					
					if ( cursor != null && cursor.getCount() > 0 ) {
						 int match=0;
					    if (cursor.moveToFirst()) {
					        // loop until it reach the end of the cursor
					        do {
					        	
					           String id_cursor =cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_USERID)); 
					           String update_cursor_date =cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_UPDATE_DATE));
					           mainListView=getListView();
					   		   int count = mainListView.getCount();
					   		
					   		  // System.out.println("id_cursor =="+id_cursor);
					   		  // System.out.println("update_cursor_date =="+update_cursor_date);
					   		   
					   		  
					   		for( j=0;j<count;j++){
					   			

					        	byte[] decodedPicByte=null;
								byte[] decodedLogoByte=null;
					        	
					   			
					   			@SuppressWarnings("unchecked")
								HashMap<String, String> array = (HashMap<String, String>) mainListView.getItemAtPosition(j);
					   			
					   			 String list_id = array.get("id");
						   		 String list_name = array.get("name");
						         String list_number = array.get("phone");
						         String list_address = array.get("company_address");
						         String list_comp_name = array.get("company");
						         String list_designation = array.get("designation");
						         String list_email = array.get("email");
						         String list_profile_description=array.get("description");
						         String list_profile_date=array.get("updateDate");
						         String list_profile_video=array.get("video");
						         String profileUrl=array.get("pic").toString();
						         String logoUrl=array.get("logo").toString();
						         String list_profile_comments = array.get("comments");
						         String list_profile_url = array.get("webUrl");
						         String list_office=array.get("office");
						         String list_fax=array.get("fax");
						         
						         
						   		 
						   		 
						         
						        
						         
						       
						         
						         
							 		
							 		
					   			if(id_cursor.equals(list_id)){
					   				
					   				//System.out.println("Exist in server can update" + j);
					   				// code to update
					   				
					   				
					   				
					   				
					   				if(update_cursor_date.equals(list_profile_date)){
					   					
					   					
					   					
					   				}else{
					   					

						   				/*if(mySearchList ==null ){
								        	 
								        	 
								        	 HashMap<String, byte[]> arrayImage=imageList.get(j);
									         HashMap<String, byte[]> arrayLogo=logoList.get(j);
									         
									         decodedPicByte=arrayImage.get("profile");
									         decodedLogoByte=arrayLogo.get("logo");
								        	 
									       
									         
								         }else{
								        	 
								        	 HashMap<String, String> searchData= mySearchList.get(j);
								        	 String number=searchData.get("number");
								        	 HashMap<String, byte[]> arrayImage =imageList.get(Integer.parseInt( number));
								        	 HashMap<String, byte[]> arrayLogo=logoList.get(Integer.parseInt( number));
								        	 decodedPicByte=arrayImage.get("profile");
								        	 decodedLogoByte=arrayLogo.get("logo");
								        	 
								        	
								         }*/
						   				
						   				Uri updateUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL + "/" + list_id);
								         
								         ContentValues values=new ContentValues();
									 		values.put(CardTable.KEY_USERID, list_id);
									 		values.put(CardTable.KEY_NAME, list_name);
									 		values.put(CardTable.KEY_EMAIL, list_email);
									 		values.put(CardTable.KEY_NUMBER, list_number);
									 		//values.put(CardTable.KEY_PROFILE_PIC, decodedPicByte);
									 		values.put(CardTable.KEY_DESIGNATION, list_designation);
									 		values.put(CardTable.KEY_VIDEO, list_profile_video);
									 		values.put(CardTable.KEY_COMPANY, list_comp_name);
									 		values.put(CardTable.KEY_ADDRESS, list_address);
									 		//values.put(CardTable.KEY_LOGO, decodedLogoByte);
									 		values.put(CardTable.KEY_DESCRIPTION, list_profile_description);
											values.put(CardTable.KEY_UPDATE_DATE, list_profile_date);
											values.put(CardTable.KEY_COMMENTS, list_profile_comments);
											values.put(CardTable.KEY_WEB_ADDRESS, list_profile_url);
											values.put(CardTable.KEY_OFFICE, list_office);
											values.put(CardTable.KEY_FAX, list_fax);
					   					
					   					getContentResolver().update(updateUri, values, null, null);
					   				    
					   				    
					   				    	//checks.set(j, true);
					   				    	
					   				   
					   				
					   				}
					   				
					   			  match=j;
					   				
					   				break;
					   			}
					   		}
					           
					        } while (cursor.moveToNext());
					    }

					    // make sure to close the cursor
					    //cursor.close();
					}else{
						
						
						
						handler.sendEmptyMessage(SYNCHRONIZE_NONE);
						
						isRunning=false;
						//bar.dismiss();
						//Toast.makeText(context, "No data in local system.First Download something", Toast.LENGTH_SHORT).show();
					}
					
					
					try{
						
						 cursor.close();
						
					}catch(Exception e){
						
					}
					
					if(isRunning){
						
						
						handler.sendEmptyMessage(SYNCHRONIZE);
					}
					//Toast.makeText(NewFetch.this, "Synchronization has been completed", Toast.LENGTH_SHORT).show();
				
				
			}
			});
    	
    	isRunning=true;
    	background.start();
    
    	
    	
    	
    	
		int i,item=0,size=0;
		String[] projection = new String[] {CardTable.KEY_USERID,CardTable.KEY_UPDATE_DATE};
		Uri uri=CardManagingProvider.CONTENT_URI_DETAIL;
		Cursor cursor = getContentResolver().query(uri, projection, null, null, CardTable.KEY_USERID);
		
		
		
							
		if ( cursor != null && cursor.getCount() > 0 ) {
			 int match=0;
		    if (cursor.moveToFirst()) {
		        // loop until it reach the end of the cursor
		        do {
		        	
		           String id_cursor =cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_USERID)); 
		           String update_cursor_date =cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_UPDATE_DATE));
		           mainListView=getListView();
		   		   int count = mainListView.getCount();
		   		
		   		 
		   		   
		   		  
		   		for( i=0;i<count;i++){
		   			

		   			
		   			@SuppressWarnings("unchecked")
					HashMap<String, String> array = (HashMap<String, String>) mainListView.getItemAtPosition(i);
		   			
		   			 String list_id = array.get("id");
			   		 String profileUrl=array.get("pic").toString();
			         String logoUrl=array.get("logo").toString();
			         String list_date=array.get("updateDate");
			         	
				 		
		   			if(id_cursor.equals(list_id)){
		   				
		   				
		   				
		   				if(update_cursor_date.equals(list_date)){
		   					
		   					
		   					
		   				}else{
		   								
		   							
		   					size++;
				   			
		   				    	ProfileBitmapManager.PROFILEINSTANCE.profileQueueJob(profileUrl,item,list_id,size);
				                LogoBitmapManager.LOGOINSTANCE.logoQueueJob(logoUrl,item,list_id,size);
				            	item++;
		   				   
		   				
		   				}
		   				
		   			  match=i;
		   				
		   				break;
		   			}
		   		}
		           
		        } while (cursor.moveToNext());
		    }

		    
		}
		
		try{
			
			 cursor.close();
			
		}catch(Exception e){
			
		}
		
		if(item==0){
			
			handler.sendEmptyMessage(SYNCHRONIZE_NONE);
			
		}
		        	
		
	}

	
	private class FetchCardList extends AsyncTask<String, Integer, ArrayList<HashMap<String, String>>>{
		
		
		
		private Context mContext;
        
        
        
        FetchCardList(Context context){
        	
        	
        	
	            //mContext = context;
        	
        	//if(loadingDialog==null){
        		
        		
        		
        		mContext = context;
	            loadingDialog = new ProgressDialog(mContext);
	           // loadingDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
	            if(refreshProcess == 1){
	            	loadingDialog.setMessage("Refreshing Cards..");
	            }else{
	            	loadingDialog.setMessage("Fetching Cards..");
	            }
	            
	            loadingDialog.setCancelable(false);
	            loadingDialog.setIndeterminate(false);
	            loadingDialog.show();
        		
        	//}
        		
	            
	            async=1;
	            
        }
		
        
        public void onPreExecute(){
        	
        	
        	
        	
        }

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(String... urls) {
			// TODO Auto-generated method stub
			//String response = "";
			
			//check.setVisibility(View.GONE);
			
			//System.out.println("do in background");
			myList = new ArrayList<HashMap<String, String>>();
			
			for (String url : urls) {
				
				
				
				try {
					
					DefaultHttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost(url);
					HttpResponse execute = httpclient.execute(httppost);
					
					HttpEntity entity     = execute.getEntity();
					 is = entity.getContent();

					

				} catch (Exception e) {
					
					//Toast.makeText(getBaseContext(), "Server connection error", Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}
				
				try{
					
					 publishProgress();
					
					BufferedReader reader = new BufferedReader(new InputStreamReader( is, "iso-8859-1"),8); 
					sb = new StringBuilder();
		        	sb.append(reader.readLine() + "\n");
		        	String line="0";
		        	
		        	while((line =reader.readLine()) !=null){
		        		
		        		sb.append(line + "\n");
		        		
		        	}
					
		        	is.close();
		        	result=sb.toString();
		        	
					
					
				}catch(Exception e){
					Log.e("log_tag", "Error converting result "+e.toString());
					
					//Toast.makeText(getBaseContext(), "Error in string convertion", Toast.LENGTH_LONG).show();
				}
				
				try{
					
					
					
					JSONArray jArray = new JSONArray(result);
					JSONObject json_data;
					
					for(int i=0; i<jArray.length(); i++){
						
						HashMap<String, String> map = new HashMap<String, String>();
						
						json_data= jArray.getJSONObject(i);
						
						row_id=json_data.getInt("user_id");
						String card_id=Integer.toString(row_id);
			        	name=json_data.getString("name");
			        	designation=json_data.getString("designation");
			        	company=json_data.getString("company_name");
			        	company_adderss=json_data.getString("company_address");
			        	phone=json_data.getString("phone");
			        	profile_pic=json_data.getString("profile_picture");
			        	email=json_data.getString("email");
			        	logo=json_data.getString("company_logo");
			        	video=json_data.getString("video");
			        	description=json_data.getString("description");
			        	updateDate=json_data.getString("date_of_update");
			        	comments=json_data.getString("comment");
			        	web_address=json_data.getString("web_url");
			        	office=json_data.getString("office");
			        	fax=json_data.getString("fax");
			        	//System.out.println(video);
			        	
			        	/*System.out.println(row_id);
			        	System.out.println(name);
			        	System.out.println(designation);
			        	System.out.println(company);
			        	System.out.println(company_adderss);
			        	System.out.println(phone);
			        	System.out.println(email);
			        	System.out.println(profile_pic);
			        	*/
			        	map.put("id", card_id);
			        	map.put("name", name);
			        	map.put("designation", designation);
			        	map.put("company", company);
			        	map.put("company_address", company_adderss);
			        	map.put("phone", phone);
			        	map.put("email", email);
			        	map.put("pic", profile_pic);
			        	map.put("logo", logo);
			        	map.put("video", video);
			        	map.put("description", description);
			        	map.put("updateDate", updateDate);
			        	map.put("comments", comments);
			        	map.put("webUrl", web_address);
			        	map.put("office", office);
			        	map.put("fax", fax);
			        	
			        	myList.add(map);
			        	
			        	
					} 	
					
				}catch(JSONException ep){
					
					Log.e("log_tag", "Error JAson result "+ep.toString());
					
		        	//Toast.makeText(getBaseContext(), "No Data Found", Toast.LENGTH_LONG).show();
		        	
		        }catch (ParseException e) {
		        	// TODO Auto-generated catch block
		        	
					e.printStackTrace();
					
				}
			}
			return myList;
		}
		
		
		
		protected void onProgressUpdate(Integer... values){
			
			try{
				
				loadingDialog.show();
				
			}catch(Exception e){
				
				//Log.i("loadingDialog", "loadingDialog "+e);
			}
			 
			
        }
		
		
		
		protected void onCancelled (){
			super.onCancelled();
			
			try{
				
				loadingDialog.dismiss();
				loadingDialog=null;
				
			}catch(Exception e){
				
				//System.out.println("loadingDialog exception"+e);
			}
			
			
		}
		
		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> list) {
			
			/* imageList = new ArrayList<String>();
		     logoList = new ArrayList<String>();
			
			*/
			
			
			refreshProcess=0;
			SharedPreferences updateSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor prefUpdateEditor = updateSettings.edit();  
			prefUpdateEditor.putInt("update", 0); 
			prefUpdateEditor.putInt("save", 0); 
			prefUpdateEditor.putInt("loginSearch", 0); 
			prefUpdateEditor.putInt("registerSearch", 0);
			prefUpdateEditor.commit(); 
			
			bumpEnable();
			 
			if(list.size()<= 3){
				
				
				try{
					
						loadingDialog.dismiss();
						loadingDialog=null;
						
					}catch(Exception e){
						
						//System.out.println("loadingDialog exception"+e);
					}
				
			}
			
			
			for (int i = 0; i < list.size(); i++) {
	                checks.add(i, false);
			}
			
			
			
			initializeArray();
			
			if(list.size() >0){
		    	
		    	mAdapter = new MyCustomAdapter(NewFetch.this,list,NORMAL);
				 
				 setListAdapter(mAdapter);
		    	
		    }else {
		    	
		    	
		    	//Toast.makeText(NewFetch.this, "No Data Found", Toast.LENGTH_LONG).show();
		    	
		    	
		    	
		   }
		    
			 
	    	
	   
		}
	}
	
	
	@SuppressWarnings("unchecked")
	private void serverConnection() {
		// TODO Auto-generated method stub
		
		
		
		 SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	        String userName = loginSettings.getString("user_name", null);
	        String passWord = loginSettings.getString("password", null);
	        String user = loginSettings.getString("user_id", null);
			String auto_user_id= loginSettings.getString("auto_user_id", null);
	        
	        
		
	        //data = getLastNonConfigurationInstance();
		MyAppData appState = ((MyAppData)getApplicationContext());
		data=appState.getState();
		
        	
        	
        
        	if(data ==null){
        		
        		
        		data = getLastNonConfigurationInstance();
        		
        		    		
     	        
     	        //if(userName!=null&&passWord !=null){
        		//if(user ==null && auto_user_id==null){
        		if(user ==null ){
     	        	boolean connect=haveNetworkConnection();
     	        	
     	        	if(data!=null){
        				
        				
        				
        				HashMap<String, ArrayList<HashMap<String, String>>> dataBase=((ArrayList<HashMap<String, ArrayList<HashMap<String, String>>>>)data).get(0);
        				myList=dataBase.get("dataBase");
        				
        				
        				mAdapter = new MyCustomAdapter(NewFetch.this,myList,NORMAL);
                		
                		
        				for (int i = 0; i < myList.size(); i++) {
             	             checks.add(i, false);
        				}    
        				
        				setListAdapter(mAdapter);
        				
        			}else
     	        	
     	        	
     	        	
     	        	if(connect){
     	        		
     	        		 task = new FetchCardList(NewFetch.this);
     	        		//task.execute(new String[] {"http://dev-fsit.com/iphone/digcard/fetchcard.php"});
     	        		//task.execute(new String[] {"http://dev-fsit.com/iphone/fetchcard.php"});
     	        		//task.execute(new String[] {"http://192.168.1.187/android/fetchcard.php"});
     	        		task.execute(new String[] {"http://digcardapp.com/app/fetchcard.php"});
     	        	}else{
     	        		
     	        		//Toast.makeText(this, "No network connection", Toast.LENGTH_SHORT).show();
     	        		alertFunction("No Internet Connection",1);
     	        	}
     	        	
     	        	
     	        }else {
     	        	
     	        	 showLoading(NewFetch.this, "Not Loged In");
     	        } 
        		
        	
        	}else{
        		
        		
        	
        		//if(user ==null && auto_user_id==null){
        		if(user ==null ){
        			HashMap<String, ArrayList<HashMap<String, String>>> dataBase=((ArrayList<HashMap<String, ArrayList<HashMap<String, String>>>>)data).get(0);
        			
        			myList=dataBase.get("dataBase");
        			
        			//int size=((ArrayList<HashMap<String, String>>) data).size();
        			int size=myList.size();
        			//System.out.println("size :"+size);
        			
        			if(size ==0){
        				
        				boolean connect=haveNetworkConnection();
         	        	if(connect){
         	        		
        				
	        				 task = new FetchCardList(NewFetch.this);
	        				 //task.execute(new String[] {"http://dev-fsit.com/iphone/digcard/fetchcard.php"});
	        				//task.execute(new String[] {"http://dev-fsit.com/iphone/fetchcard.php"});
	        				 //task.execute(new String[] {"http://192.168.1.187/android/fetchcard.php"});
	        				 task.execute(new String[] {"http://digcardapp.com/app/fetchcard.php"});
        				
         	        	}else{
         	        		
         	        		//Toast.makeText(this, "No network connection", Toast.LENGTH_SHORT).show();
         	        		alertFunction("No Internet Connection",1);
         	        		
         	        	}
        				
        			}else{
        				
        				//System.out.println("size not zero");
        				//mAdapter = new MyCustomAdapter(NewFetch.this,(ArrayList<HashMap<String, String>>)data);
        				mAdapter = new MyCustomAdapter(NewFetch.this,myList,NORMAL);
                		
                		/*for (int i = 0; i < ((ArrayList<HashMap<String, String>>) data).size(); i++) {
              	             checks.add(i, false);
              	         }*/
        			 
        				
        				//System.out.println("size not zero ,making null");
        				for (int i = 0; i < myList.size(); i++) {
             	             checks.add(i, false);
        				}    
        				//System.out.println("seting adapter");
        				setListAdapter(mAdapter);
        				
        			}
        			
        			       			
        		}else{
        			
        			showLoading(NewFetch.this, "Not Loged In");
        			
        		}
        		
        		
        	
        	}
        
        }
		
		
	//}
	
	
	
	private boolean haveNetworkConnection() {
	    boolean haveConnectedWifi = false;
	    boolean haveConnectedMobile = false;

	    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo[] netInfo = cm.getAllNetworkInfo();
	    for (NetworkInfo ni : netInfo) {
	        if (ni.getTypeName().equalsIgnoreCase("WIFI"))
	            if (ni.isConnected()){
	            	
	            	//Toast.makeText(this, "WIFI network connected", Toast.LENGTH_SHORT).show();
	                haveConnectedWifi = true;
	            	
	            }
	            	
	            	
	        if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
	            if (ni.isConnected()){
	            	//Toast.makeText(this, "MOBILE network connected", Toast.LENGTH_SHORT).show();
	            	haveConnectedMobile = true;
	            }
	                
	    }
	    return haveConnectedWifi || haveConnectedMobile;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Object onRetainNonConfigurationInstance() {
	    //final ArrayList<HashMap<String, String>> prev_list = mylist;
	    //keepPhotos(list);
		
		MyAppData myAppState = ((MyAppData)getApplication());
		data=myAppState.getState();
		
		if(data!=null && async!=1){
			
			
			
			HashMap<String, ArrayList<HashMap<String, String>>> dataBase=((ArrayList<HashMap<String, ArrayList<HashMap<String, String>>>>)data).get(0);
			
			myList=dataBase.get("dataBase");
			//imageList=dataBase.get("imageData");
			//logoList=dataBase.get("logoData");
			//myList=(ArrayList<HashMap<String, String>>) data;
		}
		
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
        String userName = loginSettings.getString("user_name", null);
        String passWord = loginSettings.getString("password", null);
        if(userName == null && passWord == null){
        	
        	return null;
        }else{
        	
        	HashMap<String, ArrayList<HashMap<String, String>>> newMap = new HashMap<String, ArrayList<HashMap<String, String>>>();
    		
    		
    		//System.out.println("imageData ::"+imageList.size());
    		//System.out.println("logoData ::"+logoList.size());
    		newMap.put("dataBase", myList);
    		//newMap.put("imageData", imageList);
    		//newMap.put("logoData", logoList);
    		
    		allMyList.add(newMap);
    		
    		
    		//System.out.println("allMyList ::"+allMyList);
    		
    		if(data==null && async==1){
    			
    			
    			MyAppData appState = ((MyAppData)getApplicationContext());
        		appState.setState(allMyList);
    			
    		}
    		
    		
    		
    	    //return myList;
    		return allMyList;
        	
        }
		
	}
	
	
	protected void onListItemClick(ListView l, View v, int position, long id){
		super.onListItemClick(l, v, position, id);
		
		//System.out.println("position :"+position);
		
		//CheckBox box=(CheckBox) findViewById(R.id.card_id);
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		int visible=loginSettings.getInt("check", 0);
		
		if(visible == 1){
			
			
			
			boolean currentlyChecked = checks.get(position);
			checks.set(position, !currentlyChecked);
			CheckBox markedItem = (CheckBox) v.findViewById(R.id.card_id);
			markedItem.setChecked(checks.get(position));
			
	
			
			
		}else{
		
			HashMap<String, byte[]> photo;
			HashMap<String, byte[]> photoLogo;
			
			byte[] c_profile_pic;
			//byte[] c_profile_logo;
			
			String pic;
			
		 @SuppressWarnings("unchecked")
		HashMap<String, String> o = (HashMap<String, String>) l.getItemAtPosition(position);                  
    	 
         String c_id = o.get("id");
         String c_name = o.get("name");
         String c_number = o.get("phone");
         String c_address = o.get("company_address");
         String c_comp_name = o.get("company");
         String c_designation = o.get("designation");
         String c_email = o.get("email");
         String pic_url = o.get("pic");
         String logo = o.get("logo");
         String c_profile_video = o.get("video");
         String c_description = o.get("description");
         String c_udate_date = o.get("updateDate");
         String c_comments = o.get("comments");
         String c_url = o.get("webUrl");
         String c_office = o.get("office");
         String c_fax = o.get("fax");
         
         
         String str="sorry";
  		  
    	byte[] arr = str.getBytes();
         
         if(mySearchList ==null ){
        	 
        	 
        	 photo=imageList.get(position);
            // photoLogo=logoList.get(position);
             c_profile_pic =photo.get("profile");
             //c_profile_logo =photoLogo.get("logo");
             
             if(c_profile_pic!=arr){
            	 
            	 pic=Base64.encodeBytes(c_profile_pic);
            	 
             }else{
            	 
            	 pic=Base64.encodeBytes(byteGlobalArray);
             }
             
            // logo=Base64.encodeBytes(c_profile_logo);
        	 
         }else{
        	 
        	 HashMap<String, String> searchData= mySearchList.get(position);
        	 String number=searchData.get("number");
        	 photo=imageList.get(Integer.parseInt( number));
        	// photoLogo=logoList.get(Integer.parseInt( number));
        	 c_profile_pic =photo.get("profile");
             //c_profile_logo =photoLogo.get("logo");
        	 if(c_profile_pic!=arr){
            	 
            	 pic=Base64.encodeBytes(c_profile_pic);
            	 
             }else{
            	 
            	 pic=Base64.encodeBytes(byteGlobalArray);
             }
            // logo=Base64.encodeBytes(c_profile_logo);
         }
          
         
       /*  
         String c_profile_pic =imageList.get(position);
         String c_profile_logo =logoList.get(position) ;
         */
        // System.out.println("image count=" +imageList.size());
        // System.out.println("c_profile_pic=" +c_profile_pic);
       //  System.out.println("c_profile_logo=" +c_profile_logo);
		 
		 
		Intent edit_intent = new Intent(NewFetch.this, BussinessCard.class);
		
		
		edit_intent.putExtra("card_id",c_id);
		edit_intent.putExtra("card_name",c_name);
		edit_intent.putExtra("card_number",c_number);
		edit_intent.putExtra("card_address",c_address);
		edit_intent.putExtra("card_company_name",c_comp_name);
		edit_intent.putExtra("card_designation",c_designation);
		edit_intent.putExtra("card_email",c_email);
		edit_intent.putExtra("card_pic",pic);
		edit_intent.putExtra("card_pic_url",pic_url);
		edit_intent.putExtra("card_logo",logo);
		edit_intent.putExtra("card_video",c_profile_video);
		edit_intent.putExtra("card_description",c_description);
		edit_intent.putExtra("card_update_date",c_udate_date);
		edit_intent.putExtra("card_comments",c_comments);
		edit_intent.putExtra("card_web_url",c_url);
		edit_intent.putExtra("card_office",c_office);
		edit_intent.putExtra("card_fax",c_fax);
		
		startActivity(edit_intent);
		
		}
		
		
		
		
	}
	
	
	
	public class MyCustomAdapter extends BaseAdapter implements View.OnClickListener{
		
		 ArrayList<HashMap<String, String>> list = null;
		 Context myContext;
		 Bitmap mIcon1;
		 int condition;
		 private int value;
		 private LayoutInflater mInflater;
		// private int visible;
		 
		 
		public MyCustomAdapter(Context context, ArrayList<HashMap<String, String>> list,int condition) {
			// TODO Auto-generated constructor stub
			
			this.myContext = context;
			this.list = list;
			this.condition=condition;
			//this.fetchSearch=search;   
			   
			   //ListView listView1 = (ListView) findViewById(R.id.list);
			   
			   mInflater = LayoutInflater.from(context); 
			   mIcon1=BitmapFactory.decodeResource(context.getResources(), R.drawable.profile_pic);
			   
			   BitmapManager.INSTANCE.setPlaceholder(BitmapFactory.decodeResource(context.getResources(), R.drawable.profile_pic));
			   
			   SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
		         value = loginSettings.getInt("check", 0);
		         
		         for (int i = 0; i < this.list.size(); i++) {
		             checks.add(i, false);
		         }

			
		}

		
		public int getCount() {
			// TODO Auto-generated method stub
			 return list.size();
		}

	
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			 return list.get(position);
		}

		
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			ViewHolder holder; 
			if (convertView == null) { 
				
				convertView = mInflater.inflate(R.layout.card_list, null);
				holder = new ViewHolder(); 
				holder.view_name = (TextView) convertView.findViewById(R.id.card_name); 
				holder.view_designation = (TextView) convertView.findViewById(R.id.card_designation); 
				holder.view_company = (TextView) convertView.findViewById(R.id.card_company); 
				holder.view_number = (TextView) convertView.findViewById(R.id.card_phone); 
				holder.view_pic = (ImageView) convertView.findViewById(R.id.card_imag); 
				holder.view_check = (CheckBox) convertView.findViewById(R.id.card_id);
				convertView.setTag(holder); 
			}else { 
				holder = (ViewHolder) convertView.getTag(); 
			} 
			
			     HashMap<String, String> map = this.list.get(position);
			     
			     
			     if(value == 1){
			    	 
			    	 holder.view_check.setVisibility(View.VISIBLE);
			     }
			     
			    String l_name = map.get("name").toString();
			    String l_number = map.get("phone").toString();
			    String l_company = map.get("company").toString();
			    String l_designation = map.get("designation").toString();
			    String l_pic = map.get("pic").toString();
			   
			    
			    holder.view_name.setText(l_name);
			    holder.view_designation.setText(l_designation);
			    holder.view_company.setText(l_company);
			    holder.view_number.setText(l_number);
			    holder.view_pic.setTag(l_pic);
			    
			   		    
			  /*  HashMap<String, byte[]> defaultMap=new HashMap<String, byte[]>();
                HashMap<String, byte[]> defaulMapLogo=new HashMap<String, byte[]>();

                defaultMap.put("profile" ,byteGlobalArray);
                defaulMapLogo.put("logo" ,byteGlobalLogoArray);

                imageList.add(position,defaultMap);
                logoList.add(position,defaulMapLogo);*/
			    
			    
			    if(condition==5){
			    	
			    	BitmapManager.INSTANCE.loadBitmap(l_pic,position, holder.view_pic, 34, 34);
			    	 //System.out.println("normal " + condition);
			    	
			    }else if(condition==6){
			    	
			    	//System.out.println("search " + condition);
			    	String number=map.get("number");
			    	BitmapManager.INSTANCE.loadBitmap(l_pic,Integer.parseInt( number), holder.view_pic, 34, 34);
			    	
			    }
			    
			    
			   
			 
			    holder.view_check.setTag(Integer.valueOf(position));
			    holder.view_check.setOnClickListener(this);
			    holder.view_check.setChecked(checks.get(position));
			   
			    
			
			return convertView;
		}
		
		
		public class ViewHolder {
	        public TextView view_name;
	        public TextView view_designation;
	        public TextView view_company;
	        public TextView view_number;
	        public ImageView view_pic;
	        public CheckBox  view_check;
	    }


		
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			
			Integer index = (Integer)v.getTag();
		     boolean state = checks.get(index.intValue());

		     checks.set(index.intValue(), !state);
		     //listView1.FullRowSelect = true;
			
		}
		
		
	}

	 public enum BitmapManager { 
		 INSTANCE; 
		 
		 private final Map<String, SoftReference<Bitmap>> cache;  
	     private final ExecutorService pool;
	     
	     private Map<ImageView, String> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, String>());  
	     private Bitmap placeholder;
		 
	     BitmapManager() {  
	    	 
	    	 cache = new HashMap<String, SoftReference<Bitmap>>();  
	         pool = Executors.newFixedThreadPool(5);  
	     }
	     public void setPlaceholder(Bitmap bmp) {  
	            placeholder = bmp;  
	        }
	     public Bitmap getBitmapFromCache(String url) {  
	            if (cache.containsKey(url)) {  
	                return cache.get(url).get();  
	            }  
	      
	            return null;  
	        }
	     
	     public void queueJob(final String url,final int position, final ImageView imageView, final int width, final int height) {  
	            /* Create handler in UI thread. */  
	            final Handler handler = new Handler() {  
	                @Override  
	                public void handleMessage(Message msg) {  
	                    String tag = imageViews.get(imageView);  
	                    if (tag != null && tag.equals(url)) {  
	                        if (msg.obj != null) {  
	                            imageView.setImageBitmap((Bitmap) msg.obj); 
	                            ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
		       					 ((Bitmap) msg.obj).compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutput);
		       					 byte [] byteArray = byteArrayOutput.toByteArray();
		       					// String imageString=Base64.encodeBytes(byteArray);    
		       					HashMap<String, byte[]> map=new HashMap<String, byte[]>();
		       					map.put("profile",byteArray);
		       					imageList.set(position, map);
		       					
		       					// ((Bitmap) msg.obj).recycle();
		       					
		       					if(position >= 2){
		       					
		       						try{
			       						loadingDialog.dismiss();
			       						loadingDialog=null;
			       					}catch(Exception e){
			       						
			       						//System.out.println("loadingDialog exception"+e);
			       					}
		                            
		       						
		       					}
		       					
	                        } else {  
	                            imageView.setImageBitmap(placeholder);  
	                           // Log.d(null, "fail " + url);  
	                        }  
	                    }  
	                }  
	            };
	            
	            pool.submit(new Runnable() {  
	                
	                public void run() {  
	                    final Bitmap bmp = downloadBitmap(url, width, height);  
	                    Message message = Message.obtain();  
	                    message.obj = bmp;  
	                    //Log.d(null, "Item downloaded: " + url);  
	      
	                    handler.sendMessage(message);  
	                }  
	            });  
	        } 
	     
	     public void loadBitmap(final String url,final int position, final ImageView imageView, final int width, final int height) {  
	            imageViews.put(imageView, url);  
	            Bitmap bitmap = getBitmapFromCache(url);  
	      
	            // check in UI thread, so no concurrency issues  
	            if (bitmap != null) {  
	                // Log.d(null, "Item loaded from cache: " + url);  
	                imageView.setImageBitmap(bitmap);
	                
	                ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
	                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutput);
  					byte [] byteArray = byteArrayOutput.toByteArray();
  					// String imageString=Base64.encodeBytes(byteArray);    
  					HashMap<String, byte[]> map=new HashMap<String, byte[]>();
  					map.put("profile",byteArray);
  					imageList.set(position, map);
  					//bitmap.recycle();
  					
	                if(position >= 2){
       					
   						try{
   							
       						loadingDialog.dismiss();
       						//loadingDialog=null;
       						
       					}catch(Exception e){
       						
       						//System.out.println("loadingDialog exception"+e);
       					}
                        
   						
   					}
	                
	            } else {  
	                imageView.setImageBitmap(placeholder);  
	                queueJob(url,position, imageView, width, height);  
	            }  
	        } 
	     
	     private Bitmap downloadBitmap(String url, int width, int height) {  
	            try {  
	            	//url="http://dev-fsit.com/iphone/image/alex profilefirst.jpg";
	            	
	               // Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());  
	                
	               /*  ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
					 bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutput);
					 byte [] byteArray = byteArrayOutput.toByteArray();
					 String imageString=Base64.encodeBytes(byteArray);       
	                 imageList.add(imageString);
	                
	                */
	               // bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);  
	                
	                
	                BitmapFactory.Options optnsSizeOnly = new BitmapFactory.Options();
	            	optnsSizeOnly.inJustDecodeBounds = true;
	            	BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, optnsSizeOnly);

	            	final int REQUIRED_SIZE = 80;
	            			 int width_tmp = optnsSizeOnly.outWidth;
	            			 int height_tmp = optnsSizeOnly.outHeight;
	            			 
	            			 int scale = 1;
	            			 
	            			 while (true) {
	            		            if (width_tmp / 2 < REQUIRED_SIZE
	            		               || height_tmp / 2 < REQUIRED_SIZE) {
	            		                break;
	            		            }
	            		            width_tmp /= 2;
	            		            height_tmp /= 2;
	            		            scale *= 2;
	            		        }
	            			 
	            			 BitmapFactory.Options o2 = new BitmapFactory.Options();
	            			 o2.inSampleSize = scale;
	            	Bitmap	bitmap= BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, o2); 
	                cache.put(url, new SoftReference<Bitmap>(bitmap));  
	                return bitmap;  
	                
	            } catch (MalformedURLException e) {  
	                e.printStackTrace();  
	            } catch (IOException e) {  
	                e.printStackTrace();  
	            }  
	      
	            	return null;  
	        }
	     
	     
	 } 
	 
	 
	 public enum ProfileBitmapManager {  
		 
		 PROFILEINSTANCE;  
	      Bitmap profileBitmap; 
	      
	      private final ExecutorService profilePool;  
	      
	      ProfileBitmapManager() {  
	            
	            profilePool = Executors.newFixedThreadPool(5);  
	      } 
	        
	      
	      public void profileQueueJob(final String url,final int position,final String userId, final int size){
	    	  
	    	  
	    	  
	    	  final Handler handler = new Handler() {  
	                @Override  
	                public void handleMessage(Message msg) {  
	                    
	                   
	                        if (msg.obj != null) { 
	                        	
	                        ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
	       					 ((Bitmap) msg.obj).compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutput);
	       					 byte [] byteArray = byteArrayOutput.toByteArray();
	       					// String imageString=Base64.encodeBytes(byteArray);    
	       					HashMap<String, byte[]> map=new HashMap<String, byte[]>();
	       					map.put("profile",byteArray);
	       					//imageList.set(position, map);
	       					
	       					ContentValues values=new ContentValues();
	       					values.put(CardTable.KEY_PROFILE_PIC, byteArray);
	       					
	       					Uri queryUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL + "/" + userId);
	       					 
	       					 resolver.update(queryUri, values, null, null);
	       					 
	       					((Bitmap) msg.obj).recycle();
	       					 
	       					if(size-1 ==position){
	       						System.out.println("item ==="+position);
	       						
	       						try{
	       							
	       							
	       							pd.dismiss();
	       							pd=null;
	       							//Toast.makeText(context, "Cards Downloaded", Toast.LENGTH_LONG).show();
	       							alertFunction("Successfully Downloaded",0);
	       							
	       						}catch(Exception e){
	       							Log.d("NewFetch", "error"+e);
	       							
	       						}
	       						
	       						try{
	       							
	       							
	       							bar.dismiss();
	       							bar=null;
	       							//Toast.makeText(context, "Update Successfull", Toast.LENGTH_LONG).show();
	       							alertFunction("Update Successfull",0);
	       						
	       						}catch(Exception e){
	       							Log.d("NewFetch", "error"+e);
	       							
	       						}
	       						
	       						
	       						try{
	       							
	       							progress.dismiss();
	       							progress=null;
	       							//Toast.makeText(context, "Cards Downloaded", Toast.LENGTH_SHORT).show();
	       							alertFunction("Cards Downloaded",0);
	       							
	       						}catch(Exception e){
	       							
	       							Log.d("NewFetch", "error"+e);
	       						}
	       						
	       						
	       					}
	       					
	       	                  //imageList.set(position, imageString);
	       	                  
	       	                 // System.out.println("profile url :"+url);
	       	                 // System.out.println("profile position :"+position);
	                             
	                        } else {  
	                            
	                            Log.d(null, "fail " + url);  
	                        }  
	                    }  
	                  
	            }; 
	            
	            profilePool.submit(new Runnable() {  
	                 
	                public void run() {  
	                    final Bitmap bmp = profileDownloadBitmap(url);  
	                    Message message = Message.obtain();  
	                    message.obj = bmp;  
	                    //Log.d(null, "Item downloaded: " + url);  
	      
	                    handler.sendMessage(message);  
	                }  
	            });
	    	  
	      }
	      
	      
	      private Bitmap profileDownloadBitmap(String url) {  
	            try {  
	            	BitmapFactory.Options optnsSizeOnly = new BitmapFactory.Options();
	            	optnsSizeOnly.inJustDecodeBounds = true;
	            	BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, optnsSizeOnly);

	            	final int REQUIRED_SIZE = 80;
	            			 int width_tmp = optnsSizeOnly.outWidth;
	            			 int height_tmp = optnsSizeOnly.outHeight;
	            			 
	            			 int scale = 1;
	            			 
	            			 while (true) {
	            		            if (width_tmp / 2 < REQUIRED_SIZE
	            		               || height_tmp / 2 < REQUIRED_SIZE) {
	            		                break;
	            		            }
	            		            width_tmp /= 2;
	            		            height_tmp /= 2;
	            		            scale *= 2;
	            		        }
	            			 
	            			 BitmapFactory.Options o2 = new BitmapFactory.Options();
	            			 o2.inSampleSize = scale;
	            			 profileBitmap= BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, o2);  
	                
	                return profileBitmap;  
	            } catch (MalformedURLException e) {  
	                e.printStackTrace();  
	            } catch (IOException e) {  
	                e.printStackTrace();  
	            }  
	      
	            return null;  
	        } 
		 
	 }
	 
	 
	 
	 
public enum LogoBitmapManager {  
		 
		  LOGOINSTANCE;  
	      Bitmap logoBitmap; 
	      
	      private final ExecutorService logoPool;  
	      
	      LogoBitmapManager() {  
	            
	    	  logoPool = Executors.newFixedThreadPool(5);  
	        } 
	        
	      
	      public void logoQueueJob(final String url, final int position,final String userId, final int size){
	    	  
	    	  
	    	  
	    	  
	    	  final Handler handler = new Handler() {  
	                @Override  
	                public void handleMessage(Message msg) {  
	                	
	                   
	                        if (msg.obj != null) { 
	                        	
	                        
	                        	ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
		       					 ((Bitmap) msg.obj).compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutput);
		       					 byte [] byteArray = byteArrayOutput.toByteArray();
		       					 String imageString=Base64.encodeBytes(byteArray);  
		       					
		       					HashMap<String, byte[]> map=new HashMap<String, byte[]>();
		       					map.put("logo",byteArray);
		       					//logoList.set(position, map);
		       					//System.out.println("item ==="+position);
		       					//System.out.println("size ==="+size);
		       					
		       					
		       					ContentValues values=new ContentValues();
		       					values.put(CardTable.KEY_LOGO, byteArray);
		       					
		       					Uri queryUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL + "/" + userId);
		       					 
		       					 resolver.update(queryUri, values, null, null);
		       					 
		       					((Bitmap) msg.obj).recycle();
		       					//logoList.set(position, imageString);
	                        	
	       	                  
	       	                //  System.out.println("logo url :"+url);
	       	                //  System.out.println("logo position :"+position);
	                             
	                        } else {  
	                            
	                            Log.d(null, "fail " + url);  
	                        }  
	                    }  
	                  
	            }; 
	            
	            logoPool.submit(new Runnable() {  
	                 
	                public void run() {  
	                    final Bitmap bmp = logoDownloadBitmap(url);  
	                    
	                    Message message = Message.obtain();  
	                    message.obj = bmp;  
	                   // Log.d(null, "Item downloaded: " + url);  
	      
	                    handler.sendMessage(message);  
	                }  
	            });
	    	  
	      }
	      
	      
	      private Bitmap logoDownloadBitmap(String url) {  
	    	  try {  
	    		  
	            	BitmapFactory.Options optnsSizeOnly = new BitmapFactory.Options();
	            	optnsSizeOnly.inJustDecodeBounds = true;
	            	BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, optnsSizeOnly);

	            	final int REQUIRED_SIZE = 80;
	            			 int width_tmp = optnsSizeOnly.outWidth;
	            			 int height_tmp = optnsSizeOnly.outHeight;
	            			 
	            			 int scale = 1;
	            			 
	            			 while (true) {
	            		            if (width_tmp / 2 < REQUIRED_SIZE
	            		               || height_tmp / 2 < REQUIRED_SIZE) {
	            		                break;
	            		            }
	            		            width_tmp /= 2;
	            		            height_tmp /= 2;
	            		            scale *= 2;
	            		        }
	            			 
	            			 BitmapFactory.Options o2 = new BitmapFactory.Options();
	            			 o2.inSampleSize = scale;
	            			 logoBitmap= BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, o2);  
	                
	                return logoBitmap;  
	            } catch (MalformedURLException e) {  
	                e.printStackTrace();  
	            } catch (IOException e) {  
	                e.printStackTrace();  
	            }  
	      
	            return null;  
	        } 
		 
	 }
	

@Override
public void onBackPressed() {
	
	
	SharedPreferences deleteSettings = getApplicationContext().getSharedPreferences("Myprofile", MODE_PRIVATE);  
    int bumpActive= deleteSettings.getInt("bump", 0);
    final int auto=deleteSettings.getInt("auto", 0);
    
  /*  if(bumpActive == 1){
    	
    	MainActivity parent;
    	parent =(MainActivity) this.getParent();
    	//parent.bumpServiceCancel(bumpActive);
    	
    }
    */
	
	
	//super.onBackPressed();
	
	AlertDialog.Builder builder = new AlertDialog.Builder(NewFetch.this);
	builder.setTitle("Exit");
	builder.setCancelable(false);
	builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

	  public void onClick(DialogInterface dialog, int arg1) {
	  // do something when the OK button is clicked
		  
		  dialog.dismiss();
		  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor prefEditor = loginSettings.edit();
			
			//if(auto==0){
				
				prefEditor.putString("user_name", null);  
			    prefEditor.putString("password", null);
			    prefEditor.putString("user_id", null);
				prefEditor.putInt("number", 3);
				
			/*}else{
				
				prefEditor.putInt("number", 0);
				
			}*/
			 
			prefEditor.commit();
	        
		 finish();
		  
	  }});
	
	builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
		
		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			  
			  dialog.dismiss();
			  
			  
				   return;
				
				
				
		        
			  
		  }});
	
	
	
	builder.setMessage("You want to exit application").create().show();
}



	public void bumpEnable(){
		
		int value=0;
		MainActivity parent;
        parent =(MainActivity) NewFetch.this.getParent();
        parent.manipulateBump(value);
	}
	
	
	public void bumpDisable(){
		
		int value=1;
		MainActivity parent;
        parent =(MainActivity) NewFetch.this.getParent();
        parent.manipulateBump(value);
	}


public void run() {
	// TODO Auto-generated method stub
	
	
	String cursor_id;
	String c_profile_pic;
	String c_profile_logo;
	this.mainListView=getListView();
	int count = mainListView.getCount();
	
	
	
	for(int i=0;i<count;i++){
		
		//
		byte[] decodedPicByte=null;
		byte[] decodedLogoByte=null;
		
		 
		
		@SuppressWarnings("unchecked")
		HashMap<String, String> array = (HashMap<String, String>) mainListView.getItemAtPosition(i);
		
		String[] value= new String[] {CardTable.KEY_NAME, CardTable.KEY_USERID};
		
		String c_id = array.get("id");
         String c_name = array.get("name");
         String c_number = array.get("phone");
         String c_address = array.get("company_address");
         String c_comp_name = array.get("company");
         String c_designation = array.get("designation");
         String c_email = array.get("email");
         String c_profile_description=array.get("description");
         String c_profile_date=array.get("updateDate");
         String c_profile_video=array.get("video");
         String c_profile_comments = array.get("comments");
         String c_profile_url = array.get("webUrl");
         String c_office = array.get("office");
         String c_fax = array.get("fax");
         
         
         
         
         if(mySearchList ==null ){
        	 
        	 
        	 /* HashMap<String, byte[]> arrayImage=imageList.get(i);
	         HashMap<String, byte[]> arrayLogo=logoList.get(i);
	         
	         decodedPicByte=arrayImage.get("profile");
	         decodedLogoByte=arrayLogo.get("logo");
	         
	         
	        try {
	        	 decodedPicByte = Base64.decode(c_profile_pic.getBytes());
					
				
					decodedLogoByte = Base64.decode(c_profile_logo.getBytes());
					
			
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
        	 
        	 decodedPicByte=byteGlobalArray;
	         decodedLogoByte=byteGlobalLogoArray;
        	 
        	 
        	 
         }else{
        	 
        	 /* HashMap<String, String> searchData= mySearchList.get(i);
        	 String number=searchData.get("number");
        	 HashMap<String, byte[]> arrayImage =imageList.get(Integer.parseInt( number));
        	 HashMap<String, byte[]> arrayLogo=logoList.get(Integer.parseInt( number));
        	 decodedPicByte=arrayImage.get("profile");
        	 decodedLogoByte=arrayLogo.get("logo");
        	 
        	 try {
        		 decodedPicByte = Base64.decode(c_profile_pic.getBytes());
					
				
					decodedLogoByte = Base64.decode(c_profile_logo.getBytes());
					
			
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	 */
         }
         
         
       //  String c_profile_pic=imageList.get(i);
        // String c_profile_logo=logoList.get(i);
         
        // System.out.println("profile :"+c_profile_pic);
        // System.out.println("logo :"+c_profile_logo);
         
         
         decodedPicByte=byteGlobalArray;
         decodedLogoByte=byteGlobalLogoArray;
         
         Uri queryUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL + "/" + c_id);
         Cursor cursor = getContentResolver().query(queryUri, value, null, null, null);
         
         
        ContentValues values=new ContentValues();
 		values.put(CardTable.KEY_USERID, c_id);
 		values.put(CardTable.KEY_NAME, c_name);
 		values.put(CardTable.KEY_EMAIL, c_email);
 		values.put(CardTable.KEY_NUMBER, c_number);
 		values.put(CardTable.KEY_PROFILE_PIC, decodedPicByte);
 		values.put(CardTable.KEY_DESIGNATION, c_designation);
 		values.put(CardTable.KEY_VIDEO, c_profile_video);
 		values.put(CardTable.KEY_COMPANY, c_comp_name);
 		values.put(CardTable.KEY_ADDRESS, c_address);
 		values.put(CardTable.KEY_LOGO, decodedLogoByte);
 		values.put(CardTable.KEY_DESCRIPTION, c_profile_description);
		values.put(CardTable.KEY_UPDATE_DATE, c_profile_date);
		values.put(CardTable.KEY_COMMENTS, c_profile_comments);
		values.put(CardTable.KEY_WEB_ADDRESS, c_profile_url);
		values.put(CardTable.KEY_OFFICE, c_office);
		values.put(CardTable.KEY_FAX, c_fax);
         
         
 		
 		
 		if ( cursor != null && cursor.getCount() > 0 ) {
 		    if (cursor.moveToFirst()) {
 		        // loop until it reach the end of the cursor
 		        do {
 		        	cursor_id=cursor.getString(cursor.getColumnIndexOrThrow(CardTable.KEY_USERID));
 		        	

 		        	 if(c_id.equals(cursor_id)){
 		        		 
 		        		 getContentResolver().update(queryUri, values, null, null);
 		        	 }else {
 		        		 //System.out.println("Value not exist inserting");
 		        		 getContentResolver().insert(CardManagingProvider.CONTENT_URI_DETAIL, values);
 		        		//getContentResolver().update(queryUri, values, null, null);
 		        	 }
 		        	
 		        } while (cursor.moveToNext());
 		    }

 		    // make sure to close the cursor
 		   // cursor.close();
 		}else {
        	 
        	 getContentResolver().insert(CardManagingProvider.CONTENT_URI_DETAIL, values);
	    }
         
 		 cursor.close();	         
	}
	
	
	handler.sendEmptyMessage(DOWNLOADALL);
}

private Handler handler=new Handler(){
	
	public void handleMessage(Message msg){
		
		switch(msg.what){
		
		case DOWNLOADALL:
			
			try{
				
				
				//pd.dismiss();
				//pd=null;
				//Toast.makeText(NewFetch.this, "Downloading has been completed", Toast.LENGTH_LONG).show();
				
				
			}catch(Exception e){
				Log.d("NewFetch", "error"+e);
				
			}
			
			bumpEnable();
			
			
			break;
			
		case SYNCHRONIZE:
			
			
			try{
				
				//isRunning=false;
				//bar.dismiss();
				//bar=null;
				//Toast.makeText(NewFetch.this, "Synchronization has been completed", Toast.LENGTH_LONG).show();
			
			}catch(Exception e){
				Log.d("NewFetch", "error"+e);
				
			}
			bumpEnable();
			
			break;
			
		case SYNCHRONIZE_NONE:
			try{
				
				isRunning=false;
				bar.dismiss();
				bar=null;
				
				//Toast.makeText(NewFetch.this,"No data in local Database.First Download something", Toast.LENGTH_LONG).show();
			
		}catch(Exception e){
			Log.d("NewFetch", "error"+e);
			
		}
		
		bumpEnable();
		//Toast.makeText(NewFetch.this, "Updation has been completed", Toast.LENGTH_SHORT).show();
		alertFunction("Update Successfull",0);
		
			break;
		case DOWNLOAD:
			try{
				
				//progress.dismiss();
				
				
				
			}catch(Exception e){
				
				Log.d("NewFetch", "error"+e);
			}
			
			
			
			  			
			SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor prefEditor = loginSettings.edit();  
			prefEditor.putInt("check", 0); 
			prefEditor.commit();  
			
			String data=searchText();
			
			if(data.length()==0){
				
				mAdapter = new MyCustomAdapter(NewFetch.this,myList,NORMAL);
				 
				 setListAdapter(mAdapter);
				
			}else{
				
				doSearch(data);
				
			}
			
			
			//mAdapter = new MyCustomAdapter(NewFetch.this,myList,NORMAL);
			 
			//setListAdapter(mAdapter);
			
			bumpEnable();
			
			break;
		}
		
		
		
	}
};



	public static void alertFunction(String message, int check){
		AlertDialog alert=null;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			
			  dialog.dismiss();
			  
			  
		  }});
		if(check == 1){
			builder.setTitle("Warning");
		}
		builder.setMessage(message);
		alert=builder.create();
		alert.show();
		
	}
	
}

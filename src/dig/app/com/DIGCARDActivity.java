package dig.app.com;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;

public class DIGCARDActivity extends Activity {
    /** Called when the activity is first created. */
	
	private  int SPLASH_DISPLAY_TIME=2000;
	private int value=0;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        boolean tablet=isTablet(this);
        
        if(tablet){
        	
        	SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor prefEditor = loginSettings.edit();  
			prefEditor.putInt("tablet", 1);
			prefEditor.commit();
			value=1;
			
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			
        }else{
			
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			
		}
        
        MyAppData appState = ((MyAppData)getApplication());
		appState.tablet(value);
        
        new Handler().postDelayed(new Runnable(){

			public void run() {
				// TODO Auto-generated method stub
				
				
				Intent intent=new Intent(DIGCARDActivity.this,MainActivity.class);
				startActivity(intent);
				DIGCARDActivity.this.finish();
			}
        	
        }, SPLASH_DISPLAY_TIME);
        	
        
    }
    
    
    public boolean isTablet(Context context) {  
        return (context.getResources().getConfiguration().screenLayout   
                & Configuration.SCREENLAYOUT_SIZE_MASK)    
                >= Configuration.SCREENLAYOUT_SIZE_LARGE; 
	}
    
    public void onResume(){
    	super.onResume();
    	
    	//com.facebook.Settings.publishInstallAsync(this, "659621604053010");
    }
    
}
package dig.app.com;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.FormBodyPart;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

@SuppressWarnings("unused")
public class NewRegister extends Activity{
	
	InputStream is=null;
	String result=null;
	boolean connection;
	String username,card_name,card_number,card_address,card_company_name,card_designation,card_email,password,video,description,comments,web_address,office,fax;
	TextView inputName,inputNumber,designation,companyName,companyAddress,inputEmail,userName,passWord,inputVideo,inputDescription,inputComment,inputUrl,inputOffice,inputFax;
	private ArrayList<String> response=new ArrayList<String>();
	ImageView profilePic,companyLogo;
	String filePath,fileImagePath = null;
	String videoId;
	private Bitmap bmLogo,bm;
	private KeyListener listener;
	private Bitmap mIcon1,mLogo,logoThumb;
	static Context context;
	Button Save;
	int imageSelect=0;
	private LinearLayout registerLayout;
	
	private static final int REQUEST_PICK_FILE=1;
	private static final int REQUEST_PICK_IMAGE=2;
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.form);
		
		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
		SharedPreferences.Editor prefEditor = loginSettings.edit();  
		prefEditor.putInt("number", 2); 
		prefEditor.commit(); 
		
		context = this;
		registerLayout=(LinearLayout) findViewById(R.id.register_layout);
		
		inputName=(TextView) findViewById(R.id.register_name);
		inputNumber=(TextView) findViewById(R.id.register_phone);
		designation=(TextView) findViewById(R.id.register_designation);
		companyName=(TextView) findViewById(R.id.register_company);
		companyAddress=(TextView) findViewById(R.id.register_address);
		inputEmail=(TextView) findViewById(R.id.register_email);
		userName=(TextView) findViewById(R.id.register_username);
		passWord=(TextView) findViewById(R.id.register_password);
		inputVideo=(TextView) findViewById(R.id.register_video);
		inputDescription=(TextView) findViewById(R.id.register_description);
		inputComment=(TextView) findViewById(R.id.register_comment);
		inputUrl=(TextView) findViewById(R.id.register_url);
		inputOffice=(TextView) findViewById(R.id.register_office);
		inputFax=(TextView) findViewById(R.id.register_fax);
		//Save=(Button) findViewById(R.id.submit);
		
		fileImagePath="logo";
		filePath="profile";
		
		listener = inputName.getKeyListener();
		
		profilePic=(ImageView) findViewById(R.id.register_pic);
		companyLogo=(ImageView) findViewById(R.id.register_company_logo);
		
		mIcon1=BitmapFactory.decodeResource(this.getResources(), R.drawable.profile_pic);
		mLogo=BitmapFactory.decodeResource(this.getResources(), R.drawable.logo);
		logoThumb=BitmapFactory.decodeResource(this.getResources(), R.drawable.logo_thumbnail);
		/*Save.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
			}
		});*/
	}
	
	
	
	/*@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		System.out.println("save menu");
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.save_menu, menu);
		return true;
	}
	*/
	
	public void onStart(){
		super.onStart();
	
		registerLayout.requestFocus();
		
	}
	
	
	public boolean onMenuItemSelected(int featureId, MenuItem item){
		
		switch(item.getItemId()){
			
			case R.id.menu_save:
				
				validation();
				break;
				
			
		}
		return super.onMenuItemSelected(featureId, item);
		
		
	}
	
	
	public void validation(){
		
		boolean emailCheck,numberCheck,videoCheck,urlCheck,nameCheck;
		int success=0,valid=0,ready=0;
		
		username=userName.getText().toString();
		card_email=inputEmail.getText().toString();
		card_name=inputName.getText().toString();
		password=passWord.getText().toString();
		card_company_name=companyName.getText().toString();
		card_designation=designation.getText().toString();
		card_address=companyAddress.getText().toString();
		card_number=inputNumber.getText().toString();
		video=inputVideo.getText().toString();
		description=inputDescription.getText().toString();
		comments=inputComment.getText().toString();
		web_address=inputUrl.getText().toString();
		
		nameCheck=checkName(card_name);
		emailCheck=checkEmail(card_email);
		numberCheck=checkNumber(card_number);
		videoCheck=checkVideo(video);
		urlCheck=checkUrl(web_address);
		/*
		if(card_name.equalsIgnoreCase("")){
			makeToast("Name");
		}else if(username.equalsIgnoreCase("")){
			makeToast("user name");
		}else if(password.equalsIgnoreCase("")){
			makeToast("Password");
		}else if(card_email.equalsIgnoreCase("")){
			makeToast("Email Address");
		}else if(emailCheck==false){
			Toast.makeText(NewRegister.this, "Please enter a valid email address", Toast.LENGTH_SHORT).show();
		}else if(card_number.equalsIgnoreCase("")){
			makeToast("Number");
		}else if(numberCheck==false){
			Toast.makeText(NewRegister.this, "Please enter only numbers in Phone field", Toast.LENGTH_SHORT).show();
		}else if(card_company_name.equalsIgnoreCase("")){
			makeToast("Company Name");
		}else if(card_designation.equalsIgnoreCase("")){
			makeToast("Designation");
		}else if(card_address.equalsIgnoreCase("")){
			makeToast("Address");
		}else if(description.equalsIgnoreCase("")){
			makeToast("Description");
		}else if(video.equalsIgnoreCase("")){
			serverConnection();
		}else if(videoCheck ==false){
			
			Toast.makeText(NewRegister.this, "Video ID is missing.Enter a valid youtube video URL.", Toast.LENGTH_SHORT).show();
			
		}else{
			serverConnection();
			
		}*/
		
		
		if(card_name.equalsIgnoreCase("")){
			makeToast("Name");
			success=0;
		}else if(nameCheck==false){
			//Toast.makeText(NewRegister.this, "Please enter a valid name", Toast.LENGTH_SHORT).show();
			alertFunction("Enter a valid name",1);
			success=0;
		}else if(username.equalsIgnoreCase("")){
			makeToast("user name");
			success=0;
		}else if(password.equalsIgnoreCase("")){
			makeToast("Password");
			success=0;
		}else if(card_email.equalsIgnoreCase("")){
			makeToast("Email Address");
			success=0;
		}else if(emailCheck==false){
			alertFunction("Enter a valid email address",1);
			//Toast.makeText(NewRegister.this, "Please enter a valid email address", Toast.LENGTH_SHORT).show();
			success=0;
		}else if(card_number.equalsIgnoreCase("")){
			makeToast("Number");
			success=0;
		}else if(numberCheck==false){
			alertFunction("Enter a valid number",1);
			//Toast.makeText(NewRegister.this, "Please enter only numbers in Phone field", Toast.LENGTH_SHORT).show();
			success=0;
		}else{
			success=1;
			
		}/*else if(card_company_name.equalsIgnoreCase("")){
			makeToast("Company Name");
			success=0;
		}else if(card_designation.equalsIgnoreCase("")){
			makeToast("Title");
			success=0;
		}else if(card_address.equalsIgnoreCase("")){
			makeToast("Address");
			success=0;
		}else if(description.equalsIgnoreCase("")){
			makeToast("Description");
			success=0;
		}else if(comments.equalsIgnoreCase("")){
			//makeToast("Comments");
			success=1;
		}*/
		
		 if(web_address.equalsIgnoreCase("")){
			
			 valid=1;
		}else if(urlCheck ==false){
			alertFunction("Enter a valid Web Address",1);
			//Toast.makeText(NewRegister.this, "Enter a valid Web Address.", Toast.LENGTH_SHORT).show();
			valid=0;
		}else{
			
			valid=1;
			
		}
		 
		 
		
		if(video.equalsIgnoreCase("")){
			
			ready=1;
		}else if(videoCheck ==false){
			alertFunction("Video ID is missing.Enter a valid youtube video URL with 'v' parameter",1);
			//Toast.makeText(NewRegister.this, "Video ID is missing.Enter a valid youtube video URL with 'v' parameter.", Toast.LENGTH_LONG).show();
			ready=0;
		}else{
			
			
			Uri uri = Uri.parse(video);
			
			try{
				
				 videoId =  uri.getQueryParameter("v");
				 ready=1;
				 
				 
				
			}catch(Exception e){
				
				Log.i("Register", "no video id");
			}
			
		}
		
		if(success==1 && ready==1 && valid==1){
		
			
			//Toast.makeText(this, "Insertion success", Toast.LENGTH_SHORT).show();
			serverConnection();
			
		}
		
	}
	
	
	
	private boolean checkName(String name){
		
		Pattern pattern = Pattern.compile("^[a-zA-Z \\.]+$");
		Matcher matcher = pattern.matcher(name);
		return matcher.matches();
		
		
		//return connection;
		
	}
	
	private boolean checkUrl(String url){
		
		
			
			if (!url.startsWith("https://") && !url.startsWith("http://")){
			    url = "http://" + url;
			}
		
		
		return URLUtil.isValidUrl(url);
		
		/*String regex = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";

		//Pattern pattern = Pattern.compile(regex);
		Pattern patt = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = patt.matcher(url);
		//return matcher.matches();
		return true;*/
		
		
	}
	
	private boolean checkVideo(String video){
		
		String video_path = video;
		Uri uri = Uri.parse(video_path);
		int count=0;
		try{
			
			String	videoId =  uri.getQueryParameter("v");
			 count=videoId.length();
			
		}catch(Exception e){
			
			Log.i("Register", "no video id");
		}
		
		if(count > 0){
			
			return true;
			
		}else{
			
			return false;
		}
		
		
	}
	
	private boolean checkNumber(String number){
		if(number.length()<=15){
			Pattern pattern = Pattern.compile("^[0-9]+$");
			Matcher matcher = pattern.matcher(number);
			return matcher.matches();
		}else{
			return false;
		}
		
		
		
	}
	
	private boolean checkEmail(String email){
		
		
		//final String regularExp = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		final String MAIL_REGEX = "([_A-Za-z0-9-]+)(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})";
		Pattern pattern = Pattern.compile(MAIL_REGEX);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

		
	}
	
	private void makeToast(String str){
		
		String notice="Please enter the required fields";
		
		//Toast.makeText(NewRegister.this, notice, Toast.LENGTH_SHORT).show();
		AlertDialog alert=null;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(NewRegister.this);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			
			  dialog.dismiss();
			  
			  
		  }});
		builder.setTitle("Warning");
		builder.setMessage(notice);
		alert=builder.create();
		alert.show();
		
	}
	
	
	private void serverConnection() {
		// TODO Auto-generated method stub
		if(connection){
			InsertData task = new InsertData(this);
			//task.execute(new String[] {"http://dev-fsit.com/iphone/digcard/register.php"});
			//task.execute(new String[] {"http://dev-fsit.com/iphone/register.php"});
			//http://dev-fsit.com/iphone/digcard/
			task.execute(new String[] {"http://digcardapp.com/app/register.php"});
			//http://192.168.1.187/android/register.php
			
			bumpDisable();
			
		}else{
			alertFunction("No Internet Connection", 1);
			//Toast.makeText(NewRegister.this, "No network connection available", Toast.LENGTH_LONG).show();
			
		}
		
		
		
	}	
	
	
	
	public void bumpDisable(){
		
		int value=1;
		MainActivity parent;
        parent =(MainActivity) NewRegister.this.getParent();
        parent.manipulateBump(value);
	}
	
	
	public void bumpEnable(){
		
		int value=0;
		MainActivity parent;
        parent =(MainActivity) NewRegister.this.getParent();
        parent.manipulateBump(value);
	}
	
	 public void onResume(){
			super.onResume();
			
			//com.facebook.Settings.publishInstallAsync(this, "659621604053010");
		    
			MainActivity parent;
	    	parent =(MainActivity) this.getParent();
	    	parent.manipulateTitle("Register");
			
			SharedPreferences oneTimeSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	        int check = oneTimeSettings.getInt("oneTimeCheck", 0);
	        String  l_name  = oneTimeSettings.getString("oneTimeName", null);
	        if(check ==1){
	        	inputName.setKeyListener(null);
	        	inputName.setEnabled(false);
	        	inputName.setText(l_name);
	        }else if(imageSelect !=1){
	        	inputName.setKeyListener(listener);
	        	inputName.setEnabled(true);
	        	inputName.setText("");
	        }else{
	        	imageSelect =0;
	        }
			
			SharedPreferences itemSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
			SharedPreferences.Editor prefEditor = itemSettings.edit();  
			prefEditor.putInt("number", 2); 
			prefEditor.commit(); 
			
		 connection=haveNetworkConnection(); 
		 
		 if(connection ==false){
			 alertFunction("No Internet Connection",1);
			 //Toast.makeText(NewRegister.this, "No network connection available", Toast.LENGTH_LONG).show();
		 }
			
		 //bumpServiceStart();
	  }
	 
	/* 
	 private void bumpServiceStart(){
			
			SharedPreferences deleteSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	        int bumpActive= deleteSettings.getInt("bump", 0);
	        int recieverActive=deleteSettings.getInt("reciever", 0);
	        
	        if(bumpActive==0){
	        	
	        	System.out.println("going to start bump service");
	        	
	        	MainActivity parent;
	        	parent =(MainActivity) this.getParent();
	        	parent.bumpServiceStart();
	        	
	        	
	        }	
		}
	  */
	  
	  private boolean haveNetworkConnection() {
		    boolean haveConnectedWifi = false;
		    boolean haveConnectedMobile = false;

		    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		    NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		    for (NetworkInfo ni : netInfo) {
		        if (ni.getTypeName().equalsIgnoreCase("WIFI"))
		            if (ni.isConnected())
		                haveConnectedWifi = true;
		        if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
		            if (ni.isConnected())
		                haveConnectedMobile = true;
		    }
		    return haveConnectedWifi || haveConnectedMobile;
		}
	
	
	
	private class InsertData extends AsyncTask<String,Integer,ArrayList<String>>{
		

		String ba1,byteLogo=null;
		byte [] ba,imageBy;
		private ProgressDialog loadingDialog;
		private Context mContext;
		
		
	InsertData(Context context)
        {
			
			mContext = context;
            loadingDialog = new ProgressDialog(mContext);
            loadingDialog.setMessage("Registering..");
            loadingDialog.setCancelable(false);
            loadingDialog.show();
        }
		@Override
		protected ArrayList<String> doInBackground(String... urls) {
			// TODO Auto-generated method stub
			
			
			username=userName.getText().toString().trim();
			card_email=inputEmail.getText().toString().trim();
			card_name=inputName.getText().toString().trim();
			password=passWord.getText().toString().trim();
			card_company_name=companyName.getText().toString().trim();
			card_designation=designation.getText().toString().trim();
			card_address=companyAddress.getText().toString().trim();
			card_number=inputNumber.getText().toString().trim();
			video=inputVideo.getText().toString().trim();
			description=inputDescription.getText().toString().trim();
			comments=inputComment.getText().toString().trim();
			web_address=inputUrl.getText().toString().trim();
			office=inputOffice.getText().toString().trim();
			fax=inputFax.getText().toString().trim();
			
			
			if(videoId==null){
				
				
				
				videoId="";
			}
			/*if(comments==null){
				comments="";
			}
			if(web_address==null){
				web_address="";
			}*/
			
			
			if(bm!=null ){
			
			ByteArrayOutputStream bao = new ByteArrayOutputStream();
			bm.compress(Bitmap.CompressFormat.JPEG, 90, bao);
			ba = bao.toByteArray();
			ba1=Base64.encodeBytes(ba);
			
			
			 
			
			}else{
				
				ByteArrayOutputStream bao = new ByteArrayOutputStream();
				mIcon1.compress(Bitmap.CompressFormat.JPEG, 90, bao);
				ba = bao.toByteArray();
				ba1=Base64.encodeBytes(ba);
				
								
				
			}
			
			
			
			if(bmLogo!=null){
			
				ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
				bmLogo.compress(Bitmap.CompressFormat.JPEG, 90, byteArray);
				imageBy = byteArray.toByteArray();
				byteLogo=Base64.encodeBytes(imageBy);
				
				 
			
			}else{
				
				ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
				logoThumb.compress(Bitmap.CompressFormat.JPEG, 90, byteArray);
				imageBy = byteArray.toByteArray();
				byteLogo=Base64.encodeBytes(imageBy);
				
				
				
				
			}
			for (String url : urls) {
						
						
						
						try {
							
							DefaultHttpClient httpclient = new DefaultHttpClient();
							HttpContext localContext = new BasicHttpContext();

							HttpPost httppost = new HttpPost(url);
							
							/*							
							List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				            nameValuePairs.add(new BasicNameValuePair("username", username));
				            nameValuePairs.add(new BasicNameValuePair("card_email", card_email));
				            nameValuePairs.add(new BasicNameValuePair("card_name", card_name));
				            nameValuePairs.add(new BasicNameValuePair("password", password));
				            nameValuePairs.add(new BasicNameValuePair("card_company_name", card_company_name));
				            nameValuePairs.add(new BasicNameValuePair("card_designation", card_designation));
				            nameValuePairs.add(new BasicNameValuePair("card_address", card_address));
				            nameValuePairs.add(new BasicNameValuePair("card_number", card_number));
				            nameValuePairs.add(new BasicNameValuePair("image",ba1));
				            nameValuePairs.add(new BasicNameValuePair("Logo",byteLogo));
				            nameValuePairs.add(new BasicNameValuePair("video",video));
				            nameValuePairs.add(new BasicNameValuePair("LogoPath",fileImagePath));
				            nameValuePairs.add(new BasicNameValuePair("imagePath",filePath));
				            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
							*/
							
							
							
							
							
							MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
							
							FormBodyPart bodyPart=new FormBodyPart("uname", new StringBody(username));
													entity.addPart(bodyPart);
													
										 bodyPart=new FormBodyPart("password", new StringBody(password));
										 			entity.addPart(bodyPart);
										 			
										 bodyPart=new FormBodyPart("name", new StringBody(card_name));
										 			entity.addPart(bodyPart);
										 			
										 bodyPart=new FormBodyPart("email", new StringBody(card_email));
										 			entity.addPart(bodyPart); 
										 			
										 bodyPart=new FormBodyPart("companyname", new StringBody(card_company_name));
										 			entity.addPart(bodyPart);
										 bodyPart=new FormBodyPart("designation", new StringBody(card_designation));
										 			entity.addPart(bodyPart);
										 bodyPart=new FormBodyPart("companyAddr", new StringBody(card_address));
										 			entity.addPart(bodyPart);
										 bodyPart=new FormBodyPart("phno", new StringBody(card_number));
										 			entity.addPart(bodyPart);
							 			 bodyPart=new FormBodyPart("description", new StringBody(description));
										 			entity.addPart(bodyPart);
										 bodyPart=new FormBodyPart("LogoPath", new StringBody(fileImagePath));
										 			entity.addPart(bodyPart);
										 bodyPart=new FormBodyPart("imagePath", new StringBody(filePath));
										 			entity.addPart(bodyPart);
										 bodyPart=new FormBodyPart("video", new StringBody(videoId));
										 			entity.addPart(bodyPart);
										 bodyPart=new FormBodyPart("comment", new StringBody(comments));
										 			entity.addPart(bodyPart);
										 bodyPart=new FormBodyPart("web_url", new StringBody(web_address));
										 			entity.addPart(bodyPart);				
							 			 bodyPart=new FormBodyPart("office", new StringBody(office));
							 			 			entity.addPart(bodyPart);	
							 			 bodyPart=new FormBodyPart("fax", new StringBody(fax));
							 			 			entity.addPart(bodyPart);	
							 			 			
							entity.addPart("profile", new ByteArrayBody(ba,"imagename.jpg"));
							entity.addPart("company", new ByteArrayBody(imageBy,"imagename.jpg"));
							
							httppost.setEntity(entity);
							HttpResponse execute = httpclient.execute(httppost,localContext);
							//System.out.println("http post response");
							HttpEntity rentity     = execute.getEntity();
							is = rentity.getContent();
							
		
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						
						try{
							
							String line = "";
						     StringBuilder total = new StringBuilder();
						     // Wrap a BufferedReader around the InputStream
						     BufferedReader rd = new BufferedReader(new InputStreamReader(is, "iso-8859-1"),8);
						     // Read response until the end
						     try {
						      while ((line = rd.readLine()) != null) { 
						    	 // System.out.println("total");
						        total.append(line); 
						      }
						     } catch (IOException e) {
						      e.printStackTrace();
						     }
						     
							
							
							
							
				        	//sb.append(reader.readLine());
				        	
				        	
				        		
				        	
							
				        	is.close();
				        	 result=total.toString();
				        	
							
							
						}catch(Exception e){
							Log.e("log_tag", "Error converting result "+e.toString());
						}
						
						
						try{
							
							
							
							JSONArray jArray = new JSONArray(result);
							//result=jArray[0];
							JSONObject json_data;
							
							response=new ArrayList<String>();
							
							for(int i=0; i<jArray.length(); i++){
								
								json_data=jArray.getJSONObject(i);
								String state=json_data.getString("flag");
								if(state.equalsIgnoreCase("true")){
									
									String id=json_data.getString("user_id");
									String date=json_data.getString("date");
									response.add(state);
									response.add(id);
									response.add(date);
									

									
									
								}else{
									
									response.add(state);
								}
								
								
								
								
							}
							
							
						}catch(JSONException ep){
							
							Log.e("log_tag", "Error JAson result "+ep.toString());
							
				        	//Toast.makeText(getBaseContext(), "No Data Found", Toast.LENGTH_LONG).show();
				        	
				        }catch (ParseException e) {
				        	// TODO Auto-generated catch block
				        	
							e.printStackTrace();
							
						}
						
			}
			return response;
		}
		
		
		protected void onProgressUpdate(Integer... values)
        {
			 loadingDialog.show();
			
        }
		
		@Override
		protected void onPostExecute(ArrayList list) {
			
			bumpEnable();
			
			try {
				loadingDialog.dismiss();
				loadingDialog = null;
		    } catch (Exception e) {
		        
		    }
			
			String data=(String)list.get(0);
			if(data.equals("true")){
				
		             Log.w("SENCIDE", "TRUE");
		             //Toast.makeText(getApplication(), "You have been successfully registered", Toast.LENGTH_SHORT).show();
		             
		             /*Intent intent= new Intent(Register.this, MainActivity.class);
		             intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		             startActivity(intent);*/
		             
		             card_name=inputName.getText().toString();
		             
		             SharedPreferences checkSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
		 	         int registerCount = checkSettings.getInt("register_count", 0);
		             
		             
		             SharedPreferences shre = getSharedPreferences("Myprofile", MODE_PRIVATE); 
					 SharedPreferences.Editor prefEditor = shre.edit(); 
					 prefEditor.putInt("oneTimeCheck",1);
					 prefEditor.putInt("register_count",registerCount+1);
					 prefEditor.putString("oneTimeName",card_name);
					 prefEditor.putString("profilePic",ba1);
					 prefEditor.putString("profilePicName",filePath);
					 prefEditor.putString("profileLogo",byteLogo);
					 prefEditor.putString("profileLogoName",fileImagePath);
					 prefEditor.commit();
		             
					 
					
					 
					 
		            
		 			card_email=inputEmail.getText().toString();
		 			
		 			card_company_name=companyName.getText().toString();
		 			card_designation=designation.getText().toString();
		 			card_address=companyAddress.getText().toString();
		 			card_number=inputNumber.getText().toString();
		 			video=inputVideo.getText().toString();
		 			description=inputDescription.getText().toString();
		 			comments=inputComment.getText().toString();
		 			web_address=inputUrl.getText().toString();
		 			office=inputOffice.getText().toString();
		 			fax=inputFax.getText().toString();
		             
		            userName.setText("");
		            inputEmail.setText("");
		            inputName.setText("");
		            passWord.setText("");
		 			companyName.setText("");
		 			designation.setText("");
		 			companyAddress.setText("");
		 			inputNumber.setText("");
		 			inputVideo.setText("");
		 			inputDescription.setText("");
		 			inputComment.setText("");
					inputUrl.setText("");
					inputOffice.setText("");
					inputFax.setText("");
		 			profilePic.setImageBitmap(mIcon1);
		 			companyLogo.setImageBitmap(logoThumb);
		 			bm =null;
		 			bmLogo = null;
		 			
		 			String user_id=(String)list.get(1);
		 			String date=(String)list.get(2);
		 			
		 			
		 			ContentValues values=new ContentValues();
			 		values.put(CardTable.KEY_USERID, user_id);
			 		values.put(CardTable.KEY_NAME, card_name);
			 		values.put(CardTable.KEY_EMAIL, card_email);
			 		values.put(CardTable.KEY_NUMBER, card_number);
			 		values.put(CardTable.KEY_PROFILE_PIC, ba);
			 		values.put(CardTable.KEY_DESIGNATION, card_designation);
			 		values.put(CardTable.KEY_VIDEO, videoId);
			 		values.put(CardTable.KEY_COMPANY, card_company_name);
			 		values.put(CardTable.KEY_ADDRESS, card_address);
			 		values.put(CardTable.KEY_LOGO, imageBy);
			 		values.put(CardTable.KEY_DESCRIPTION, description);
			 		values.put(CardTable.KEY_UPDATE_DATE, date);
			 		values.put(CardTable.KEY_COMMENTS, comments);
			 		values.put(CardTable.KEY_WEB_ADDRESS, web_address);
			 		values.put(CardTable.KEY_OFFICE, office);
			 		values.put(CardTable.KEY_FAX, fax);
			 		
			 		getContentResolver().insert(CardManagingProvider.CONTENT_URI_DETAIL, values);
		             
			 		
			 		SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	                
	                SharedPreferences.Editor preEditor = loginSettings.edit();  
	                 
	                if(registerCount == 0){
	                	preEditor.putString("main_user_id", user_id);
	                }
	                preEditor.putString("user_id", user_id);
	                preEditor.putString("firstTime", "no");
	                preEditor.putString("register", "yes");
	                preEditor.putInt("registerSearch", 1);
	                preEditor.putInt("save", 0);
	                preEditor.commit();
		 			
	                
	                
	                MainActivity parent;
        	    	parent =(MainActivity) NewRegister.this.getParent();
        	    	parent.changeTab();
		             
		             
             
            }else if(data.equals("existu")){
                alertFunction("User Name already exist",0);
                //Toast.makeText(getApplication(), "User Name already exist", Toast.LENGTH_SHORT).show();   
                
	        }else if(data.equals("existe")){
	        	alertFunction("Email already exist",0);
	        	//Toast.makeText(getApplication(), "Email already exist", Toast.LENGTH_SHORT).show();  
	        
            }else{
            	alertFunction("Insertion Failed",0);
	             Log.w("SENCIDE", "FALSE");
	             //Toast.makeText(getApplication(), "Insertion Failed", Toast.LENGTH_SHORT).show();           
            }
	    	
	   
		}
		
	}
	
	
	
	private boolean checkPicName(String name){
		
		
		Pattern pattern = Pattern.compile("^[a-zA-Z0-9]+$");
		Matcher matcher = pattern.matcher(name);
		return matcher.matches();
		
	}

	
	 private void switchTabInActivity(int i) {
			// TODO Auto-generated method stub
	    	
	    	MainActivity parent;
	    	parent =(MainActivity) this.getParent();
	    	parent.switchTab(i);
	}

	public void browse(View view){

		
		
		
		switch(view.getId()){
		
			case R.id.button_logo:
				imageSelect =1;
				SharedPreferences autoSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	        	SharedPreferences.Editor prefAutoEditor = autoSettings.edit();
	        	prefAutoEditor.putInt("dntReset", 1);
	        	prefAutoEditor.commit();
				
					
					Intent i = new Intent(Intent.ACTION_PICK,
		               android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					i.putExtra("crop", "true");
					i.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri(1));
					i.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
					startActivityForResult(i, REQUEST_PICK_FILE); 
	       
		       
					break;
					
			case R.id.button_profile:
				imageSelect=1;
				
				
				SharedPreferences resetSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	        	SharedPreferences.Editor resetAutoEditor = resetSettings.edit();
	        	resetAutoEditor.putInt("dntReset", 1);
	        	resetAutoEditor.commit();
				
				Intent intnt = new Intent(Intent.ACTION_PICK,
			               android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				intnt.putExtra("crop", "true");
				intnt.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri(0));
				intnt.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
						startActivityForResult(intnt, REQUEST_PICK_IMAGE); 
					
				break;
		}   
	}
	
	private Uri getTempUri(int check) {
	    return Uri.fromFile(getTempFile(check));
	}
	
	File cropFile;
	private File getTempFile(int numb) {
	    if (isSDCARDMounted()) {

	    	
	    File bsRoot = new File(Environment.getExternalStorageDirectory(), "DigCrop");
	    if (!bsRoot.exists()) {
	    	bsRoot.mkdirs();
	    }
	    if(numb == 0){
	    	cropFile = new File(bsRoot, "profile"+System.currentTimeMillis()+"crop.jpg");
	    }else{
	    	cropFile = new File(bsRoot, "logo"+System.currentTimeMillis()+"crop.jpg");
	    }
	    
	    
	    try {
	    	cropFile.createNewFile();
	    } catch (IOException e) {

	    }
	    return cropFile;
	    } else {
	    return null;
	    }
	    }

	    private boolean isSDCARDMounted(){
	    String status = Environment.getExternalStorageState();
	    if (status.equals(Environment.MEDIA_MOUNTED))
	    return true;
	    return false;
	    }

	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
	    super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 
	    
	    switch(requestCode) { 
			  case REQUEST_PICK_FILE:
			        if(resultCode == RESULT_OK){  
			            Uri selectedLogo = Uri.fromFile(cropFile);
			           
			           String selectedLogoPath = cropFile.getAbsolutePath();
			            	
			            	if (selectedLogoPath != null) {
			                	
		                	      fileImagePath = selectedLogoPath;
		                	      
		                	      String[] tokens = fileImagePath.split("/");
		                	      
		                	      int size=tokens.length;
		                	      
		                	      fileImagePath=tokens[size-1];
		                	      
		                	      
		                	      
		                	      boolean logoName=checkPicName(fileImagePath);
		                	      if(!logoName){
		                	    	  
		                	    	  fileImagePath="imagename.jpg";
		                	  		
		                	      }
		                	
		                } else {
		                	
		                	//Toast.makeText(getApplicationContext(), "Unknown Logo path", Toast.LENGTH_LONG).show();
		               }
			            	
			            	 try {
								bmLogo =decodeUri(selectedLogo);
							} catch (FileNotFoundException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			            	companyLogo.setImageBitmap(bmLogo);
			            	
						
			        }
			        
			        break;
			    
			  case REQUEST_PICK_IMAGE:   
				  
				  
				  if(resultCode == RESULT_OK){  
			            
			            
			            Uri selectedImage = Uri.fromFile(cropFile);
			            System.out.println(selectedImage);
			            
			            String selectedImagePath = cropFile.getAbsolutePath();
			            				                
				                if (selectedImagePath != null) {
				                	
				                	      filePath = selectedImagePath;
				                	      
				                	      String[] tokens = filePath.split("/");
				                	      
				                	      int size=tokens.length;
				                	      
				                	      filePath=tokens[size-1];
				                	      
				                	      
				                	      
				                	      boolean picName=checkPicName(filePath);
				                	      if(!picName){
				                	    	  
				                	    	  filePath="imagename.jpg";
				                	    	  
				                	      }
				                	
				                } else {
				                	
				                	//Toast.makeText(getApplicationContext(), "Unknown Image path", Toast.LENGTH_LONG).show();
				               }

				            
				                
				                
								try {
									bm=decodeUri(selectedImage);
								} catch (FileNotFoundException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
								
								profilePic.setImageBitmap(bm);
								
							
			           
			           
			            
			        }
			        break;
	    }
	}

	
	

	private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException{
		
		// TODO Auto-generated method stub
		
		BitmapFactory.Options o = new BitmapFactory.Options();
		o.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);
		
		 final int REQUIRED_SIZE = 80;
		 int width_tmp = o.outWidth;
		 int height_tmp = o.outHeight;
		 
		 int scale = 1;
		 
		 while (true) {
	            if (width_tmp / 2 < REQUIRED_SIZE
	               || height_tmp / 2 < REQUIRED_SIZE) {
	                break;
	            }
	            width_tmp /= 2;
	            height_tmp /= 2;
	            scale *= 2;
	        }
		 
		 BitmapFactory.Options o2 = new BitmapFactory.Options();
		 o2.inSampleSize = scale;
		 return BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);
		 
		
	}
	
	
	@Override
	public void onBackPressed() {
		
		SharedPreferences deleteSettings = getApplicationContext().getSharedPreferences("Myprofile", MODE_PRIVATE);  
	    int bumpActive= deleteSettings.getInt("bump", 0);
	    final int auto= deleteSettings.getInt("auto", 0);
	    
	    /*if(bumpActive == 1){
	    	
	    	MainActivity parent;
	    	parent =(MainActivity) this.getParent();
	    	parent.bumpServiceCancel(bumpActive);
	    	
	    }*/

		
		
		//super.onBackPressed();
		
		AlertDialog.Builder builder = new AlertDialog.Builder(NewRegister.this);
		builder.setTitle("Exit");
		builder.setCancelable(false);
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			  
			  dialog.dismiss();
			  SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE); 
				SharedPreferences.Editor prefEditor = loginSettings.edit();  
				
				//if(auto==0){
					prefEditor.putString("user_name", null);  
				    prefEditor.putString("password", null);
				    prefEditor.putString("user_id", null);
					prefEditor.putInt("number", 3); 
					//prefEditor.putInt("bump", 0); 
					
				/*}else{
					
					prefEditor.putInt("number", 0); 
					 
					
				}*/
				
				prefEditor.commit();  
			  
			 finish();
			  
		  }});
		
		builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  dialog.dismiss();
				  
				  
					   return;
					
					
					
			        
				  
			  }});
		
		
		
		builder.setMessage("You want to exit application").create().show();
	}
	
	
	public void cancel(View view){
		switch(view.getId()){
		
		case R.id.button_profile_cancel:
			
			bm = null;
			profilePic.setImageBitmap(mIcon1);
 			
			break;
		case R.id.button_logo_cancel:
			
			bmLogo = null;
			companyLogo.setImageBitmap(logoThumb);
			break;
		}
		
		
		
		
	}
	
	public static void alertFunction(String message,int check){
		AlertDialog alert=null;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			
			  dialog.dismiss();
			  
			  
		  }});
		if(check ==1){
			builder.setTitle("Warning");
		}
		builder.setMessage(message);
		alert=builder.create();
		alert.show();
		
	}
	

}

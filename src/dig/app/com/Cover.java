package dig.app.com;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.ref.SoftReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Toast;

import com.example.coverflow.CoverFlow;

@SuppressWarnings("serial")
public class Cover extends Activity implements Serializable{
	
	
	WebView w = null; 
	FetchCardList task;
	 CoverFlow coverFlow;
	 ImageAdapter coverImageAdapter;
	 InputStream cover_is=null;
	 
	 
	 
	 ArrayList<HashMap<String, String>> myCoverList = new ArrayList<HashMap<String, String>>();
	 ArrayList<HashMap<String, String>> coverList = new ArrayList<HashMap<String, String>>();
	// ArrayList<HashMap<String, ArrayList<HashMap<String, String>>>> extraCoverList = new ArrayList<HashMap<String, ArrayList<HashMap<String, String>>>>();
	 HashMap<String, ArrayList<HashMap<String, String>>> extraCoverList = new HashMap<String, ArrayList<HashMap<String, String>>>();
	 public static ArrayList<HashMap<String, String>> coverImageList; 
	 public static ArrayList<HashMap<String, String>> coverLogoList;
	 
	 StringBuilder cover_sb=null;
	 String cover_result=null;
	 int cover_id,coverOrientation,count;
	 String cover_name,cover_designation,cover_company,cover_company_adderss,cover_phone,cover_email,cover_profile_pic,cover_logo,cover_video,cover_description, cover_update;
	 Bundle bn=null;
	 Object data=null;
	// private Drawable mDrawable;
	 int asyncCover=0;
	// private static final int COMPLETE=0;
	// private static final int FAILED=1;
	 
	@SuppressWarnings("unchecked")
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		
	      //  coverFlow = new CoverFlow(this);
	        
	       // coverFlow.setAdapter(new ImageAdapter(this, null));
	        
	        //ImageAdapter coverImageAdapter =  new ImageAdapter(this);
	        
	       // coverImageAdapter.createReflectedImages();
	        
	       // coverFlow.setAdapter(coverImageAdapter);
	        
	       
	        
	        //setContentView(coverFlow);
	        
		//Use this if you want to use XML layout file
			setContentView(R.layout.cover_flow);
			
			
			System.out.println("cover flow create ");
			w=(WebView) findViewById(R.id.web_view);
			
		    coverFlow =  (CoverFlow) findViewById(R.id.coverflow);

		
	        GradientDrawable gd = new GradientDrawable(
	                GradientDrawable.Orientation.TOP_BOTTOM,
	                new int[] {0x00000000,0x00000000});
	        gd.setCornerRadius(0f);

	       coverFlow.setBackgroundDrawable(gd);
	        //coverFlow.setBackgroundColor(#FFFFFF);
	        coverFlow.setOnItemClickListener(new OnItemClickListener() {
	           

			
				public void onItemClick(AdapterView<?> l, View v, int position, long id) {
					// TODO Auto-generated method stub
					int coverCenter=coverFlow.getdetail(v);
					
					if(coverCenter==1){
						
						
						
						//System.out.println("number  == "+number);
						
						HashMap<String, String> o = null;
						
						if(coverList !=null && coverList.size()>0){
							
							count=coverList.size();
							int number=position %count;
							System.out.println("number "+number);
							 o = coverList.get(number); 
							
						}else{
							
							int number=position %count;
							 o = myCoverList.get(number); 
						}
						
						                 
				    	 
				         String c_id = o.get("id");
				         String c_name = o.get("name");
				         String c_number = o.get("phone");
				         String c_address = o.get("company_address");
				         String c_comp_name = o.get("company");
				         String c_designation = o.get("designation");
				         String c_email = o.get("email");
				        //String c_profile_pic = o.get("pic");
				         //String c_profile_logo = o.get("logo");
				         String c_profile_video = o.get("video");
				         String c_profile_description = o.get("description");
				         String c_profile_date = o.get("updateDate");
						
				         HashMap<String, String> photo=coverImageList.get(position %count);
				         HashMap<String, String> photoLogo=coverLogoList.get(position %count);
				         
				         String c_profile_pic =photo.get("profile");
				         String c_profile_logo =photoLogo.get("logo");
				      
						Intent edit_intent = new Intent(Cover.this, BussinessCard.class);
						
						
						edit_intent.putExtra("card_id",c_id);
						edit_intent.putExtra("card_name",c_name);
						edit_intent.putExtra("card_number",c_number);
						edit_intent.putExtra("card_address",c_address);
						edit_intent.putExtra("card_company_name",c_comp_name);
						edit_intent.putExtra("card_designation",c_designation);
						edit_intent.putExtra("card_email",c_email);
						edit_intent.putExtra("card_pic",c_profile_pic);
						edit_intent.putExtra("card_logo",c_profile_logo);
						edit_intent.putExtra("card_video",c_profile_video);
						edit_intent.putExtra("card_description",c_profile_description);
						edit_intent.putExtra("card_update_date",c_profile_date);
						
						startActivity(edit_intent);
						
						
					}else{
						
						
					}
				}
	          });
		
	       
	        
	               
	        
	        SharedPreferences loginSettings = getSharedPreferences("Myprofile", MODE_PRIVATE);  
	        String userName = loginSettings.getString("user_name", null);
	        String passWord = loginSettings.getString("password", null);
	        
	        
	        if(userName !=null && passWord !=null){
				   //serverConnection();
	        	
	        	
	        	int type=getscrOrientation();
	    		coverOrientation=type;
	    		//System.out.println("orientation= "+type);
	    		
	    		
	    		if(type == 2){
	    			//loadingDialog.dismiss();
	    			if( task!=null && task.getStatus() !=AsyncTask.Status.FINISHED){
	    		    	
	    		    	task.cancel(true);
	    		   }
	    			data = getLastNonConfigurationInstance();
	    			this.finish();
	    			
	    		
	    		}else if(type ==3){
	        	//bn = new Bundle();
	           // bn = getIntent().getExtras();
	            
	            
	            	
	            	System.out.println("not empty bundle");
	            	
	            	coverList = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("dataBase");
	            	
	            	//extraCoverList = (ArrayList<HashMap<String, ArrayList<HashMap<String, String>>>>) bn.getSerializable("bundleobj");
	            	//extraCoverList = (HashMap<String, ArrayList<HashMap<String, String>>>) bn.getSerializable("bundleobj");
	            	
	            	/*coverList=(ArrayList<HashMap<String, String>>)bn.getSerializable("bundleobj");
	            	coverImageList=(ArrayList<HashMap<String, String>>)bn.getSerializable("image");
	            	coverLogoList=(ArrayList<HashMap<String, String>>)bn.getSerializable("logo");*/
	            	
	            	if(coverList !=null && coverList.size() >0){
	            		//HashMap<String, ArrayList<HashMap<String, String>>> dataBase=extraCoverList.get(0);
		            	/*
		            	coverList=extraCoverList.get("dataBase");
		            	coverImageList=extraCoverList.get("imageData");
		            	coverLogoList=extraCoverList.get("logoData");
		            	*/
		            	
		            	if(coverList !=null){
		            		
		            		
		            		int size=coverList.size();
		            		
		            		if(size >0){
		            			
		            			
		            			coverImageList = new ArrayList<HashMap<String, String>>();  
		            			coverLogoList = new ArrayList<HashMap<String, String>>();
		            			
		            			for (int i = 0; i < coverList.size(); i++) {
		            		    	
		            		    	HashMap<String, String> map=new HashMap<String, String>();
		            	             HashMap<String, String> mapLogo=new HashMap<String, String>();

		            	             map.put("profile","null");
		            	             mapLogo.put("logo","null");

		            	             coverImageList.add(map);
		            	             coverLogoList.add(mapLogo);
		            		    }
		            			
		            			coverImageAdapter =  new ImageAdapter(Cover.this,coverList);
						    	
						    	coverFlow.setAdapter(coverImageAdapter);
						    	
						    	 coverFlow.setSpacing(-40);
						          coverFlow.setSelection(size*100, true);
						          coverFlow.setAnimationDuration(1000);
		            		}else {
		            			
		            			Toast.makeText(Cover.this, "No Data Found", Toast.LENGTH_LONG).show();
		            		}
		            		
		            		
		            	}
			            
		            	
		            else{
		            	
		            	//System.out.println("empty bundle");
		            	
		            	task = new FetchCardList(this);
			    		task.execute(new String[] {"http://dev-fsit.com/iphone/fetchcard.php"});
		            }
	            		
	            	}else{
	            		
	            		task = new FetchCardList(this);
			    		task.execute(new String[] {"http://dev-fsit.com/iphone/fetchcard.php"});
	            		
	            	}
	            	
	            	
	            	
	            
	        	
				 }
	    		
	        }	
	        
	}
	
	
	
	/*
	private void serverConnection() {
		// TODO Auto-generated method stub
		int type=getscrOrientation();
		coverOrientation=type;
		//System.out.println("orientation= "+type);
		
		
		if(type == 2){
			//loadingDialog.dismiss();
			if( task!=null && task.getStatus() !=AsyncTask.Status.FINISHED){
		    	
		    	task.cancel(true);
		    }
			
			this.finish();
			
		}else if(type ==3){
						
		task = new FetchCardList(this);
		task.execute(new String[] {"http://192.168.1.187/android/cardList.php"});
		}
	}
	*/
	
	@Override
	public void onBackPressed() {
	   return;
	}
	
	
	
	public int getscrOrientation()
	{
	Display getOrient = getWindowManager().getDefaultDisplay();

	int orientation = getOrient.getOrientation();

	// Sometimes you may get undefined orientation Value is 0
	// simple logic solves the problem compare the screen
	// X,Y Co-ordinates and determine the Orientation in such cases
	//if(orientation==Configuration.ORIENTATION_UNDEFINED){

	//Configuration config = getResources().getConfiguration();
	//orientation = config.orientation;

	//if(orientation==Configuration.ORIENTATION_UNDEFINED){
	//if height and widht of screen are equal then
	// it is square orientation
	if(getOrient.getWidth()==getOrient.getHeight()){
		
		orientation = 1;
	
	}else{ //if widht is less than height than it is portrait
	if(getOrient.getWidth() < getOrient.getHeight()){
		
		orientation = 2;
	
	}else{ // if it is not any of the above it will defineitly be landscape
		
		orientation = 3;
	
	}
	}
	
	
	return orientation; 
	}
	
	
private class FetchCardList extends AsyncTask<String, Integer, ArrayList<HashMap<String, String>>>{
		
		
		
		private Context mContext;
        private ProgressDialog loadingDialog;
        private volatile boolean running = true;
        
        
        FetchCardList(Context context)
        {
            mContext = context;
            loadingDialog = new ProgressDialog(mContext);
           // loadingDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            loadingDialog.setMessage("Fetching from server. Please wait...");
            loadingDialog.setCancelable(true);
            
           // loadingDialog.setMax(100);
            
            asyncCover=1;
            
        }
		
        @Override
        protected void onPreExecute() {
            
        	if(coverOrientation == 3){
            	
            	loadingDialog.show();
            	
            }else{
            	
            	loadingDialog.dismiss();
            	loadingDialog = null;
            }
        	
        }

        

		@Override
		protected ArrayList<HashMap<String, String>> doInBackground(String... urls) {
			// TODO Auto-generated method stub
			
			
			if(running){
			
			System.out.println("do in background");
			for (String url : urls) {
				
				
				
				try {
					//System.out.println("http post");
					DefaultHttpClient httpclient = new DefaultHttpClient();
					HttpPost httppost = new HttpPost(url);
					HttpResponse execute = httpclient.execute(httppost);
					//System.out.println("http post response");
					HttpEntity entity     = execute.getEntity();
					cover_is = entity.getContent();

					

				} catch (Exception e) {
					e.printStackTrace();
				}
				
				try{
					
					 publishProgress();
					//System.out.println("http response string");
					BufferedReader reader = new BufferedReader(new InputStreamReader( cover_is, "iso-8859-1"),8); 
					cover_sb = new StringBuilder();
					cover_sb.append(reader.readLine() + "\n");
		        	String line="0";
		        	
		        	while((line =reader.readLine()) !=null){
		        		//System.out.println("string convert while function");
		        		cover_sb.append(line + "\n");
		        		
		        	}
					
		        	cover_is.close();
		        	cover_result=cover_sb.toString();
		        	
					
					
				}catch(Exception e){
					Log.e("log_tag", "Error converting result "+e.toString());
				}
				
				try{
					
					//System.out.println("json function");
					
					JSONArray jArray = new JSONArray(cover_result);
					JSONObject json_data;
					
					for(int i=0; i<jArray.length(); i++){
						
						HashMap<String, String> map = new HashMap<String, String>();
						
						json_data= jArray.getJSONObject(i);
						
						cover_id=json_data.getInt("user_id");
						String card_id=Integer.toString(cover_id);
						cover_name=json_data.getString("name");
						cover_designation=json_data.getString("designation");
						cover_company=json_data.getString("company_name");
						cover_company_adderss=json_data.getString("company_address");
						cover_phone=json_data.getString("phone");
						cover_profile_pic=json_data.getString("profile_picture");
						cover_email=json_data.getString("email");
						cover_logo=json_data.getString("company_logo");
						cover_video=json_data.getString("video");
						cover_description=json_data.getString("description");
						cover_update=json_data.getString("date_of_update");
			        	
			        	map.put("id", card_id);
			        	map.put("name", cover_name);
			        	map.put("designation", cover_designation);
			        	map.put("company", cover_company);
			        	map.put("company_address", cover_company_adderss);
			        	map.put("phone", cover_phone);
			        	map.put("email", cover_email);
			        	map.put("pic", cover_profile_pic);
			        	map.put("logo", cover_logo);
			        	map.put("video", cover_video);
			        	map.put("description", cover_description);
			        	map.put("updateDate", cover_update);

			        	
			        	myCoverList.add(map);
			        	
			        	
					} 	
					
				}catch(JSONException ep){
					
					Log.e("log_tag", "Error JAson result "+ep.toString());
					
		        	Toast.makeText(getBaseContext(), "No Data Found", Toast.LENGTH_LONG).show();
		        	
		        }catch (ParseException e) {
		        	// TODO Auto-generated catch block
		        	
					e.printStackTrace();
					
				}
			}
			
			
			}
			return myCoverList;
		}
		
		
		@Override
	    protected void onCancelled() {
	        running = false;
	    }
		
		
		
		protected void onProgressUpdate(Integer... values){
			if(coverOrientation == 3){
            	
            	loadingDialog.show();
            	
            }else{
            	
            	loadingDialog.dismiss();
            	loadingDialog = null;
            }
			
        }
		
		
		@Override
		protected void onPostExecute(ArrayList<HashMap<String, String>> list) {
			
			
			count=list.size();
			
			coverImageList = new ArrayList<HashMap<String, String>>();  
			coverLogoList = new ArrayList<HashMap<String, String>>();
			
			
			try {
				if(loadingDialog.isShowing()){
					
					loadingDialog.dismiss();
					loadingDialog = null;
				}
		    } catch (Exception e) {
		        // nothing
		    }
		    
		    
		    
		    
		    for (int i = 0; i < list.size(); i++) {
		    	
		    	HashMap<String, String> map=new HashMap<String, String>();
	             HashMap<String, String> mapLogo=new HashMap<String, String>();

	             map.put("profile","null");
	             mapLogo.put("logo","null");

	             coverImageList.add(map);
	             coverLogoList.add(mapLogo);
		    }
		    
		    /*for (int i = 0; i < list.size(); i++) {
	             
				HashMap<String, String> map = list.get(i);
	            String profileUrl=map.get("pic").toString();
	            String logoUrl=map.get("logo").toString();
	            
	            ProfileBitmapManager.PROFILEINSTANCE.profileQueueJob(profileUrl,i,list.size());
	            LogoBitmapManager.LOGOINSTANCE.logoQueueJob(logoUrl,i,list.size());
	             
	         }*/
			
			
		    
			
			//Title.setText("Fetch Card");
			
			/*byte[] imageAsBytes;
			try {
				imageAsBytes = Base64.decode(profile_pic.getBytes());
				ImageView image = (ImageView)FetchCard.this.findViewById(R.id.card_imag);
			    image.setImageBitmap(
			            BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    

			
			String[] from= new String[] {"id", "name", "designation", "company", "phone"};
	    	
	    	int[] to =new int[] {R.id.card_id, R.id.card_name, R.id.card_designation, R.id.card_company, R.id.card_phone};
	    	
	    	ListAdapter adapter=new SimpleAdapter(Fetch.this, list, R.layout.card_list, from, to);
	    	setListAdapter(adapter);*/
			
		    
		    	
		    	
		    	if(count >0){
		    		
		    		coverImageAdapter =  new ImageAdapter(Cover.this,list);
			    	
			    	coverFlow.setAdapter(coverImageAdapter);
			    	
			    	 coverFlow.setSpacing(-40);
			          coverFlow.setSelection(count*100, true);
			          coverFlow.setAnimationDuration(1000);
		    		
		    	}else{
		    		
		    		Toast.makeText(Cover.this, "No Data Found", Toast.LENGTH_LONG).show();
		    	}
		     
		    	
		    
			 
	    	
	   
		}
	}
	
	

				@Override
				public void onDestroy() {
				    super.onDestroy();
				    
				    if( task!=null && task.getStatus() !=AsyncTask.Status.FINISHED){
				    	
				    	task.cancel(true);
				    }
				    
				    
				}

	
	
	
				
				@Override
				public Object onRetainNonConfigurationInstance() {
				    //final ArrayList<HashMap<String, String>> prev_list = mylist;
					
				    //keepPhotos(list);
					
					if(data!=null && asyncCover!=1){
						
						//HashMap<String, ArrayList<HashMap<String, String>>> dataBase=extraCoverList.get(0);
		            	
						//myCoverList=dataBase.get("dataBase");
						//myCoverList=(ArrayList<HashMap<String, String>>) data;
					}
				    return myCoverList;
				}
	
	
      public class ImageAdapter extends BaseAdapter {
		
		
		ArrayList<HashMap<String, String>> cover = null;
		
		private Context iContext;
		Bitmap resizedBitmap;
		Bitmap back; 
		Picture picture;
		int  thumbNail;
		WebViewClient ViewStoryWebViewClient;
	    Canvas canvas;
	    Bitmap new_back;
		
		public ImageAdapter(Context context, ArrayList<HashMap<String, String>> list) {
			// TODO Auto-generated constructor stub
			
			
			
			this.iContext=context;
			this.cover=list;
			
			
			BitmapManager.COVERINSTANCE.setPlaceholder(BitmapFactory.decodeResource(  
	                context.getResources(), R.drawable.profile_pic));
			
			BitmapLogoManager.COVERLOGOINSTANCE.setPlaceholder(BitmapFactory.decodeResource(  
	                context.getResources(), R.drawable.logo_thumbnail));
			BitmapVideoManager.COVERVIDEOINSTANCE.setPlaceholder(BitmapFactory.decodeResource(  
	                context.getResources(), R.drawable.video_thumbnail));
		}

	
		public int getCount() {
			// TODO Auto-generated method stub
			return Integer.MAX_VALUE;
		}

		
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return  position % cover.size();
		}

		
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		
		
		/*	private Bitmap cropeFunction(String str){
				
				//System.out.println("ImageAdapter crope method");
				
				
			
				
				try {
					
					byte[] imageAsBytes;
					imageAsBytes = Base64.decode(str.getBytes());
					
				           
					
					Bitmap bitmapOrg = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
					
					int width = bitmapOrg.getWidth();
					
					int height = bitmapOrg.getHeight();
					
					int required=34;
					
					Matrix matrix = new Matrix();

					float scaleWidth = ((float) required) / width;
				    float scaleHeight = ((float) required) / height;

					
					matrix.postScale(scaleWidth, scaleHeight);
					
					
					resizedBitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, 
		                    width, height, matrix, true); 

					//return resizedBitmap;
				    				    
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				return  resizedBitmap;
    		
				
    		
    		
    		

    		
    	}
		*/
		private Bitmap createReflectedImages(Bitmap bit){
			
			
			
			
			int new_width=bit.getWidth();
			
			int new_height=bit.getHeight();
			
			
			//This will not scale but will flip on the Y axis
    		
			Matrix matrix = new Matrix();
			
			matrix.preScale(1, -1);
			
			Bitmap reflectionImage = Bitmap.createBitmap(bit, 0, new_height/2, new_width, new_height/2, matrix, false);
			
			return reflectionImage;
			
			
		}
		
		private Bitmap adjustBitmap(Bitmap val){
			
			int width=val.getWidth()+20;
			int height=val.getHeight()+25;
			
			Matrix matrix = new Matrix();

			float scaleWidth = ((float) width) /val.getWidth() ;
		    float scaleHeight = ((float) height) / val.getHeight();
		    System.out.println("scaleWidth :"+scaleWidth);
		    System.out.println("scaleHeight :"+scaleHeight);
		    
		    matrix.postScale(scaleWidth, scaleHeight);
			
			
			Bitmap adjust = Bitmap.createBitmap(val, 0, 0, val.getWidth(), val.getHeight(), matrix, false);
			
			return adjust;
			
			
		}
		
		
		
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			
			thumbNail=0;
			String l_pic,l_logo;
			String l_video=null;
			String videoId=null;
			String imageUrl=null;
			Bitmap bitmapWithReflection;
		     
			System.out.println("get view method");
			
			int exactPosition=position % cover.size();
		    //w = new WebView(Cover.this); 
			
						
			 HashMap<String, String> map = this.cover.get(position % cover.size());
			 
			 	String l_name = map.get("name").toString();
			    String l_number = map.get("phone").toString();
			    String l_company = map.get("company").toString();
			    String l_designation = map.get("designation").toString();
			   
			     l_video = map.get("video").toString();
			    
			    if(l_video !=null){
					
					try{
						thumbNail=1;
						Uri uri = Uri.parse(l_video);
						videoId=uri.getQueryParameter("v").toString();
				    	
				    	imageUrl="http://img.youtube.com/vi/"+videoId+"/2.jpg";
				    	
						
					}catch(Exception e){
						
						Log.i("profile", "No video id");
						
					}
					
					
				}
			    
			    back = BitmapFactory.decodeResource(getResources(),
                        R.drawable.cover);
			    
			    System.out.println("backwidth :"+back.getWidth());
			    System.out.println("backheight :"+back.getHeight());
			    
			     new_back=adjustBitmap(back);
			     
			     Bitmap localImageReflection=createReflectedImages(new_back);
			     int reflectionHeight=localImageReflection.getHeight()/3;
			     
			     bitmapWithReflection = Bitmap.createBitmap(new_back.getWidth(), new_back.getHeight()+reflectionHeight, Config.ARGB_8888);
					
					
					//Create a new Canvas with the bitmap that's big enough for
					
					//the image plus gap plus reflection
				
					 canvas = new Canvas(bitmapWithReflection);
					 
					 
					// w.setWebViewClient(new myWebClient());
 					// w.setPictureListener(new MyPictureListener());
 					//canvas.scale(scaling, scaling);
 				   // font = Typeface.create("Arial", Typeface.NORMAL);
 					
 					
 					canvas.drawBitmap(new_back, 0, 0, null);
 					
 					//Draw the name
 					//canvas.d
 					
 					Paint textPaint = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
 				    textPaint.setStyle(Paint.Style.FILL);
 				    textPaint.setTextAlign(Paint.Align.CENTER );
 				    //textPaint.setAntiAlias(true);
 				    textPaint.setARGB(255, 0, 0, 255);
 				    textPaint.setFakeBoldText(true);
 				    textPaint.setTextSize(10);


 					Paint name = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG); 
 					/*name.setColor(Color.BLUE); 
 					
 					canvas.drawPaint(name); */
 					name.setTextAlign(Paint.Align.CENTER );
 					name.setTextSize(10); 
 					name.setTypeface(Typeface.SERIF);
 					
 					canvas.drawText(l_name, new_back.getWidth()/2, 20, textPaint); 
 					
 					
 					Paint desig=new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
					 
					
					desig.setColor(Color.BLACK); 
					desig.setTypeface(Typeface.SERIF);
					desig.setStyle(Paint.Style.FILL);
					desig.setFakeBoldText(true);
					desig.setTextSize(7); 
					
					canvas.drawText(l_designation, new_back.getWidth()/2-15, new_back.getHeight()/4+7, desig);
					
					//Draw comp
					
					Paint comp=new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
					
					comp.setColor(Color.BLACK); 
					comp.setTypeface(Typeface.SERIF);
					comp.setStyle(Paint.Style.FILL);
					comp.setFakeBoldText(true);
					comp.setTextSize(7); 
					
					canvas.drawText(l_company, new_back.getWidth()/2-15, new_back.getHeight()/4+17, comp);
					
					
					//Draw comp
					
					Paint numb=new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
					
					numb.setColor(Color.BLACK); 
					numb.setTypeface(Typeface.SERIF);
					numb.setStyle(Paint.Style.FILL);
					numb.setFakeBoldText(true);
					numb.setTextSize(7);
					
					canvas.drawText(l_number, new_back.getWidth()/2-15, new_back.getHeight()/4+27, numb);
					
					Paint logo=new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
					
					logo.setColor(Color.BLACK); 
					logo.setTypeface(Typeface.SERIF);
					logo.setTextSize(7);
					logo.setStyle(Paint.Style.FILL);
					logo.setFakeBoldText(true);
					
					canvas.drawText("Logo", new_back.getWidth()/6+6, (new_back.getHeight()/3)*2-10, logo);
					
					if(thumbNail ==0){
  				    	 
   				    	
 						
						 Bitmap video=BitmapFactory.decodeResource(getResources(),
			                        R.drawable.video_thumbnail);
							
							
							Bitmap profile_video = Bitmap.createBitmap(video, 0, 0, video.getWidth(), video.getHeight(), null, false); 
							
							canvas.drawBitmap(profile_video, new_back.getWidth()/2+5, (new_back.getHeight()/3)*2-5, null);
					}
					
					canvas.drawBitmap(localImageReflection,0, new_back.getHeight(), null);
					
					Paint paint = new Paint();
					
					LinearGradient shader = new LinearGradient(0, new_back.getHeight(), 0, new_back.getHeight()+reflectionHeight, 0x70d2dff2, 0x00ffffff, TileMode.CLAMP);
					//Set the paint to use this shader (linear gradient)
					
					paint.setShader(shader);
					//Set the Transfer mode to be porter duff and destination in
					
					paint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
					
					//Draw a rectangle using the paint with our linear gradient
					
					canvas.drawRect(0, new_back.getHeight(), new_back.getWidth(), new_back.getHeight()+reflectionHeight, paint);
			 
			 
				 
				  l_pic = map.get("pic").toString();
				  l_logo = map.get("logo").toString();
				 
				 
				 BitmapManager.COVERINSTANCE.loadBitmap(l_pic, canvas, exactPosition,  new_back.getWidth(), new_back.getHeight());
				 BitmapLogoManager.COVERLOGOINSTANCE.logoLoadBitmap(l_logo, canvas, exactPosition, new_back.getWidth(), new_back.getHeight());
				 BitmapVideoManager.COVERVIDEOINSTANCE.videoLoadBitmap(imageUrl, canvas, new_back.getWidth(), new_back.getHeight());
			
					
					ImageView imageView = new ImageView(iContext);
    				
					imageView.setImageBitmap(bitmapWithReflection);
					
					imageView.setLayoutParams(new CoverFlow.LayoutParams(new_back.getWidth(), new_back.getHeight()));
				
					imageView.setScaleType(ScaleType.CENTER_INSIDE);
					
					
			return imageView;
		}
      }	
		
		/*
	    private class myWebClient extends WebViewClient {
	    	  
	    	  
	    	  @Override  
			    public void onPageFinished(WebView view, String url){
	    		  super.onPageFinished(view, url);

					
					 
					  picture = view.capturePicture();
					 System.out.println("picure details");
					 System.out.println("picure url :"+url);
					 System.out.println("picture width:"+picture.getWidth()+ " height :"+picture.getHeight());
					  
					// Bitmap  b = Bitmap.createBitmap( picture.getWidth(), picture.getHeight(), Bitmap.Config.ARGB_8888);

					 	//Canvas c = new Canvas( b );

						//picture.draw( c );
					  if(picture.getWidth()!=0 &&picture.getHeight()!=0){
						  thumbNail=1;
						  
						 // view.invalidate();
							// System.out.println(" has picture ");
							 canvas.drawPicture(picture, new RectF(new_back.getWidth()/2+5, (new_back.getHeight()/3)*2-5, new_back.getWidth()-15, new_back.getHeight()-15));
							 
						 }else{
							 
							// System.out.println(" has  no picture ");
							 
							 Bitmap video=BitmapFactory.decodeResource(getResources(),
				                        R.drawable.video_thumbnail);
								
								
								Bitmap profile_video = Bitmap.createBitmap(video, 0, 0, video.getWidth(), video.getHeight(), null, false); 
								
								canvas.drawBitmap(profile_video, new_back.getWidth()/2+5, (new_back.getHeight()/3)*2-5, null);
						 }
					 
					 
				 } 
	          } 
	    
		  
		
		
	}*/
      
     
      
      public enum BitmapManager { 
 		 COVERINSTANCE; 
 		 
 		 private final Map<String, SoftReference<Bitmap>> picCache;  
 	     private final ExecutorService pool;
 	     
 	      
 	     private Bitmap placeholder;
 		 
 	     BitmapManager() {  
 	    	 
 	    	picCache = new HashMap<String, SoftReference<Bitmap>>();  
 	         pool = Executors.newFixedThreadPool(5);  
 	     }
 	     public void setPlaceholder(Bitmap bmp) {  
 	            placeholder = bmp;  
 	        }
 	     public Bitmap getBitmapFromCache(String url) {  
 	            if (picCache.containsKey(url)) {  
 	                return picCache.get(url).get();  
 	            }  
 	      
 	            return null;  
 	        }
 	     
 	     public void queueJob(final String url,final Canvas canvas, final int position, final int width, final int height) {  
 	            /* Create handler in UI thread. */  
 	            final Handler handler = new Handler() {  
 	                @Override  
 	                public void handleMessage(Message msg) {  
 	                   
 	                        if (msg.obj != null) {  
 	                           
 	                        	
 	                        	ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
 		       					 ((Bitmap) msg.obj).compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutput);
 		       					 byte [] byteArray = byteArrayOutput.toByteArray();
 		       					 String imageString=Base64.encodeBytes(byteArray);    
 		       					HashMap<String, String> map=new HashMap<String, String>();
 		       					map.put("profile",imageString);
 		       					coverImageList.set(position, map);
 	                        	
 	                        	canvas.drawBitmap((Bitmap) msg.obj, width/8, height/4-5, null);
 	                        } else {  
 	                           
 	                        	canvas.drawBitmap(placeholder, width/8, height/4-5, null);
 	                           
 	                        }  
 	                  
 	                }  
 	            };
 	            
 	            pool.submit(new Runnable() {  
 	                
 	                public void run() {  
 	                    final Bitmap bmp = downloadBitmap(url, 34, 34);  
 	                    Message message = Message.obtain();  
 	                    message.obj = bmp;  
 	                   // Log.d(null, "Item downloaded: " + url);  
 	      
 	                    handler.sendMessage(message);  
 	                }  
 	            });  
 	        } 
 	     
 	     public void loadBitmap(final String url, final Canvas canvas, final int position, final int width, final int height) {  
 	            //imageViews.put(imageView, url);  
 	            Bitmap bitmap = getBitmapFromCache(url);  
 	      
 	            // check in UI thread, so no concurrency issues  
 	            if (bitmap != null) {  
 	               // Log.d(null, "Item loaded from cache: " + url);  
 	               canvas.drawBitmap(bitmap, width/8, height/4-5, null);
 	            } else {  
 	               
 	            	canvas.drawBitmap(placeholder, width/8, height/4-5, null);
 	                queueJob(url,canvas, position, width, height);  
 	            }  
 	        } 
 	     
 	     private Bitmap downloadBitmap(String url, int width, int height) {  
 	            try {  
 	            	
 	                Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());  
 	                
 	               /*  ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
 					 bitmap.compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutput);
 					 byte [] byteArray = byteArrayOutput.toByteArray();
 					 String imageString=Base64.encodeBytes(byteArray);       
 	                 imageList.add(imageString);
 	                
 	                */
 	                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);  
 	               picCache.put(url, new SoftReference<Bitmap>(bitmap));  
 	                return bitmap;  
 	                
 	            } catch (MalformedURLException e) {  
 	                e.printStackTrace();  
 	            } catch (IOException e) {  
 	                e.printStackTrace();  
 	            }  
 	      
 	            	return null;  
 	        }
 	     
 	     
 	 } 
      
      
      public enum BitmapLogoManager { 
  		 COVERLOGOINSTANCE; 
  		 
  		 private final Map<String, SoftReference<Bitmap>> logoCache;  
  	     private final ExecutorService logoPool;
  	     
  	      
  	     private Bitmap placeholder;
  		 
  	   BitmapLogoManager() {  
  	    	 
  		 logoCache = new HashMap<String, SoftReference<Bitmap>>();  
  	    	logoPool = Executors.newFixedThreadPool(5);  
  	     }
  	     public void setPlaceholder(Bitmap bmp) {  
  	            placeholder = bmp;  
  	        }
  	     public Bitmap getLogoBitmapFromCache(String url) {  
  	            if (logoCache.containsKey(url)) {  
  	                return logoCache.get(url).get();  
  	            }  
  	      
  	            return null;  
  	        }
  	     
  	     public void queueLogoJob(final String url,final Canvas canvas, final int position, final int width, final int height) {  
  	            /* Create handler in UI thread. */  
  	            final Handler handler = new Handler() {  
  	                @Override  
  	                public void handleMessage(Message msg) {  
  	                   
  	                        if (msg.obj != null) {  
  	                           
  	                        	
  	                        	
  	                        	ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
		       					 ((Bitmap) msg.obj).compress(Bitmap.CompressFormat.JPEG, 90, byteArrayOutput);
		       					 byte [] byteArray = byteArrayOutput.toByteArray();
		       					 String imageString=Base64.encodeBytes(byteArray);
		       					 
		       					HashMap<String, String> map=new HashMap<String, String>();
		       					map.put("logo",imageString);
		       					coverLogoList.set(position, map);
  	                        	
  	                        	canvas.drawBitmap((Bitmap) msg.obj, width/7, (height/3)*2-5, null);
  	                        } else {  
  	                           
  	                        	canvas.drawBitmap(placeholder, width/7, (height/3)*2-5, null);
  	                           
  	                        }  
  	                  
  	                }  
  	            };
  	            
  	          logoPool.submit(new Runnable() {  
  	                  
  	                public void run() {  
  	                    final Bitmap bmp = downloadLogoBitmap(url, 34, 34);  
  	                    Message message = Message.obtain();  
  	                    message.obj = bmp;  
  	                   // Log.d(null, "Item downloaded: " + url);  
  	      
  	                    handler.sendMessage(message);  
  	                }  
  	            });  
  	        } 
  	     
  	     public void logoLoadBitmap(final String url, final Canvas canvas, final int position, final int width, final int height) {  
  	            //imageViews.put(imageView, url);  
  	            Bitmap bitmap = getLogoBitmapFromCache(url);  
  	      
  	            // check in UI thread, so no concurrency issues  
  	            if (bitmap != null) {  
  	               
  	             canvas.drawBitmap(bitmap, width/7, (height/3)*2-5, null);
  	             
  	            } else {  
  	            	
  	            	canvas.drawBitmap(placeholder, width/7, (height/3)*2-5, null);
  	            	queueLogoJob(url,canvas,position, width, height);  
  	            }  
  	        } 
  	     
  	     private Bitmap downloadLogoBitmap(String url, int width, int height) {  
  	            try {  
  	            	
  	                Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(  
  	                        url).getContent());  
  	                
  	              
  	                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);  
  	              logoCache.put(url, new SoftReference<Bitmap>(bitmap));  
  	                return bitmap;  
  	                
  	            } catch (MalformedURLException e) {  
  	                e.printStackTrace();  
  	            } catch (IOException e) {  
  	                e.printStackTrace();  
  	            }  
  	      
  	            	return null;  
  	        }
  	     
  	     
  	 } 
      
      
      public enum BitmapVideoManager { 
   		 COVERVIDEOINSTANCE; 
   		 
   		 private final Map<String, SoftReference<Bitmap>> videoCache;  
   	     private final ExecutorService videoPool;
   	     
   	  private Bitmap placeholder;
   	  
   	  
   	BitmapVideoManager() {  
	    	 
   		videoCache = new HashMap<String, SoftReference<Bitmap>>();  
   		videoPool = Executors.newFixedThreadPool(5);  
    }
   	
   	
   	public void setPlaceholder(Bitmap bmp) {  
          placeholder = bmp;  
      }
   public Bitmap getVideoBitmapFromCache(String url) {  
          if (videoCache.containsKey(url)) {  
              return videoCache.get(url).get();  
          }  
    
          return null;  
      }
   
   public void queueVideoJob(final String url,final Canvas canvas, final int width, final int height) {  
         /* Create handler in UI thread. */  
         final Handler handler = new Handler() {  
             @Override  
             public void handleMessage(Message msg) {  
                
                     if (msg.obj != null) {  
                                           	
                     	canvas.drawBitmap((Bitmap) msg.obj, width/2, (height/3)*2-5, null);
                     } else {  
                           
                    	 canvas.drawBitmap(placeholder, width/2, (height/3)*2-5, null);
                     }  
               
             }  
         };
         
       videoPool.submit(new Runnable() {  
             
             public void run() {  
                 final Bitmap bmp = downloadVideoBitmap(url, 34, 34);  
                 Message message = Message.obtain();  
                 message.obj = bmp;  
                  
   
                 handler.sendMessage(message);  
             }  
         });  
     } 
   	    
   
   public void videoLoadBitmap(final String url, final Canvas canvas, final int width, final int height) {  
         
         Bitmap bitmap = getVideoBitmapFromCache(url);  
   
         // check in UI thread, so no concurrency issues  
         if (bitmap != null) {  
            
        	 canvas.drawBitmap(bitmap, width/2, (height/3)*2-5, null);
         } else {  
           
        	//canvas.drawBitmap(placeholder, width/2+5, (height/3)*2, null);
         	queueVideoJob(url,canvas, width, height);  
         }  
     } 
   
   
   private Bitmap downloadVideoBitmap(String url, int width, int height) {  
         try {  
         	
             Bitmap bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());  
             
            
             bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);  
           videoCache.put(url, new SoftReference<Bitmap>(bitmap));  
             return bitmap;  
             
         } catch (MalformedURLException e) {  
             e.printStackTrace();  
         } catch (IOException e) {  
             e.printStackTrace();  
         }  
   
         	return null;  
     }
   	      
}
   	     
   	     
   	  
    
    	  
      

}

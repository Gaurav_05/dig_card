package dig.app.com;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CardDataBase extends SQLiteOpenHelper{
	
	private static final String DATABASE_NAME = "dig_card";
	private static final int DATABASE_VERSION=3;

	public CardDataBase(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		// TODO Auto-generated method stub
		CardTable.onCreate(database);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		CardTable.onUpgrade(database, oldVersion, newVersion);
	}

}

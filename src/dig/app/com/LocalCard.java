package dig.app.com;


import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class LocalCard extends Activity{

	Uri cardUri;
	Button cardDelete;
	TextView CardName,CardNumber,CardDesignation,CardCompanyName,CardCompanyAddress,CardEmail, cardDescription, cardWeburl, cardComments,cardOffice,cardFax;
	byte[] profile_pic,profile_logo;// profile_video
	ImageView cardVideo;
	Drawable mDrawable;
	private static final int COMPLETE=0;
	private static final int FAILED=1;
	int deviceType=0;
	String profile_video=null;
	Bitmap mBitmap,bVideo;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.local_card_detail);
		
		MyAppData appState = ((MyAppData)getApplication());
		deviceType=appState.getTablet();
		/*if(deviceType==1){
			
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			
		}else{
			
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			
		}*/
		
		
		CardName=(TextView) findViewById(R.id.localCard_name);
		CardNumber=(TextView) findViewById(R.id.localCard_number);
		CardDesignation=(TextView) findViewById(R.id.localCard_designation);
		CardCompanyName=(TextView) findViewById(R.id.localCard_company);
		CardCompanyAddress=(TextView) findViewById(R.id.localCard_address);
		CardEmail=(TextView) findViewById(R.id.localCard_email);
		cardVideo=(ImageView) findViewById(R.id.localCard_company_video);
		cardDescription=(TextView) findViewById(R.id.localCard_description);
		cardWeburl=(TextView) findViewById(R.id.localCard_weburl);
		cardComments=(TextView) findViewById(R.id.localCard_comments);
		cardOffice=(TextView) findViewById(R.id.localCard_office);
		cardFax=(TextView) findViewById(R.id.localCard_fax);
		Bundle extras = getIntent().getExtras();
		
		
		CardName.setSelected(true);
		CardDesignation.setSelected(true);
		CardCompanyName.setSelected(true);
		CardEmail.setSelected(true);
		
		bVideo=BitmapFactory.decodeResource(this.getResources(), R.drawable.video_thumbnail_big2);
		
		if (extras != null) {
			cardUri = extras
					.getParcelable(CardManagingProvider.CONTENT_ITEM_TYPE);

			fillData(cardUri);
		}
		
		cardDelete=(Button) findViewById(R.id.card_delete);
		
		cardDelete.setOnClickListener(new View.OnClickListener() {
			
		
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				delete_alert();
			}
		});
		
		
		CardNumber.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String numberString=CardNumber.getText().toString();
				
				if (!numberString.equals("")) {
					  Uri number = Uri.parse("tel:" + numberString);
					  Intent dial = new Intent(Intent.ACTION_DIAL, number);
					  
					  startActivity(dial);
					 }
				
			}
		});
		
		cardOffice.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String numberString=cardOffice.getText().toString();
				
				if (!numberString.equals("")) {
					  Uri number = Uri.parse("tel:" + numberString);
					  Intent dial = new Intent(Intent.ACTION_DIAL, number);
					  
					  startActivity(dial);
					 }
				
			}
		});
		
		CardEmail.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				
				String recipient=CardEmail.getText().toString();
				
				//Toast.makeText(BussinessCard.this, "mail to "+recipient, Toast.LENGTH_SHORT).show();
				if(recipient!=null){
					
					String aEmailList[] = { recipient };  
					Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);  
					emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, aEmailList);
					emailIntent.setType("plain/text");
					//startActivity(Intent.createChooser(emailIntent, "Send your email in:"));
					final PackageManager pm = getPackageManager(); 
	        		final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, PackageManager.MATCH_DEFAULT_ONLY);
	        		/*ResolveInfo best = null;
	        		for (final ResolveInfo info : matches){
	                        if (info.activityInfo.name.toLowerCase().contains("mail"))
	                                best = info;
	                        if (best != null) {
	                        	emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name); 
	                            startActivity(emailIntent);
	                        }
	                }*/
	        		
	        		if(matches.size()>0){
	        			
	        			startActivity(Intent.createChooser(emailIntent, "Send your email in:"));
	        		}
	        		
				}
							
				
			}
		});
		
		cardWeburl.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				String url=cardWeburl.getText().toString();
				
				if(url!=null){
					
					if (!url.startsWith("https://") && !url.startsWith("http://")){
					    url = "http://" + url;
					}
					Intent openUrlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					startActivity(openUrlIntent);
					
				}
				
			}
		});
		
		
		cardVideo.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(profile_video.length() >0){
					
					playVideo(profile_video);
					
				}else{
					
					Toast.makeText(LocalCard.this, "You have not specified any Video Link", Toast.LENGTH_LONG).show();
				}
			}
		});
		
	}
	
	
	
	public void onResume(){
    	super.onResume();
    	
    	//com.facebook.Settings.publishInstallAsync(this, "659621604053010");
    }
	
	public void playVideo(String video){
    	
    	
			String video_path = video;
			//Uri uri = Uri.parse(video_path);

    	
    	//uri = Uri.parse("vnd.youtube:"  + uri.getQueryParameter("v"));
			try{
				
				Uri uri=Uri.parse("vnd.youtube:"  + video_path);
				
		    	Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		    	startActivity(intent);

			}catch(Exception e){
				
				
			}
    	
    }

	private void fillData(Uri uri) {
		// TODO Auto-generated method stub
		
		String[] projection = { CardTable.KEY_USERID,
				CardTable.KEY_NAME, CardTable.KEY_EMAIL, CardTable.KEY_DESIGNATION, CardTable.KEY_COMPANY, CardTable.KEY_ADDRESS, CardTable.KEY_NUMBER ,CardTable.KEY_PROFILE_PIC, CardTable.KEY_LOGO, CardTable.KEY_DESCRIPTION,CardTable.KEY_WEB_ADDRESS,CardTable.KEY_COMMENTS,CardTable.KEY_OFFICE,CardTable.KEY_FAX,CardTable.KEY_VIDEO};
		Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
		
		if (cursor != null) {
			if(cursor.moveToFirst()){
			

			CardName.setText(cursor.getString(cursor
					.getColumnIndexOrThrow(CardTable.KEY_NAME)));
			CardNumber.setText(cursor.getString(cursor
					.getColumnIndexOrThrow(CardTable.KEY_NUMBER)));
			CardDesignation.setText(cursor.getString(cursor
					.getColumnIndexOrThrow(CardTable.KEY_DESIGNATION)));
			CardCompanyName.setText(cursor.getString(cursor
					.getColumnIndexOrThrow(CardTable.KEY_COMPANY)));
			CardCompanyAddress.setText(cursor.getString(cursor
					.getColumnIndexOrThrow(CardTable.KEY_ADDRESS)));
			CardEmail.setText(cursor.getString(cursor
					.getColumnIndexOrThrow(CardTable.KEY_EMAIL)));
			
			profile_pic=cursor.getBlob(cursor
					.getColumnIndexOrThrow(CardTable.KEY_PROFILE_PIC));
			profile_logo=cursor.getBlob(cursor
					.getColumnIndexOrThrow(CardTable.KEY_LOGO));
			
			profile_video=cursor.getString(cursor
					.getColumnIndexOrThrow(CardTable.KEY_VIDEO));
			
			cardDescription.setText(cursor.getString(cursor
					.getColumnIndexOrThrow(CardTable.KEY_DESCRIPTION)));
			
			cardWeburl.setText(cursor.getString(cursor
					.getColumnIndexOrThrow(CardTable.KEY_WEB_ADDRESS)));
			
			cardComments.setText(cursor.getString(cursor
					.getColumnIndexOrThrow(CardTable.KEY_COMMENTS)));
			
			cardOffice.setText(cursor.getString(cursor
					.getColumnIndexOrThrow(CardTable.KEY_OFFICE)));
			
			cardFax.setText(cursor.getString(cursor
					.getColumnIndexOrThrow(CardTable.KEY_FAX)));
			
			if(haveNetworkConnection()){
				
				showThumbnail(profile_video);
				
			}else{
				
				cardVideo.setImageBitmap(bVideo);
				
			}
			
			
			
			//CardNumber.setText(cursor.getString(cursor
					//.getColumnIndexOrThrow(CardTable.KEY_NUMBER)));

			byte[] imageAsBytes,logoAsBytes;
			//imageAsBytes = Base64.decode(profile_pic.getBytes());
			imageAsBytes = profile_pic;
			ImageView image = (ImageView)this.findViewById(R.id.localCard_pic);
			Bitmap profileMap = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
			
			
			if(profileMap.getWidth()>100 ||profileMap.getHeight()>100){
				image.getLayoutParams().height = 100;
				image.getLayoutParams().width = 100;
			}
			image.setImageBitmap(profileMap);
			
			//logoAsBytes = Base64.decode(profile_logo.getBytes());
			logoAsBytes = profile_logo;
			ImageView logo = (ImageView)this.findViewById(R.id.localCard_company_logo);
			Bitmap logoMap = BitmapFactory.decodeByteArray(logoAsBytes, 0, logoAsBytes.length);
			
			if(logoMap.getWidth()>100 ||logoMap.getHeight()>100){
				logo.getLayoutParams().height = 100;
				logo.getLayoutParams().width = 100;
			}
			logo.setImageBitmap(logoMap);
			
		}
			cursor.close();
		}
		
	}
	
	
	public void showThumbnail(String video){
    	   	   	
    	
    	String thumbNail=video;
		String videoId=null;
		String imageUrl=null;
		
		if(thumbNail !=null&&thumbNail.length()>0){
			
			try{
				//Uri uri = Uri.parse(thumbNail);
				//videoId=uri.getQueryParameter("v").toString();
		    	
		    	//imageUrl="http://img.youtube.com/vi/"+videoId+"/2.jpg";
				imageUrl="http://img.youtube.com/vi/"+thumbNail+"/2.jpg";
		    	
				
			}catch(Exception e){
				
				Log.i("profile", "No video id");
				
			}
			
			
		}
		
    	
    	if(thumbNail !=null&&thumbNail.length()>0){
    		
    		
    		setImageBitmap(imageUrl);
    		
    	}else{
    		
    		cardVideo.setImageBitmap(bVideo);
    		
    	}
    	
    }
	
	
	
/*public void setImageDrawable(final String url){
    	
    	//System.out.println("in set Drawable");
    	
    	mDrawable=null;
    	
    	new Thread(){
    		public void run(){
    			try{
    				
    				//System.out.println("new Thread");
    				mDrawable=getDrawableFromUrl(url);
    				imageLoaderHandler.sendEmptyMessage(COMPLETE);
    			}catch(MalformedURLException e){
    				
    				//System.out.println("MalformedURLException ");
    				
    				imageLoaderHandler.sendEmptyMessage(FAILED);
    				
    			}catch(IOException e){
    				
    				//System.out.println("IOException ");
    				
    				imageLoaderHandler.sendEmptyMessage(FAILED);
    				
    			}
    			
    		};

			
    		
    	}.start();
    	
    }*/


public void setImageBitmap(final String url){
	
	
	
	mBitmap=null;
	
	new Thread(){
		public void run(){
			try{
				
				
				mBitmap=getBitmapFromUrl(url);
				imageLoaderHandler.sendEmptyMessage(COMPLETE);
				
			}catch(MalformedURLException e){
				
				
				
				imageLoaderHandler.sendEmptyMessage(FAILED);
				
			}catch(IOException e){
				
				
				
				imageLoaderHandler.sendEmptyMessage(FAILED);
				
			}
			
		};

		
		
	}.start();
	
}




private final Handler imageLoaderHandler = new Handler(new Callback(){
	
	
	
	public boolean handleMessage(Message msg) {
		// TODO Auto-generated method stub
		
		//System.out.println("message handler");
		switch(msg.what){
    	
    	
    	case COMPLETE:
    		
    		//System.out.println("message complete");
    		
    		if(mBitmap!=null){
    			
    			cardVideo.setImageBitmap(mBitmap);
    		}
    		
    		break;
    	case FAILED:
    		
    			cardVideo.setImageBitmap(bVideo);
    		
    		break;
    		
    		default:
    			//System.out.println("message default");
    			break;
    		
    		
    	}
		return true;
    
	}
	
});
//checking for Internet connection
private boolean haveNetworkConnection() {
    boolean haveConnectedWifi = false;
    boolean haveConnectedMobile = false;

    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo[] netInfo = cm.getAllNetworkInfo();
    for (NetworkInfo ni : netInfo) {
        if (ni.getTypeName().equalsIgnoreCase("WIFI"))
            if (ni.isConnected())
                haveConnectedWifi = true;
        if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
            if (ni.isConnected())
                haveConnectedMobile = true;
    }
    return haveConnectedWifi || haveConnectedMobile;
}



 /* private static Drawable getDrawableFromUrl(final String url) throws IOException,MalformedURLException{
	// TODO Auto-generated method stub
	  
	 // System.out.println("image from url");
	return Drawable.createFromStream(((java.io.InputStream)new java.net.URL(url).getContent()), "name");
}*/
	
	
  
  private Bitmap getBitmapFromUrl(String url) throws IOException,MalformedURLException{
	  
	  
	  try { 
		  
			BitmapFactory.Options optnsSizeOnly = new BitmapFactory.Options();
	      	optnsSizeOnly.inJustDecodeBounds = true;
	      	BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, optnsSizeOnly);
	      	
	      	
	
	      	final int REQUIRED_SIZE = 80;
	      			 int width_tmp = optnsSizeOnly.outWidth;
	      			 int height_tmp = optnsSizeOnly.outHeight;
	      			 
	      			 int scale = 1;
	      			 
	      			 while (true) {
	      		            if (width_tmp / 2 < REQUIRED_SIZE
	      		               || height_tmp / 2 < REQUIRED_SIZE) {
	      		                break;
	      		            }
	      		            width_tmp /= 2;
	      		            height_tmp /= 2;
	      		            scale *= 2;
	      		        }
	      			 
	      			 BitmapFactory.Options o2 = new BitmapFactory.Options();
	      			 o2.inSampleSize = scale;
	      			Bitmap logoBitmap= BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, o2); 
	      	
	      	return logoBitmap;
	      	
	      	
	  } catch (MalformedURLException e) {  
		  
          e.printStackTrace();  
      } catch (IOException e) {  
    	  
    	  
    	  
          e.printStackTrace();  
      }  
      
      return null;  
	  
  }
  
	
	private void delete_alert(){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(LocalCard.this);
		builder.setCancelable(false);
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		
				  public void onClick(DialogInterface arg0, int arg1) {
				  // do something when the OK button is clicked
					  
					  delete();
				        
					  
				  }});
				
				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					  public void onClick(DialogInterface dialog, int arg1) {
					  // do something when the OK button is clicked
						  
						  dialog.dismiss();
					        
						  
					  }});
				
				//builder.show();
				builder.setMessage("Want to Delete this Card?").create().show();
		
	}

	protected void delete() {
		// TODO Auto-generated method stub
		
		getContentResolver().delete(cardUri, null, null);
		//System.out.println(cardUri);
		//Toast.makeText(LocalCard.this, "Card has been Deleted", Toast.LENGTH_SHORT).show();
		alertFunction("Card Deleted");
		
	}
	
	public void alertFunction(String message){
		AlertDialog alert=null;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(LocalCard.this);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			
			  dialog.dismiss();
			  ((Activity) LocalCard.this).finish();
			  
		  }});
		//builder.setTitle("Warning");
		builder.setMessage(message);
		alert=builder.create();
		alert.show();
		
	}
	
}

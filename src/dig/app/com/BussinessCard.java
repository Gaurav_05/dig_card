package dig.app.com;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class BussinessCard extends Activity{
	
	String card_id,card_name,card_number,card_address,card_company_name,card_designation, card_email,card_pic, card_logo,card_video, card_description, card_date,pic_url,card_weburl,card_comments,card_office,card_fax;
	TextView BussinessName,BussinessNumber,Designation,CompanyName,CompanyAddress,BussinessEmail, companyDescription,companyUrl, companyComment,companyFax,companyOffice ;
	ImageView BussinessPic,videoThumbnail,BussinessLogo;
	Button downloadCard;
	private Bitmap mBitmap,picBitmap,logoBitmap,logoThumb;
	private static final int COMPLETE=0;
	private static final int FAILED=1;
	private static final int PICCOMPLETE=2;
	private static final int PICFAILED=3;
	private static final int LOGOCOMPLETE=4;
	private static final int LOGOFAILED=5;
	Bitmap bLogo=null,bPic=null,bVideo=null;
	
	int deviceType=0;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detail);
		
		MyAppData appState = ((MyAppData)getApplication());
		deviceType=appState.getTablet();
		/*if(deviceType==1){
			
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			
		}else{
			
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			
		}*/
		
		 
		BussinessLogo=(ImageView) findViewById(R.id.detail_company_logo);
		BussinessPic=(ImageView) findViewById(R.id.detail_pic);
		videoThumbnail=(ImageView) findViewById(R.id.detail_video);
		BussinessName=(TextView) findViewById(R.id.detail_name);
		BussinessNumber=(TextView) findViewById(R.id.detail_number);
		Designation=(TextView) findViewById(R.id.detail_designation);
		CompanyName=(TextView) findViewById(R.id.detail_company);
		CompanyAddress=(TextView) findViewById(R.id.detail_address);
		BussinessEmail=(TextView) findViewById(R.id.detail_email);
		companyDescription=(TextView) findViewById(R.id.detail_description);
		downloadCard=(Button) findViewById(R.id.card_download);
		companyComment=(TextView) findViewById(R.id.detail_comments);
		companyUrl=(TextView) findViewById(R.id.detail_url);
		companyOffice=(TextView) findViewById(R.id.detail_office);
		companyFax=(TextView) findViewById(R.id.detail_fax);
		
		BussinessName.setSelected(true);
		Designation.setSelected(true);
		CompanyName.setSelected(true);
		BussinessEmail.setSelected(true);
		
		bPic=BitmapFactory.decodeResource(this.getResources(), R.drawable.profile_pic);
		bLogo=BitmapFactory.decodeResource(this.getResources(), R.drawable.logo);
		bVideo=BitmapFactory.decodeResource(this.getResources(), R.drawable.video_thumbnail_big2);
		logoThumb=BitmapFactory.decodeResource(this.getResources(), R.drawable.logo_thumbnail);
		
		Bundle recdData = getIntent().getExtras();
		 card_id=recdData.getString("card_id");
		 card_name=recdData.getString("card_name");
		 card_number=recdData.getString("card_number");
		 card_address=recdData.getString("card_address");
		 card_company_name=recdData.getString("card_company_name");
		 card_designation=recdData.getString("card_designation");
		 card_email=recdData.getString("card_email");
		 card_pic=recdData.getString("card_pic");
		 card_logo=recdData.getString("card_logo");
		 card_video=recdData.getString("card_video");
		 card_description=recdData.getString("card_description");
		 card_date=recdData.getString("card_update_date");
		 pic_url=recdData.getString("card_pic_url");
		 card_weburl=recdData.getString("card_web_url");
		 card_comments=recdData.getString("card_comments");
		 card_office=recdData.getString("card_office");
		 card_fax=recdData.getString("card_fax");
		 
		
		
		filldata();
		
		
		BussinessNumber.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String numberString=BussinessNumber.getText().toString();
				
				if (!numberString.equals("")) {
					  Uri number = Uri.parse("tel:" + numberString);
					  Intent dial = new Intent(Intent.ACTION_DIAL, number);
					  
					  startActivity(dial);
					 }
				
			}
		});
		
		companyOffice.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				String numberString=companyOffice.getText().toString();
				
				if (!numberString.equals("")) {
					  Uri number = Uri.parse("tel:" + numberString);
					  Intent dial = new Intent(Intent.ACTION_DIAL, number);
					  
					  startActivity(dial);
					 }
				
			}
		});
		
		
		BussinessEmail.setOnClickListener(new View.OnClickListener() {
			
		
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				
				String recipient=BussinessEmail.getText().toString();
				
				//Toast.makeText(BussinessCard.this, "mail to "+recipient, Toast.LENGTH_SHORT).show();
				if(recipient!=null){
					
					String aEmailList[] = { recipient };  
					Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);  
					emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, aEmailList);
					emailIntent.setType("plain/text");
					//startActivity(Intent.createChooser(emailIntent, "Send your email in:"));
					final PackageManager pm = getPackageManager(); 
	        		final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, PackageManager.MATCH_DEFAULT_ONLY);
	        		/*ResolveInfo best = null;
	        		for (final ResolveInfo info : matches){
	                        if (info.activityInfo.name.toLowerCase().contains("mail"))
	                                best = info;
	                        if (best != null) {
	                        	emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name); 
	                            startActivity(emailIntent);
	                        }
	                }*/
	        		
	        		if(matches.size()>0){
	        			
	        			startActivity(Intent.createChooser(emailIntent, "Send your email in:"));
	        		}
				}
				
				
				

				
			}
		});
		
		companyUrl.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				
				
				String url=companyUrl.getText().toString();
				
				
				if(url!=null){
					
					if (!url.startsWith("https://") && !url.startsWith("http://")){
					    url = "http://" + url;
					}
					Intent openUrlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					startActivity(openUrlIntent);
					
				}
				
			}
		});
		
		
		downloadCard.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(haveNetworkConnection()){
					alertShow();
				}else{
					alertFunction("No Internet Connection",0);
				}
				
				
			}

			
		});
		
		videoThumbnail.setOnClickListener(new View.OnClickListener() {
			
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(card_video.length() >0){
					
					playVideo(card_video);
					
				}else{
					
					Toast.makeText(BussinessCard.this, "You have not specified any Video Link", Toast.LENGTH_LONG).show();
				}
				
				
			}
		});
		
	}
	
	public void onResume(){
    	super.onResume();
    	
    	//com.facebook.Settings.publishInstallAsync(this, "659621604053010");
    }
	
	
	
	// checking for Internet connection
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
	
	
		public void playVideo(String video){
    	    				    	
    	String video_path = video;
		
		try{
			
			
			//Uri uri = Uri.parse(video_path);

	    	// With this line the Youtube application, if installed, will launch immediately.
	    	// Without it you will be prompted with a list of the application to choose.
	    	
	    	//uri = Uri.parse("vnd.youtube:"  + uri.getQueryParameter("v"));
			
			//Uri uri=Uri.parse(video_path);
	    	Uri uri=Uri.parse("vnd.youtube:"  + video_path);
	    	
	    	Intent intent = new Intent(Intent.ACTION_VIEW, uri);
	    	startActivity(intent);
			
		}catch(Exception e){
			
			
		}
    }
	
	private void alertShow(){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(BussinessCard.this);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface arg0, int arg1) {
		  // do something when the OK button is clicked
			  
			  download();
		        
			  
		  }});
		
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			
			  public void onClick(DialogInterface dialog, int arg1) {
			  // do something when the OK button is clicked
				  
				  dialog.dismiss();
			        
				  
			  }});
		
		builder.setTitle("Download");
		builder.setMessage("Do you want to Download this Business Card ?").create().show();
	}

	private void filldata() {
		// TODO Auto-generated method stub
		
		
		Bundle recdData = getIntent().getExtras();
		 card_id=recdData.getString("card_id");
		 card_name=recdData.getString("card_name");
		 card_number=recdData.getString("card_number");
		 card_address=recdData.getString("card_address");
		 card_company_name=recdData.getString("card_company_name");
		 card_designation=recdData.getString("card_designation");
		 card_email=recdData.getString("card_email");
		 card_pic=recdData.getString("card_pic");
		 card_logo=recdData.getString("card_logo");
		 card_video=recdData.getString("card_video");
		 card_description=recdData.getString("card_description");
		 pic_url=recdData.getString("card_pic_url");
		 card_weburl=recdData.getString("card_web_url");
		 card_comments=recdData.getString("card_comments");
		 card_office=recdData.getString("card_office");
		 card_fax=recdData.getString("card_fax");
		 
		
		 /*System.out.println(card_name);
		System.out.println(card_number);
		System.out.println(card_address);
		System.out.println(card_company_name);
		System.out.println(card_designation);
		System.out.println(card_email);
		System.out.println(card_video);
		System.out.println(card_description);
		*/
		
		
		if(card_pic !=null){
			
			
			try {
				
				byte[] imageAsBytes;
				imageAsBytes = Base64.decode(card_pic.getBytes());
				ImageView image = (ImageView)this.findViewById(R.id.detail_pic);
			    image.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
			    
			    
			    
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			
			BussinessPic.setImageBitmap(bPic);
		}
		
		/*if(card_logo!=null){
			
			
			
			try {
				byte[] logoAsBytes;
				
				logoAsBytes = Base64.decode(card_logo.getBytes());
				ImageView logo = (ImageView)this.findViewById(R.id.detail_company_logo);
			    logo.setImageBitmap(
			            BitmapFactory.decodeByteArray(logoAsBytes, 0, logoAsBytes.length));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
			
		}else{
			
			BussinessLogo.setImageBitmap(bLogo);
			
		}*/
	    
	    setPictureBitmap(pic_url);
	   	
	    setLogoBitmap(card_logo);
		
	    
	    if(haveNetworkConnection()){
	    	
	    	 showThumbnail(card_video);
	    	
	    }else {
	    	
	    	videoThumbnail.setImageBitmap(bVideo);
	    	
	    }
	   
	    
		BussinessName.setText(card_name);
		BussinessNumber.setText(card_number);
		Designation.setText(card_designation);
		CompanyName.setText(card_company_name);
		CompanyAddress.setText(card_address);
		BussinessEmail.setText(card_email);
		companyDescription.setText(card_description);
		companyUrl.setText(card_weburl);
		companyComment.setText(card_comments);
		
		if(card_office!=null){
			
			companyOffice.setText(card_office);
			
		}else{
			
			companyOffice.setText("");
		}
		
		if(card_fax!=null){
			companyFax.setText(card_fax);
			
		}else{
			
			companyFax.setText("");
		}
		
		
		
	}
	
	public void setPictureBitmap(final String picUrl){
		
		
		new Thread(){
    		public void run(){
    			try{
    				
    				
    				picBitmap=getBitmapFromUrl(picUrl);
    				imageLoaderHandler.sendEmptyMessage(PICCOMPLETE);
    				
    			}catch(MalformedURLException e){
    				
    				
    				
    				imageLoaderHandler.sendEmptyMessage(PICFAILED);
    				
    			}catch(IOException e){
    				
    				
    				
    				imageLoaderHandler.sendEmptyMessage(PICFAILED);
    				
    			}
    			
    		};

			
    		
    	}.start();
		
	}
	
	public void setLogoBitmap(final String logoUrl){
		
		
		new Thread(){
    		public void run(){
    			try{
    				
    				
    				logoBitmap=getBitmapFromUrl(logoUrl);
    				imageLoaderHandler.sendEmptyMessage(LOGOCOMPLETE);
    				
    			}catch(MalformedURLException e){
    				
    				
    				
    				imageLoaderHandler.sendEmptyMessage(LOGOFAILED);
    				
    			}catch(IOException e){
    				
    				
    				
    				imageLoaderHandler.sendEmptyMessage(LOGOFAILED);
    				
    			}
    			
    		};

			
    		
    	}.start();
		
	}
	
	public void showThumbnail(String video){
    
    	String thumbNail=video;
		String videoId=null;
		String imageUrl=null;
		
		if(thumbNail !=null&&thumbNail.length()>0){
			
			try{
				//Uri uri = Uri.parse(thumbNail);
				//videoId=uri.getQueryParameter("v").toString();
		    	
		    	//imageUrl="http://img.youtube.com/vi/"+videoId+"/2.jpg";
				imageUrl="http://img.youtube.com/vi/"+thumbNail+"/2.jpg";
		    	
				
			}catch(Exception e){
				
				Log.i("profile", "No video id");
				
			}
			
			
		}
		
    	
    	if(thumbNail !=null&&thumbNail.length()>0){
    		
    		
    		setImageBitmap(imageUrl);
    		
    	}else{
    		
    		videoThumbnail.setImageBitmap(bVideo);
    		
    	}
    }
	
	
	public void setImageBitmap(final String url){
    	
    	
    	
    	mBitmap=null;
    	
    	new Thread(){
    		public void run(){
    			try{
    				
    				
    				mBitmap=getBitmapFromUrl(url);
    				imageLoaderHandler.sendEmptyMessage(COMPLETE);
    				
    			}catch(MalformedURLException e){
    				
    				
    				
    				imageLoaderHandler.sendEmptyMessage(FAILED);
    				
    			}catch(IOException e){
    				
    				
    				
    				imageLoaderHandler.sendEmptyMessage(FAILED);
    				
    			}
    			
    		};

			
    		
    	}.start();
    	
    }
    
    private final Handler imageLoaderHandler = new Handler(new Callback(){
    	
    	
		
		public boolean handleMessage(Message msg) {
			// TODO Auto-generated method stub
			
			//System.out.println("message handler");
			switch(msg.what){
	    	
	    	
	    	case COMPLETE:
	    		
	    		
	    		if(mBitmap!=null){
	    			
	    			
	    			
	    			videoThumbnail.setImageBitmap(mBitmap);
	    		}
	    		
	    		
	    		break;
	    	case FAILED:
	    		
	    		
	    		videoThumbnail.setImageBitmap(bVideo);
	    		
	    		break;
	    		
	    	case PICCOMPLETE:
	    		
	    		if(picBitmap!=null){
	    			
	    			/*System.out.println("picBitmap not null");
	    			String haystack = pic_url;
	    			String needle = "default.jpg";
	    			
	    			int index = haystack.indexOf(needle);
	    			
	    			if (index != -1){
	    				BussinessPic.getLayoutParams().height = 34;
	    				BussinessPic.getLayoutParams().width = 34;
	    				BussinessPic.setImageBitmap(bPic);
	    			}else{
	    				BussinessPic.getLayoutParams().height = 100;
	    				BussinessPic.getLayoutParams().width = 100;
	    				BussinessPic.setImageBitmap(picBitmap);
	    			}*/
	    			if(picBitmap.getWidth()>100 || picBitmap.getHeight()>100){
	    				BussinessPic.getLayoutParams().height = 100;
	    				BussinessPic.getLayoutParams().width = 100;
	    			}
	    			BussinessPic.setImageBitmap(picBitmap);
	    			
	    		}else{
	    			BussinessPic.getLayoutParams().height = 34;
    				BussinessPic.getLayoutParams().width = 34;
	    			BussinessPic.setImageBitmap(bPic);
	    			
	    		}
	    		break;
	    	case PICFAILED:
	    		
	    		BussinessPic.getLayoutParams().height = 34;
				BussinessPic.getLayoutParams().width = 34;
	    		BussinessPic.setImageBitmap(bPic);
	    		
	    		break;
	    	case LOGOCOMPLETE:
	    		
	    		if(logoBitmap!=null){
	    			
	    			System.out.println("logoBitmap not null");
	    			/*String haystack = pic_url;
	    			String needle = "default.jpg";
	    			
	    			int index = haystack.indexOf(needle);
	    			
	    			if (index != -1){
	    				BussinessLogo.getLayoutParams().height = 34;
	    				BussinessLogo.getLayoutParams().width = 34;
	    				BussinessLogo.setImageBitmap(logoThumb);
	    			}else{
	    				BussinessLogo.getLayoutParams().height = 100;
	    				BussinessLogo.getLayoutParams().width = 100;
	    				BussinessLogo.setImageBitmap(logoBitmap);
	    			}*/
	    			if(logoBitmap.getWidth()>100 || logoBitmap.getHeight()>100){
	    				BussinessLogo.getLayoutParams().height = 100;
	    				BussinessLogo.getLayoutParams().width = 100;
	    			}
	    			BussinessLogo.setImageBitmap(logoBitmap);
	    			
	    		}else{
	    			BussinessLogo.getLayoutParams().height = 34;
    				BussinessLogo.getLayoutParams().width = 34;
	    			BussinessLogo.setImageBitmap(bLogo);
	    		}
	    		
	    		
	    		
	    		break;
	    	case LOGOFAILED:
	    		
	    		BussinessLogo.getLayoutParams().height = 34;
				BussinessLogo.getLayoutParams().width = 34;
	    		BussinessLogo.setImageBitmap(bLogo);
	    		
	    		break;
	    		default:
	    			
	    			break;
	    		
	    		
	    	}
			return true;
	    
		}
    	
    });
    		
   


	/*  private static Drawable getDrawableFromUrl(final String url) throws IOException,MalformedURLException{
		// TODO Auto-generated method stub
		  
		  System.out.println("image from url");
		return Drawable.createFromStream(((java.io.InputStream)new java.net.URL(url).getContent()), "name");
	}*/
	
	  
	  private Bitmap getBitmapFromUrl(String url) throws IOException,MalformedURLException{
		  
		  
		  try { 
			  
				BitmapFactory.Options optnsSizeOnly = new BitmapFactory.Options();
		      	optnsSizeOnly.inJustDecodeBounds = true;
		      	BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, optnsSizeOnly);
		      	
		      	
		
		      	final int REQUIRED_SIZE = 80;
		      			 int width_tmp = optnsSizeOnly.outWidth;
		      			 int height_tmp = optnsSizeOnly.outHeight;
		      			 
		      			 int scale = 1;
		      			 
		      			 while (true) {
		      		            if (width_tmp / 2 < REQUIRED_SIZE
		      		               || height_tmp / 2 < REQUIRED_SIZE) {
		      		                break;
		      		            }
		      		            width_tmp /= 2;
		      		            height_tmp /= 2;
		      		            scale *= 2;
		      		        }
		      			 
		      			 BitmapFactory.Options o2 = new BitmapFactory.Options();
		      			 o2.inSampleSize = scale;
		      			Bitmap logoBitmap= BitmapFactory.decodeStream((InputStream) new URL(url).getContent(), null, o2); 
		      	
		      	return logoBitmap;
		      	
		      	
		  } catch (MalformedURLException e) {  
			  
              e.printStackTrace();  
          } catch (IOException e) {  
        	  
        	  
        	  
              e.printStackTrace();  
          }  
          
          return null;  
		  
	  }
	
	
	private void download() {
		// TODO Auto-generated method stub
		
		byte[] decodedPicByte=null;
		byte[] decodedLogoByte=null;
		Bundle recdData = getIntent().getExtras();
		card_pic=recdData.getString("card_pic");
		//card_logo=recdData.getString("card_logo");
		
		try {
			decodedPicByte = Base64.decode(card_pic.getBytes());
			
		
			//decodedLogoByte = Base64.decode(card_logo.getBytes());
			
	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(picBitmap!=null){
			
			//Bitmap bitmap = ((BitmapDrawable)picDrawable).getBitmap();
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			picBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			decodedPicByte = stream.toByteArray();
		}
		
		if(logoBitmap!=null){
			
			//Bitmap bitmap = ((BitmapDrawable)logoDrawable).getBitmap();
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			logoBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			decodedLogoByte = stream.toByteArray();
			
		}else{
			
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bLogo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			decodedLogoByte = stream.toByteArray();
			
		}
		
		
		
		
		String[] value= new String[] {CardTable.KEY_NAME};
		
		ContentValues values=new ContentValues();
 		values.put(CardTable.KEY_USERID, card_id);
 		values.put(CardTable.KEY_NAME, card_name);
 		values.put(CardTable.KEY_EMAIL, card_email);
 		values.put(CardTable.KEY_NUMBER, card_number);
 		values.put(CardTable.KEY_PROFILE_PIC, decodedPicByte);
 		values.put(CardTable.KEY_DESIGNATION, card_designation);
 		values.put(CardTable.KEY_VIDEO, card_video);
 		values.put(CardTable.KEY_COMPANY, card_company_name);
 		values.put(CardTable.KEY_ADDRESS, card_address);
 		values.put(CardTable.KEY_LOGO, decodedLogoByte);
 		values.put(CardTable.KEY_DESCRIPTION, card_description);
 		values.put(CardTable.KEY_UPDATE_DATE, card_date);
 		values.put(CardTable.KEY_COMMENTS, card_comments);
 		values.put(CardTable.KEY_WEB_ADDRESS, card_weburl);
 		values.put(CardTable.KEY_OFFICE, card_office);
 		values.put(CardTable.KEY_FAX, card_fax);
 		
 		
		
		Uri queryUri = Uri.parse(CardManagingProvider.CONTENT_URI_DETAIL + "/" + card_id);
        Cursor cursor = getContentResolver().query(queryUri, value, null, null,null);
        if ( cursor != null && cursor.getCount() > 0 ) {
 		    
 		    	AlertDialog alert=null;
 				
 				AlertDialog.Builder builder = new AlertDialog.Builder(BussinessCard.this);
 				builder.setCancelable(false);
 				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

 				  public void onClick(DialogInterface dialog, int arg1) {
 				  // do something when the OK button is clicked
 					
 					  dialog.dismiss();
 					  
 					  
 				  }});
 				
 				builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
 					
 					  public void onClick(DialogInterface dialog, int arg1) {
 					  // do something when the OK button is clicked
 						  
 						  dialog.dismiss();
 					        
 						  
 					  }});
 				
 				builder.setTitle("Contacts Exist");
 				builder.setMessage("Contact already exists");
 				alert=builder.create();
 				alert.show();
 		    
 		   
 		    
 	  }else {
 		 
 		 getContentResolver().insert(CardManagingProvider.CONTENT_URI_DETAIL, values);
 		alertFunction("Contact Successfully Downloaded",2);
 		//Toast.makeText(BussinessCard.this, "Card has been Downloaded", Toast.LENGTH_SHORT).show();
 		
 	  }
        
        cursor.close();
	}

	
	public void alertFunction(String message,int check){
		AlertDialog alert=null;
		
		AlertDialog.Builder builder = new AlertDialog.Builder(BussinessCard.this);
		builder.setCancelable(false);
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

		  public void onClick(DialogInterface dialog, int arg1) {
		  // do something when the OK button is clicked
			
			  dialog.dismiss();
			  
			  
		  }});
		if(check ==0){
			builder.setTitle("Warning");
		}else if(check ==2){
			builder.setTitle("Contacts Downloaded");
		}
		
		builder.setMessage(message);
		alert=builder.create();
		alert.show();
		
	}
	
}

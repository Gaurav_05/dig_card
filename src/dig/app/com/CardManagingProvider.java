package dig.app.com;



import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class CardManagingProvider extends ContentProvider{
	
	
	private CardDataBase data;
	private SQLiteDatabase mDB;

	private static final int DETAIL=1;
	private static final int R_ID=2;
	private static final int L_ID=3;
	//private static final int C_ID=4;
	
	
	private static final String AUTHORTY="dig.app.contentprovider";
	private static final String DETAIL_PATH="detail_table";
	private static final String DETAIL_PATH_LOCAL="detail_table_local";
	
	
	public static final Uri CONTENT_URI_DETAIL=Uri.parse("content://" + AUTHORTY 
			+ "/" +  DETAIL_PATH);
	
	public static final Uri CONTENT_URI_DETAIL_LOCAL=Uri.parse("content://" + AUTHORTY 
			+ "/" +  DETAIL_PATH_LOCAL);
	
	
	public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
			+ "/" +  DETAIL_PATH ;

	public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
			+ "/" +   DETAIL_PATH ;
	
	
	private static final UriMatcher matcher=new UriMatcher(UriMatcher.NO_MATCH);
	static{
		
		matcher.addURI(AUTHORTY, DETAIL_PATH, DETAIL);
		matcher.addURI(AUTHORTY, DETAIL_PATH + "/#", R_ID);
		matcher.addURI(AUTHORTY, DETAIL_PATH_LOCAL + "/#", L_ID);
		//matcher.addURI(AUTHORTY, DETAIL_PATH_LOCAL, C_ID);
	}
	
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		int match=matcher.match(uri);
		mDB=data.getWritableDatabase();
		int rowDeleted=0;
		
		switch(match){
			
		 case DETAIL:
			 
			 System.out.println("delete all");
			 rowDeleted = mDB.delete(CardTable.DATABASE_TABLE, selection,
						selectionArgs);
			 
			 break;
			 
			 
		 case R_ID:
			 
			 System.out.println("profile delete ");
			 
			 String user_id = uri.getLastPathSegment();
				if (TextUtils.isEmpty(selection)) {
					rowDeleted = mDB.delete(
							CardTable.DATABASE_TABLE,
							CardTable.KEY_USERID + "=" + user_id, 
							null);
				} else {
					rowDeleted = mDB.delete(
							CardTable.DATABASE_TABLE,
							CardTable.KEY_USERID + "=" + user_id 
							+ " and " + selection,
							selectionArgs);
				}
			 
			 break;		 
		 case L_ID:
			 
			 System.out.println("delete single");
	
			 String id = uri.getLastPathSegment();
				if (TextUtils.isEmpty(selection)) {
					rowDeleted = mDB.delete(
							CardTable.DATABASE_TABLE,
							CardTable.KEY_ROWID + "=" + id, 
							null);
				} else {
					rowDeleted = mDB.delete(
							CardTable.DATABASE_TABLE,
							CardTable.KEY_ROWID + "=" + id 
							+ " and " + selection,
							selectionArgs);
				}
			 
			 break;
			 
		 default:	
				throw new IllegalArgumentException("Unknown URI: " + uri);
				
		
		}
		
		getContext().getContentResolver().notifyChange(uri, null);
		return rowDeleted;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		
		int match =matcher.match(uri);
		mDB=data.getWritableDatabase();
		long id = 0;
		
		switch(match){
		
		case DETAIL:
			
			id=mDB.insert(CardTable.DATABASE_TABLE, null, values);
			break;
			
			
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
			
		}
		
		getContext().getContentResolver().notifyChange(uri, null);
		return Uri.parse(DETAIL_PATH + "/" + id);
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		data=new CardDataBase(getContext());
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		// TODO Auto-generated method stub
		
		//System.out.println("cursor query");
		
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
		queryBuilder.setTables(CardTable.DATABASE_TABLE);
		int match =matcher.match(uri);
		
		switch(match){
		
		   case DETAIL:
			   System.out.println("fetch all");
			   break;
		
		   case R_ID:
			   System.out.println("fetch individual user id");
			   queryBuilder.appendWhere(CardTable.KEY_USERID + "="
						+ uri.getLastPathSegment());
			   break;
			   
		   case L_ID:
			   System.out.println("fetch individual usibg row id");
			   queryBuilder.appendWhere(CardTable.KEY_ROWID + "="
						+ uri.getLastPathSegment());
			   break;	   
			   
		   default:
				throw new IllegalArgumentException("Unknown URI: " + uri);		
		}
		
		mDB=data.getWritableDatabase();
		Cursor cursor = queryBuilder.query(mDB, projection, selection, 
										   selectionArgs, null, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		System.out.println(uri);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		
		int rowsUpdated = 0;
		mDB=data.getWritableDatabase();
		int uriMatch =matcher.match(uri);
		
		switch(uriMatch){
		
		case DETAIL:
			 
			rowsUpdated=mDB.update(CardTable.DATABASE_TABLE, values, selection, selectionArgs);
			
			 break;
			 
		case R_ID:
			
			
			String id = uri.getLastPathSegment();
			if(TextUtils.isEmpty(selection)){
			rowsUpdated=mDB.update(CardTable.DATABASE_TABLE,
						values,
						CardTable.KEY_USERID + "=" + id, 
						selectionArgs);
			}else {
				rowsUpdated=mDB.update(CardTable.DATABASE_TABLE,
						values,
						CardTable.KEY_USERID + "=" + id
						+ " and " 
						+ selection,
						selectionArgs);
			}
			
			break;
			
		case L_ID:
			
			String row_id = uri.getLastPathSegment();
			if(TextUtils.isEmpty(selection)){
			rowsUpdated=mDB.update(CardTable.DATABASE_TABLE,
						values,
						CardTable.KEY_ROWID + "=" + row_id, 
						selectionArgs);
			}else {
				rowsUpdated=mDB.update(CardTable.DATABASE_TABLE,
						values,
						CardTable.KEY_ROWID + "=" + row_id
						+ " and " 
						+ selection,
						selectionArgs);
			}
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);	
			
		}
		
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsUpdated;
	}

	
	
	
	
   
}    
    

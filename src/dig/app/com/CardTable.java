package dig.app.com;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class CardTable {
	
	
	private static final String TAG = "CardTable";
	public static final String DATABASE_TABLE = "card_details";
	
	public static final String KEY_ROWID = "_id";
	public static final String KEY_USERID ="user_id";
	public static final String KEY_NAME ="name";
	public static final String KEY_EMAIL ="email";
	public static final String KEY_NUMBER ="number";
	public static final String KEY_PROFILE_PIC = "profile_pic";
	public static final String KEY_DESIGNATION ="designation";
	public static final String KEY_VIDEO ="video";
	public static final String KEY_COMPANY ="company";
	public static final String KEY_ADDRESS ="address";
	public static final String KEY_LOGO ="logo";
	public static final String KEY_UPDATE_DATE ="date_of_update";
	public static final String KEY_DESCRIPTION ="description";
	public static final String KEY_WEB_ADDRESS ="web_address";
	public static final String KEY_COMMENTS ="comments";
	public static final String KEY_OFFICE ="office";
	public static final String KEY_FAX ="fax";
	
	
	/*private static final String DATABASE_CREATE = "create table "
    	+ DATABASE_TABLE
    	+"("
    	+ KEY_ROWID + " integer primary key autoincrement, "
    	+ KEY_USERID + " integer not null, "
    	+ KEY_NAME + " text not null, "
    	+ KEY_EMAIL + " text not null, "
    	+ KEY_NUMBER + " integer not null, "
    	+ KEY_PROFILE_PIC + " blob, "
    	+ KEY_DESIGNATION + " text not null, "
    	+ KEY_VIDEO + " text not null, "
    	+ KEY_COMPANY + " text not null, "
    	+ KEY_ADDRESS + " text not null, "
    	+ KEY_UPDATE_DATE + " text not null, "
    	+ KEY_DESCRIPTION + " text not null, "
    	+ KEY_LOGO + " blob "
    	+ ");";
	*/
	
	private static final String DATABASE_CREATE = "create table "
    	+ DATABASE_TABLE
    	+"("
    	+ KEY_ROWID + " integer primary key autoincrement, "
    	+ KEY_USERID + " integer not null, "
    	+ KEY_NAME + " text not null, "
    	+ KEY_EMAIL + " text not null, "
    	+ KEY_NUMBER + " integer not null, "
    	+ KEY_PROFILE_PIC + " blob, "
    	+ KEY_DESIGNATION + " text not null, "
    	+ KEY_VIDEO + " text , "
    	+ KEY_COMPANY + " text not null, "
    	+ KEY_ADDRESS + " text not null, "
    	+ KEY_UPDATE_DATE + " text not null, "
    	+ KEY_DESCRIPTION + " text , "
    	+ KEY_WEB_ADDRESS + " text , "
    	+ KEY_COMMENTS + " text , "
    	+ KEY_OFFICE + " text , "
    	+ KEY_FAX + " text , "
    	+ KEY_LOGO + " blob "
    	+ ");";
	
	
	public static void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		
		db.execSQL(DATABASE_CREATE);
	}
	
	
	public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
		Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS card_details");
        onCreate(db);
		
	}

}
